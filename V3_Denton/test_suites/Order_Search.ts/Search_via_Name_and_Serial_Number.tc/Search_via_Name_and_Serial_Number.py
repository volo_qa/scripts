#Owner:Arpine
#
# Steps to reproduce:
# 1. Create an order "Birth Certificate - State"
# 2. Remember Customer name
# 3. Add serial number and remember it
# 4. Go to order search tab
# 5. Open more options
#
# Expected result:
# 1. In Name field fill in customer name and search
# 2. Verify that found order is our created one
# 3. Reset search, and do search with stored serial number
# 4. Again verify that found order is our created one
#
import os
import random
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")
# Store helper variables
OIT = "Birth Certificate - State"
customerName = "Test Name" + str(random.randint(10, 9999))
serialNumberString=''
createdOrderNumberString=''
try:
    # Select creating order item from the list in New order screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in all required fileds for the selected order item
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS", log_file)
    obj.InputCustomerName(customerName, log_file)
    obj.Click_AddToOrder(log_file)

    # Click on SN icon, fill it as required field  and press "Submit" button to submit serial Number
    obj.click_on_serial_number_in_Order_summary()
    serialNumberString = required_obj.fill("CRS",log_file)
    obj.click_on_submit_button_in_serial_number_dialog(log_file)

    # Get order number string
    obj.locate_order_number_string(log_file)
    createdOrderNumberString = obj.return_order_number_string()
    Validate.check_isNumber(createdOrderNumberString, log_file, "Order Number")

    # Open log file and get it's content
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    # check if found any error in run_out.log file:
    # if yes, it means that TC failed. Leave TC!
    if re.search("Error:", string, re.I):
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# we done with run part
# it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

# Go to search tab and Click on more options button
obj.click_search_tab(log_file)
obj.click_on_more_less_options_btn(log_file)

# Search with customer Name
obj.fillIn_name_option_filed_value(customerName, log_file)
obj.click_search_btn(log_file)

# Get founded order number
foundedOrderNumber = obj.return_found_order_number()
Validate.diffutile(createdOrderNumberString, foundedOrderNumber, log_file, "Founded order number with search via \"Customer Name\":" + customerName)

# Reset Search and open more options
obj.click_reset_search_btn(log_file)
obj.click_on_more_less_options_btn(log_file)

# Search with Serial number
obj.fillIn_serialNumber_option_filed_value(str(serialNumberString['serial_num']), log_file)
obj.click_search_btn(log_file)

# Get founded order number
foundedOrderNumber = obj.return_found_order_number()
Validate.diffutile(createdOrderNumberString, foundedOrderNumber, log_file, "Founded order number with search via \"Serial Number\":" + str(serialNumberString['serial_num']))

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

