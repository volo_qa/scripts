#Owner:Naira
# coding=utf-8
# Order Creation
#
# Steps to reproduce:
# 1.
#
# Expected result:
# 1.
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")
try:
# navidate to Order Search tab
    obj.click_search_tab(log_file)
    # read log_file
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")


except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")
# validate +/-" buttons value before clickong on it
golden_more_btn_name = "More Options"
more_btn_name = obj.return_more_less_btn_text(log_file)
Validate.diffutile(golden_more_btn_name, more_btn_name, log_file, " \"+/-\" Button text")
# click on "+/-" button
obj.click_on_more_less_options_btn(log_file)

# validate "+/-" buttons value after clicking on it
golden_less_btn_name = "Less Options"
less_btn_name = obj.return_more_less_btn_text(log_file)
Validate.diffutile(golden_less_btn_name, less_btn_name, log_file, " \"+/-\" Button text")

# get current date
now = datetime.now()
if now.day < 10:
    current_date = str(now.month)+"/0"+str(now.day)+"/"+str(now.year)
else :
    current_date = str(now.month) + "/" + str(now.day) + "/" + str(now.year)
# validate fromDate field's content ... it should be current date
obj.locate_FromDate_field(log_file)
fromDate_value = obj.return_FromDate_field_value(log_file)
Validate.diffutile(current_date, fromDate_value, log_file, "\"from date\" field's value")

# validate toDate field's content ... it should be current date
obj.locate_ToDate_field(log_file)
ToDate_value = obj.return_ToDate_field_value(log_file)
Validate.diffutile(current_date, ToDate_value, log_file, "\"to date\" field's value")


#open log_file for read
file = open(log_file, 'r')
string = file.read()
file.close()

#search for error on log_file
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

