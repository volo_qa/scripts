#Owner:Nelli

'''
1. Navigate to CRS
2. Go to Order Search
3. Select "Order Date Range", e.g. 09/01/2016 - 09/01/2016 or current date
4. Click 'Search' button

Results: Display list of orders on 09/01/2016 or current date,
Column Ordered On display dates only Order Date Range or current date range
'''

import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...


Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    # navidate to Order Search tab
    obj.click_search_tab(log_file)
    obj.click_search_btn(log_file)

    # get current date
    from_date_field = obj.return_FromDate_field_value(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tSearch performs via " + from_date_field + " date.")


#------------------Read log_file------------------------#
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")


except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)



Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

#get list of Ordered_On
Ordered_On_List = obj.get_all_OrderedOon_fields_on_search(log_file)

# Check list is empty or not
if len(Ordered_On_List) == 0:
    Collect_log.simple_log(log_file, "Warning:\t\tList is empty. No order creates in "+ from_date_field + " date.")
else:
    #hold flag
    isNotMatch = True

    #search for not matching element in Order_On_List
    for i in Ordered_On_List:
        if not re.search(from_date_field, i, re.I):
            isNotMatch = False
            break

    if isNotMatch:
        Collect_log.simple_log(log_file, "Info:\t\tOrdered On contains only " + from_date_field + " date.")
    else:
        Collect_log.simple_log(log_file, "Error:\t\tSearch result contains wrong Ordered On date.")



#------------------Read log_file------------------------#
file = open(log_file, 'r')
string = file.read()
file.close()

#search for error on log_file
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

