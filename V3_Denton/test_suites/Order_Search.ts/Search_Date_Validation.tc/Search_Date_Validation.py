#Owner:Arpine
#
# Steps to reproduce:
# 1. Go to Orders search and click on Packages tab
# 2. Click on Search button, orders created i n that day should be founded
# 3. Change From Date value and make search within 90 days: Error message should be returned
# 4. Change search date behind 90 days
# 5. Click on search button
#
# Expected result:
# 1. check that Search date in "To Date" field is the same as in "From Date"
# 2. Nothing should be found in case of search behind 90 days
#
import os
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil  # for copy file
from datetime import datetime
import re
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py",
                                                       "tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log", "") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n")

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0])
Collect_log.simple_log(log_file, "--------------------- Run Part -------------------")
order_number_string=""
try:
    # Go to package search tab
    obj.click_search_tab(log_file)
    obj.click_package_search(log_file)

    # Click on Search button
    obj.click_search_btn(log_file)

    # Check that order search is working for current date
    order_count = obj.find_all_orders_in_order_queue(log_file)
    if order_count:
        Collect_log.simple_log(log_file, "Info:\t\tOrderQueue count is:\t" + str(order_count))
    elif obj.locate_no_match_found_string(log_file):
        Collect_log.simple_log(log_file, "Info:\t\t\"No match found\" string found.\t")
    else:
        Collect_log.simple_log(log_file, "Error:\t\tSomething went wrong for searching in current date")

    # Do search within 90 days
    fromDate = "10/10/2010"
    obj.fillIn_FromDate_filed_value(fromDate, log_file)

    # Click on Search button
    obj.click_search_btn(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")


if obj.locate_search_day_range_error_string(log_file):
    Collect_log.simple_log(log_file, "Info:\t\t\"Maximum date range is 90 days\" string founded!\t")
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Maximum date range is 90 days\" string is not founded!\t")

# Change search date behind 90 days
month = str(startTime.month)
day = str(startTime.day)
year = str(startTime.year + 1)
newDate = month + "/" + day + "/" + year
obj.fillIn_FromDate_filed_value(newDate, log_file)

# Click on Search button
obj.click_search_btn(log_file)

# check that Search date in "To Date" field is the same as in "From Date"
toDateFiled = obj.return_ToDate_field_value(log_file)
Validate.diffutile(newDate, toDateFiled, log_file, "Dates in \"To Date\" field.")

# Validate that nothing found in result
if obj.locate_no_match_found_string(log_file):
    Collect_log.simple_log(log_file, "Info:\t\t\"No match found\" string found.\t")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)


