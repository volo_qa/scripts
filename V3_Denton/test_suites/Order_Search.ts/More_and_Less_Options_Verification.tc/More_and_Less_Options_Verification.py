#Owner:Arpine
#
# Steps to reproduce:
# 1. Go to Orders search
# 2. Click on "More Options" button
#
# Expected result:
# 1. Verify that all fields values are defaults
# 2. Verify drop-down lists content
#
import os
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil  # for copy file
from datetime import datetime
import re
import sys
import  time

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py", "tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log", "") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n")

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0])
Collect_log.simple_log(log_file, "--------------------- Run Part -------------------")
order_number_string=""
try:
    # Go to package search tab
    obj.click_search_tab(log_file)

    # click on "More Options" button
    obj.click_on_more_less_options_btn(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

#------------------------Name------------------------------#
# Get Name field value
nameValue = obj.get_name_option_field_value()
if nameValue:
    Collect_log.simple_log(log_file, "Error:\t\t\"Name\" field is not empty. Value is: " + nameValue)
else:
    Collect_log.simple_log(log_file, "Info:\t\t\"Name\" field is empty.")

#------------------------Tracking ID------------------------------#
# Get tracking Id value
trackingIDValue = obj.get_trackingID_option_field_value()
if trackingIDValue:
    Collect_log.simple_log(log_file, "Error:\t\t\"Tracking ID\" field is not empty. Value is: " + nameValue)
else:
    Collect_log.simple_log(log_file, "Info:\t\t\"Tracking ID\" field is empty.")

#------------------------Department ID------------------------------#
# Get department field value
time.sleep(1)
departmentValue = obj.get_department_field_value()
if departmentValue == "-- Department --":
    Collect_log.simple_log(log_file, "Info:\t\t\"Department\" field value is: " + departmentValue)
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Department\" field value is: " + departmentValue)

# Make sure that all options are available in department options list
departmentFieldDefaultOptions = "-- Department --,Birth Records,Commissioners Court,Death Records,Foreclosures,Governmentals,Interoffice,Marks and Brands,Marriage Licenses,Military Discharges,Plats,Property Records,UCC"
departmentOptionsList = obj.get_department_field_options_list()
departmentOptionsList = departmentOptionsList.replace('\n',',')
Validate.diffutile(departmentFieldDefaultOptions, departmentOptionsList, log_file, "Department ID dropDown list content")

#------------------------Location ID------------------------------#
# Get location field value
locationValue = obj.get_location_field_value()
if locationValue == "-- Location --":
    Collect_log.simple_log(log_file, "Info:\t\t\"Location\" field value is: " + locationValue)
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Location\" field value is: " + locationValue)

# Make sure that all options are available in location options list
locationFieldDefaultValue = "-- Location --,Carrollton,Carrollton Passport,Carrollton Public,Cross Roads,Denton,In Office,Kiosk,Lewisville,Public,Super Admin"
locationOptionsList = obj.get_location_field_options_list()
locationOptionsList = locationOptionsList.replace('\n',',')
Validate.diffutile(locationFieldDefaultValue, locationOptionsList, log_file, "Location ID dropDown list content.")

#------------------------Origin ID------------------------------#
# Get origin field value
originValue = obj.get_origin_field_value()
if originValue == "-- Origin --":
    Collect_log.simple_log(log_file, "Info:\t\t\"Origin\" field value is: " + originValue)
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Origin\" field value is: " + originValue)

# Make sure that all options are available in origin options list
originFieldDefaultOptions = "-- Origin --,CRS,eForm NoReceipt,eForms,eRDS,erProxy,eSend,IANeForm,Passport Request,Search,UANeForm"
originOptionsList = obj.get_origin_field_options_list()
originOptionsList = originOptionsList.replace('\n',',')
Validate.diffutile(originFieldDefaultOptions, originOptionsList, log_file, "Origin ID dropDown list content")

#------------------------Assigned To------------------------------#
# Get assigned field value
assignedToValue = obj.get_assignedTo_field_value()
if assignedToValue == "-- AssignedTo --":
    Collect_log.simple_log(log_file, "Info:\t\t\"Assigned To\" field value is: " + assignedToValue)
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Assigned To\" field value is: " + assignedToValue)

#------------------------Payment Type------------------------------#
# Get payment type field value
paymentTypeValue = obj.get_payment_type_field_value()
if paymentTypeValue == "-- Payment Type --":
    Collect_log.simple_log(log_file, "Info:\t\t\"Payment Type\" field value is: " + paymentTypeValue)
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Payment Type\" field value is: " + paymentTypeValue)

# Make sure that all options are available in payment type options list
paymentTypeFieldDefaultOptions = "-- Payment Type --,Cash,Cashiers Check,Check,Company Account,Credit Card,IRS Direct Deposit,LegalEase,Money Order,Over/Short,Refund,State Direct Deposit,VitalChek,Void Refund"
paymentTypeOptionsList = obj.get_payment_type_field_options_list()
paymentTypeOptionsList = paymentTypeOptionsList.replace('\n',',')
Validate.diffutile(paymentTypeFieldDefaultOptions, paymentTypeOptionsList, log_file, "Payment Type dropDown list content")

#------------------------Transaction ID------------------------------#
# Get transactionID field value
transactionIDValue = obj.get_transactionID_option_field_value()
if transactionIDValue:
    Collect_log.simple_log(log_file, "Error:\t\t\"Transaction ID\" field is not empty. Value is: " + transactionIDValue)
else:
    Collect_log.simple_log(log_file, "Info:\t\t\"Transaction ID\" field is empty.")

#------------------------Serial Number------------------------------#
# Get Serial Number field value
serialNumberValue = obj.get_serial_number_option_field_value()
if serialNumberValue:
    Collect_log.simple_log(log_file, "Error:\t\t\"Serial Number\" field is not empty. Value is: " + serialNumberValue)
else:
    Collect_log.simple_log(log_file, "Info:\t\t\"Serial Number\" field is empty.")

#------------------------Document Number Start------------------------------#
# Get Serial Number field value
documentNumberValue = obj.get_document_number_start_field_value()
if documentNumberValue:
    Collect_log.simple_log(log_file, "Error:\t\t\"Document Number Start\" field is not empty. Value is: " + documentNumberValue)
else:
    Collect_log.simple_log(log_file, "Info:\t\t\"Document Number Start\" field is empty.")

#------------------------Document Number End------------------------------#
# Get Serial Number field value
documentNumberValue = obj.get_document_number_end_field_value()
if documentNumberValue:
    Collect_log.simple_log(log_file, "Error:\t\t\"Document Number End\" field is not empty. Value is: " + documentNumberValue)
else:
    Collect_log.simple_log(log_file, "Info:\t\t\"Document Number End\" field is empty.")


file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)