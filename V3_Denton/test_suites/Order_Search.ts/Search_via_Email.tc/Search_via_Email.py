#Owner:Nelli

'''
Steps
1. Navigate to CRS
2. Create new ordervia Email(e.g Military Discharge)
3. Go to Order Search
4. Fill Email
5. Perform searching

Expected Results:
1. Order search display created order
'''

import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()

config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...


Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    #variables
    OIT = "Military Discharge"
    email = "nelli.krtyan@volo.global"

    #create new order and process it up to Order Summary screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)
    obj.InputAccountEmail(email, log_file, 1)
    obj.Click_AddToOrder(log_file)

    #locate order number
    obj.locate_order_number_string(log_file)
    order_number_string = obj.return_order_number_string()

    #print order number in log
    Collect_log.simple_log(log_file, "Info:\t\tCreated order number is - " + str(order_number_string) + ".")

    #go to Search page and search via email
    obj.click_search_tab(log_file)
    obj.fill_email(email, log_file)
    obj.click_search_btn(log_file)


# read log_file
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")


except Exception as e:
    print e
    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

# get the order number list
order_number_list = obj.get_all_order_numbers_on_search(log_file)

#Check that created order is there
if order_number_string in order_number_list:
    Collect_log.simple_log(log_file, "Info:\t\tSearch result contains created order.")
else:
    Collect_log.simple_log(log_file, "Error:\t\tSearch result doesn't contain created order.")


#open log_file for read
file = open(log_file, 'r')
string = file.read()
file.close()

#search for error on log_file
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

