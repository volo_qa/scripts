#Owner:Nelli

'''
Steps
1. navigate to CRS
2. Go to Search page
3. Check subtabs availability and swithch them

Expected Results:
1. Subtabs are available
2. After swich appropriate page is loaded
'''

import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()

config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...


Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")
try:

# Navidate to Order Search tab
    obj.click_search_tab(log_file)

# Click on Packages subtab
    obj.locate_package_search(log_file)
    obj.click_package_search(log_file)


# read log_file
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")


except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

#Check that url contains 'PackageSearch'
package_url_string = obj.driver.current_url
golden_url_string = "PackageSearch"
Validate.grep_golden_in_actual(golden_url_string, package_url_string, log_file, "URL String")

#Check taht breadcrumb shows Package Search
obj.locate_search_breadcrumb(log_file)
golden_breadcrumb_value = "Package Search"
breadcrumb_value = obj.return_search_breadcrumb()
Validate.diffutile(golden_breadcrumb_value, breadcrumb_value, log_file, "Breadcrumb Value")

#Click on Orders tab
obj.locate_order_search(log_file)
obj.click_order_search(log_file)

#Check that url contains 'OrderSearch'
order_url_string = obj.driver.current_url
golden_url_string = "OrderSearch"
Validate.grep_golden_in_actual(golden_url_string, order_url_string, log_file, "URL String")

#Check that breadcrumb shows Order Search
obj.locate_search_breadcrumb(log_file)
golden_breadcrumb_value = "Order Search"
breadcrumb_value = obj.return_search_breadcrumb()
Validate.diffutile(golden_breadcrumb_value, breadcrumb_value, log_file, "Breadcrumb Value")


#Go to Documents tab
obj.locate_document_search(log_file)
obj.click_document_search(log_file)
time.sleep(3)

#switch to the new opened tab
obj.driver.switch_to_window(obj.driver.window_handles[1])

#Check that url contains 'OrderSearch'
document_url_string = obj.driver.current_url
golden_url_string = "clerkMode"
Validate.grep_golden_in_actual(golden_url_string, document_url_string, log_file, "URL String")


#open log_file for read
file = open(log_file, 'r')
string = file.read()
file.close()

#search for error on log_file
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

