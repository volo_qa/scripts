#Owner:Nelli

#  coding=utf-8
# Order Creation
#
# Steps to reproduce:
# 1.
#
# Expected result:
# 1.
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

# Run Part
Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    OIT = "State Liens - Comptroller"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"
    # NumOfPages = "1"
    # payment_method = "Cash"

    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)
    # obj.Click_OrderItem_Tab(log_file)

    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS", log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)
    obj.go_to_payment_screen(log_file)  # If making OIT with Account name, Assumed Name is Checked out without going into payment screen!!!
    obj.go_to_order_finalization('Cash', log_file)
    obj.click_on_void_button(log_file)
    obj.click_on_void_button_voidordersummary_screen(log_file)
    obj.Finalize_Void(log_file)

# log_file
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
	
# Validation Part
Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

obj.locate_order_finalization_breadcrumb(log_file)
golden_breadcrumb_value = "Order Finalization"
breadcrumb_value = obj.return_order_finalization_breadcrumb()
Validate.diffutile(golden_breadcrumb_value, breadcrumb_value, log_file, "Breadcrumb Value")

url_string = obj.driver.current_url
golden_url_string = "ShowOrderFinalization"
Validate.grep_golden_in_actual(golden_url_string, url_string, log_file, "URL String")

obj.locate_order_status(log_file)
golden_order_status = "Voided"
order_status = obj.return_order_status()
Validate.diffutile(golden_order_status, order_status, log_file, "Order Status in Web Page")

# db_order_status = DB_connect.get_order_status(golden_order_status)
# Validate.diffutile_db(db_order_status, order_status, log_file, "Order Status in DB")

obj.locate_total_amount(log_file)
golden_total_amount = '0.00'
total_amount = obj.return_total_ammount()
Validate.diffutile(golden_total_amount, total_amount, log_file, "Total Ammount is equal")

obj.locate_order_number_string(log_file)
order_number_string = obj.return_order_number_string()
Validate.check_isNumber(order_number_string, log_file, "Order Number")

obj.click_search_tab(log_file)
obj.fill_order_number(order_number_string, log_file)
obj.click_search_btn(log_file)

obj.locate_found_order_number(log_file)
golden_found_order_number = order_number_string
found_order_number = obj.return_found_order_number()
Validate.diffutile(golden_found_order_number, found_order_number, log_file, "Found Order Number String")

obj.locate_archive_string(log_file)
golden_archive_string = "Archive"
archive_string = obj.return_archive_string()
Validate.diffutile(golden_archive_string, archive_string, log_file, "Found Archive String")


# log_file
file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

	
