#Owner:Naira
#
# Covered BUG #15480
import os
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    # Store helper variables
    OIT = "Assumed Name"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    # Select creting order item from the list in New order screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in all required fileds for the selected order item
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS", log_file)

    # Insert account name and click "Add to Order"
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)

# Checkout the order
    obj.go_to_payment_screen(log_file)
    obj.go_to_order_finalization('Cash', log_file)

    # Open log file and get it's content
    file = open(log_file, 'r')
    string = file.read()
    file.close()


    # check if found any error in run_out.log file:
    # if yes, it means that TC failed. Leave TC!
    if re.search("Error:", string,re.I):
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
# We are done with running part,
# now it's time for validation
Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

obj.locate_order_finalization_breadcrumb(log_file)
golden_breadcrumb_value = "Order Finalization"
breadcrumb_value = obj.return_order_finalization_breadcrumb()
Validate.diffutile(golden_breadcrumb_value, breadcrumb_value, log_file, "Breadcrumb Value")

obj.locate_order_finalize_string(log_file)
golden_finalize_string = "Order Finalize"
finalize_string = obj.return_order_finalize_string()
Validate.grep_golden_in_actual(golden_finalize_string, finalize_string, log_file, "Finalize String")

url_string = obj.driver.current_url
golden_url_string = "ShowOrderFinalization"
Validate.grep_golden_in_actual(golden_url_string, url_string, log_file, "URL String")

obj.locate_order_status(log_file)
golden_order_status = "Finalized"
order_status = obj.return_order_status()
Validate.diffutile(golden_order_status, order_status, log_file, "Order Status in Web Page")

obj.locate_order_number_string(log_file)
order_number_string = obj.return_order_number_string()
Validate.check_isNumber(order_number_string, log_file, "Order Number")

# db_order_status = DB_connect.get_order_status(golden_order_status)
# Validate.diffutile_db(db_order_status, order_status, log_file, "Order Status in DB")

obj.click_search_tab(log_file)
obj.fill_order_number(order_number_string, log_file)
obj.click_search_btn(log_file)

obj.locate_found_order_number(log_file)
golden_found_order_number = order_number_string
found_order_number = obj.return_found_order_number()
Validate.grep_golden_in_actual(golden_found_order_number, found_order_number, log_file, "Found Order Number String")

obj.locate_archive_string(log_file)
golden_archive_string = "Capture"
archive_string = obj.return_archive_string()
Validate.grep_golden_in_actual(golden_archive_string, archive_string, log_file, "Found Archive String")

# Open log file and read content
file = open(log_file, 'r')
string = file.read()
file.close()

# check if found any error in run_out.log file:
# if yes, it means that TC failed. Leave TC!
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
