#Owner:Arpine
# coding=utf-8
# Administrative Key - Assign to any clerk 
#
# Steps to reproduce:
# Click on Administrative Key, find assignment icon near any order, click on it, select clerk name, click on Add button
#
# Expected result:
# Order is assigned to selected clerk name.
#-+
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from selenium.webdriver.support.ui import WebDriverWait
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

# Helper variables
assignName      = "Arpine Aleksanyan"
order_num       = ""
assigneeName    = ""
order_count     = ""
# At first we need to create a new order, with status "Suspended"
try:
    OIT = "Plats"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    # Select creting order item from the list in New order screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in all required fileds for the selected order item
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)

    # Insert account name and click "Add to Order"
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)

    # Get created order number
    obj.locate_order_number_string(log_file)
    order_number_string = obj.return_order_number_string()

    # Save the order
    obj.click_Save_Order(log_file)
    obj.fill_order_save_reason(log_file)

    # Refresh the "Order" queue and Press "Administrative" key button
    time.sleep(5)
    obj.Refresh_button(log_file)
    obj.wait_loader_to_be_absent(log_file)
    obj.Administrative_button(log_file)

    ## Find the created order in Order queue
    # Count all orders in the "Order Queue"
    order_count = obj.find_all_orders_in_order_queue(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tOrderQueue count is:\t" + str(order_count))

    # Iterate over all order items in "Order queue"
    # and find the first order which has assign to any clerk button.
    for i in range(1, order_count+1):
        # Get order number
        order_num = obj.read_order_number(i, log_file)
        # if order number coincides with our created order number click on "Assign to Clerk" button
        if order_num == order_number_string:
            if obj.find_assign_icon_button(i, log_file, assignName):
                assigneeName = obj.read_order_assigned_to(i,log_file)
                Collect_log.simple_log(log_file, "Info:\t\tRow in Order Queue is - " + str(i) + "\t" + order_num + "\t" + assigneeName)
                break
        else:
            continue

    # Insert asignee name
    obj.click_assignnames(log_file)
    obj.InputAssignName(assignName, log_file)
    obj.click_add(log_file)

    # Wait page loader to be absent
    obj.wait_loader_to_be_absent(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# We are done with run part, now is time to validate
Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")
try:
    # Order number (row1_4.text) assignName should be equal to assignName variable
    found_order_number = ""         # Order number
    found_order_row = ""

    for i in range(1, order_count + 1):
        found_order_number = obj.read_order_number(i, log_file)           # Order number

        # If founded order is our created one continue the work
        if found_order_number == order_num:
            Collect_log.simple_log(log_file, "Info:\t\tReading - " + str(found_order_number) + "\t which is equal to assigned Row in Order Queue.")  # \"" + str(column_4) + "\"")
            found_order_row = i
            break
        else:
            Collect_log.simple_log(log_file, "Info:\t\tReading - " + str(found_order_number) + "\t which is not equal to assigned Row in Order Queue.")

    # Compare assignee name
    found_assignee_name = obj.read_order_assigned_to(found_order_row, log_file)     # Assignee
    Collect_log.simple_log(log_file, "Info:\t\tAssigned Row in Order Queue is - " + str(found_order_row) + "\t" + found_order_number + "\t" + found_assignee_name)
    golden_assignee_name = assignName  # Golden assignName
    Validate.diffutile(golden_assignee_name, found_assignee_name, log_file, "Assigned Name")

except Exception as e:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)