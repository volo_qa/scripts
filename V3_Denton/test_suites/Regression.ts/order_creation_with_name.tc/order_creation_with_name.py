#Owner:Arpine
# coding=utf-8
# Order Creation
#
# Steps to reproduce:
# 1. Open "http://crs.qa.kofile.com/48121"
# 2. From Order Queue click on  Add New Order  '+' button.  
# 3. Fill in  Cusomer name (Account#, Email, Name) field.
# 4. Select an Order Item type from Order type dropdown (Bond to Idemnify-Service)
# 5. Fill in  required fields in the order item form.
# 6. Click on Add to Order.
# 7. From the Order Summary Page  click on Checkout button.
# 8. Select a payment method (e.g. cash).
# 9. Click on Checkout button.
#
# Expected result:
# 1. OIT should be created successfully.
# 2. Clerk should be on order Finalization page.
# 3. Order should be Finalized,
# 4. Order  should  get Order # and document #
# 5. Order Staus should be Finalized
# 6. Orer Status in DB should be 23
# 7. Order step should be Archived 
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from selenium.webdriver.support.ui import WebDriverWait
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    OIT = "Bond to Indemnify - Payment"
    name = "Test"

    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)
    # obj.Click_OrderItem_Tab(log_file)

    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)
    obj.InputCustomerName(name, log_file)
    obj.Click_AddToOrder(log_file)

    # Do payment for the order
    obj.go_to_payment_screen(log_file)  # If making OIT with Account name, Assumed Name is Checked out without going into payment screen!!!
    obj.go_to_order_finalization('Cash', log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

# Check if in the final page "Order Finalize" is available
obj.locate_order_finalize_string(log_file)
golden_finalize_string = "Order Finalize"
finalize_string = obj.return_order_finalize_string()
Validate.grep_golden_in_actual(golden_finalize_string, finalize_string, log_file, "Finalize String")

# Check if in the current page URL "ShowOrderFinalization" is available
url_string = obj.driver.current_url
golden_url_string = "ShowOrderFinalization"
Validate.grep_golden_in_actual(golden_url_string, url_string, log_file, "URL String")

# Check if in the final page "Finalized" is available
obj.locate_order_status(log_file)
golden_order_status = "Finalized"
order_status = obj.return_order_status()
Validate.diffutile(golden_order_status, order_status, log_file, "Order Status in Web Page")

# Check if order number is valid
obj.locate_order_number_string(log_file)
order_number_string = obj.return_order_number_string()
Validate.check_isNumber(order_number_string, log_file, "Order Number")

#db_order_status = DB_connect.get_order_status(golden_order_status)
#Validate.diffutile_db(db_order_status, order_status, log_file, "Order Status in DB")

# Go to search tab and search for order
obj.click_search_tab(log_file)
obj.fill_order_number(order_number_string, log_file)
obj.click_search_btn(log_file)

obj.locate_found_order_number(log_file)
golden_found_order_number = order_number_string
found_order_number = obj.return_found_order_number()
Validate.grep_golden_in_actual(golden_found_order_number, found_order_number, log_file, "Found Order Number String")

obj.locate_archive_string(log_file)
golden_archive_string = "Archive"
archive_string = obj.return_archive_string()
Validate.grep_golden_in_actual(golden_archive_string, archive_string, log_file, "Found Archive String")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
