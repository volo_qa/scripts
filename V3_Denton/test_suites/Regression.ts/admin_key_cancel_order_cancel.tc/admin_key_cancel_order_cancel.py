#Owner:Arpine
# coding=utf-8
# Administrative Key - Assign to any clerk 
#
# Steps to reproduce:
# Click on Administrative Key, find cancel 'x' near the first order which has it
# Press on 'x', then press on 'Cancel'
#
# Expected result:
# Order canceling process should be canceled
# The order should stay on the "Order queue" with the previous status
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from selenium.webdriver.support.ui import WebDriverWait
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

assignee = ""
assignName = "Mher Simonyan"
orderNumber = ""
assigneeName = ""
row_num = 0
start_from = 0

try:
    obj.Administrative_button(log_file)

    # Count all orders in the "Order Queue"
    order_count = obj.find_all_orders_in_order_queue(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tOrderQueue count is:\t" + str(order_count))

    # If the order queue is not empty,
    # iterate over orders and find the order with "Assign to" button
    if order_count:
        # find the first order which has assign to any clerk button.
        for i in range(1, order_count + 1):
            if obj.find_and_click_close_icon_button(i, log_file):
                # Get order number and order assignee
                orderNumber = obj.read_order_number(i, log_file)
                assigneeName = obj.read_order_assigned_to(i, log_file)
                Collect_log.simple_log(log_file, "Info:\t\tRow in Order Queue is - " + str(i) + "\t" + orderNumber + "\t" + assigneeName)
                row_num = i
                break
    else:
        #If the queue is empty create an order and set assignee
        OIT = "Governmentals"
        account_name = "4815 - mher.simonyan@volo.global"
        symbol = "4815"

        # Create Govermentals order item
        obj.AddNewOrder(log_file)
        obj.Initialize(log_file)
        obj.select_OIT(OIT, log_file)

        required_obj = Fill_in_required_filds(obj.driver)
        required_obj.crs_fill("CRS",log_file)
        obj.InputAccountName(account_name, log_file, symbol)
        obj.Click_AddToOrder(log_file)

        # Get created order number
        obj.locate_order_number_string(log_file)
        created_order_number = obj.return_order_number_string()

        # Save the order
        obj.click_Save_Order(log_file)
        obj.fill_order_save_reason(log_file)

        # click refresh button
        obj.Refresh_button(log_file)
        # Click on administrative button
        obj.Administrative_button(log_file)

        # Find created order
        order_count = obj.find_all_orders_in_order_queue(log_file)
        for i in range(1, order_count + 1):
            # Get order number
            order_num = obj.read_order_number(i, log_file)
            # if order number coincides with our created order number click on "X" button
            if order_num == created_order_number:
                if obj.find_and_click_close_icon_button(i, log_file):
                    orderNumber = order_num
                    assigneeName = obj.read_order_assigned_to(i, log_file)
                    Collect_log.simple_log(log_file, "Info:\t\tRow in Order Queue is - " + str(i) + "\t" + created_order_number + "\t" + assigneeName)
                    break

    # Canceling reason
    reason_string = "test reason"
    description_string = "test description"

    # Verify that we canceling correct order
    Action_reasons_bubble = obj.read_order_in_actionReasonsBubble(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tOrder In ActionReasonsBubble is - " + Action_reasons_bubble)

    # Enter cancel reason and cancel
    obj.enter_reason(reason_string, log_file)
    obj.enter_description(description_string, log_file)
    obj.click_cancel(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

try:

    # Refresh the view
    obj.Refresh_button(log_file)

    found_orderNumber = ""         # Order number
    found_order_status = ""        # Order Status
    golden_status = "Cancelled"

    # Count all orders in the "Order Queue"
    order_count = obj.find_all_orders_in_order_queue(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tOrderQueue count is:\t" + str(order_count))

    # Iterate over all order items, and verify that our order is not canceled
    for i in range(1, order_count + 1):
        found_orderNumber = obj.read_order_number(i, log_file)
        found_order_status = obj.read_order_status(i, log_file)    # order status
        if found_orderNumber == orderNumber and golden_status != found_order_status:
            Collect_log.simple_log(log_file, "Info:\t\tCanceled Row in Order Queue is - " + str(i) + "\t" + found_orderNumber + "\tStatus is: " + found_order_status)
            break
        else:
            Collect_log.simple_log(log_file, "Info:\t\tReading - " + str(found_orderNumber) + "\t Status is: " + found_order_status)

except Exception as e:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
