#Owner:Arpine
# coding=utf-8
# Initalize: Initalize pop-up absence
#
# Description: Checking
# 1. initilization pop-up should NOT appears, in case balance drawer is initilized
# 2. if Balance drawer is NOT initilized, initilizeing by pop-up and chacking the same (1) 
#
# Steps to reproduce:
# 1. Click "Balance Drawer" button
# 2. Try to find - Beginning Balance row 
# "  3. Click Post Difference	"
#   4. Choose in Drop Down List(ddl) Payment Method: BALANCE or whatever
#   5. Click Post
#   6. Again go to Balance Drawer (e.g by URL)
#   Valdiate 1:   7. Check if "Beginning Balance row" is 0.00
# 8. Go to order queue
# 9. Click "+" Add New Order button
# 10. Click  Initilize  
#
# Expected result:
# 2. If is NOT 0.00 (than ok, is initialized) go to step 8
# 8. Valdiate 1:  Inilization pop-up should  NOT apear
# 9. Valdiate 2:  User in add New order screen (initilized succesfully)
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch, BalanceDrawer
import shutil                                                               # for copy file
from datetime import datetime
import re
from selenium.webdriver.support.ui import WebDriverWait
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    PayMtd = "BALANCE"
    obj.click_Balance_Drawer(log_file)

    column_1 = obj.read_balance_row_label(3, log_file)  # drawer_summary_row    3 - 0.00, 11 - 600.00
    if column_1 == "Beginning Balance":
        column_2 = obj.read_balance_row_expected_value(3, log_file)
        Collect_log.simple_log(log_file, "Info:\t\t" + column_1 + " is " + column_2)
    else:
        column_2 = obj.read_balance_row_expected_value(11, log_file)
        Collect_log.simple_log(log_file, "Info:\t\t" + "Beginning Balance" + " is " + column_2)

    if column_2 == '0.00':
        obj.click_Initialize_Drawer(log_file)
        obj.input_init_amount(400, log_file)
        obj.click_init_submit(log_file)

    obj.click_Orders(log_file)
    obj.AddNewOrder(log_file)

    file = open(log_file, 'r');     string = file.read();    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        exit(0)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    print e
    Collect_log.end_time(log_file)
    Collect_log.duration_time(log_file, startTime)
    Collect_log.run_part_status(log_file, "FAIL")
    exit(0)

Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

try:

    # Validate 1:  Inilization pop-up should NOT apear
    obj.Not_Initialize(log_file)

    # Validate 2:  User should be in add New order screen (initilized succesfully) use URL validate
    golden_url = "http://crs.qa.kofile.com/48121/Order/NewOrder"
    url = obj.driver.current_url
    Validate.diffutile(golden_url, url, log_file, "New Order URL")

    Login.driver_close(obj.driver, log_file)  # Closes the driver \ Browser


except Exception as e:
        print e
        Collect_log.end_time(log_file)
        Collect_log.duration_time(log_file, startTime)
        Collect_log.validate_part_status(log_file, "FAIL")
        exit(0)


file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    exit(0)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")

Collect_log.end_time(log_file)
Collect_log.duration_time(log_file, startTime)
