#Owner:Arpine
# coding=utf-8
# Administrative Key - Assign to any clerk 
#
# Steps to reproduce:
# Click on Administrative Key, find assignment icon near any order, click on it, select clerk name, click on Add button
#
# Expected result:
# Order is assigned to selected clerk name.
#-+
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from selenium.webdriver.support.ui import WebDriverWait
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

"""order_count = 0
for tr in obj.driver.find_elements_by_xpath(".//*[@id='OrderQueue']/tbody/tr"):
    order_count = order_count + 1
Collect_log.simple_log(log_file, "OrderQueue count is:\t" + str(order_count))
"""
# assignee = "Mher Simonyan"
assignee = ""
assignName = "Raffi Simonyan"
column_4 = ""
column_11 = ""
row_num = 0
start_from = 0



try:
    """obj.Administrative_button(log_file)
    for i in range(1, order_count+1):       # TODO This is a Hard code, shoud be updated and made a function in library
        column_4      = obj.read_order_number(i,log_file)
        column_11     = obj.read_order_assigned_to(i,log_file)
        if column_11 == assignee:
            start_from = start_from + 1
            if start_from > 4: #20:      # TODO take 3th assigned Order as first two orders have problem and can not be assigned!!!
                Collect_log.simple_log(log_file, "Info:\t\tRow in Order Queue is - " + str(i) + "\t" + column_4 + "\t" + column_11)
                row_num = i
                break
				"""
	# Press "Administrative" key button
    obj.Administrative_button(log_file)

    # Count all orders in the "Order Queue"
    order_count = obj.find_all_orders_in_order_queue(log_file)
    Collect_log.simple_log(log_file, "OrderQueue count is:\t" + str(order_count))

    # Iterate over all order items in "Order queue"
    # and find the first order which has assign to any clerk button.
    for i in range(1, order_count+1):
        if obj.find_assign_icon_button(i, log_file):
            # Get order number and order assignee
            column_4 = obj.read_order_number(i,log_file)
            column_11 = obj.read_order_assigned_to(i,log_file)
            Collect_log.simple_log(log_file, "Info:\t\tRow in Order Queue is - " + str(i) + "\t" + column_4 + "\t" + column_11)
            break
        else:
            continue

    obj.click_assignnames(log_file)
    obj.InputAssignName(assignName, log_file)
    obj.click_add(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

try:
    # click refresh button
    obj.Refresh_button(log_file)

    # Order number (row1_4.text) assignName should be equal to assignName variable
    found_column_4 = ""         # Order number
    # found_column_11 = ""        # Assignee Name
    found_order_row = ""

    print "order_count = " + str(order_count)
    for i in range(1, order_count + 1):
        found_column_4 = obj.read_order_number(i, log_file)           # Order number

        if found_column_4 == column_4:
            Collect_log.simple_log(log_file, "Info:\t\tReading - " + str(found_column_4) + "\t which is equal to assigned Row in Order Queue.")  # \"" + str(column_4) + "\"")
            # print "in if order_number = " + found_column_4
            # print "in if golden_order_number = " + column_4

            #order_number = found_column_4
            #golden_order_number = column_4  # Golden Order number
            #Validate.diffutile(golden_order_number, order_number, log_file, "Order Number")

            found_order_row = i
            break
        else:
            Collect_log.simple_log(log_file, "Info:\t\tReading - " + str(found_column_4) + "\t which is not equal to assigned Row in Order Queue.")

    found_column_11 = obj.read_order_assigned_to(found_order_row, log_file)     # Assignee
    Collect_log.simple_log(log_file, "Info:\t\tAssigned Row in Order Queue is - " + str(found_order_row) + "\t" + found_column_4 + "\t" + found_column_11)
    assignee_name = found_column_11
    golden_assignee_name = assignName  # Golden assignName
    Validate.diffutile(golden_assignee_name, assignee_name, log_file, "Assigned Name")

except Exception as e:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
