#Owner:Nelli
# coding=utf-8

'''
Steps:
1. Create erProxy via Package.
2. Naviagte to CRS
3. Click on Search-Package tab
4. Enter package name and click on Search button
5. Created erproxy should be displayed
6. Finalize that order

Results:
1. Check that order item status is finalized
2. Check that Order is in Indexinq Queue physically
3. Check that order workflow step is Indexing on Search page.
'''

import os
import time
import autoit
import signal
import proc as proc
import select
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys
import os
import distutils
from distutils import dir_util
import psutil
import fileinput
#----------------------erProxy creation part------------------#

#get current directory
real_path = os.path.dirname(os.path.realpath(__file__))

#get Payloads path
xml_dir = real_path + "\..\erProxy Package\Payloads"

#get erProxy path and switch to it
config_path = real_path + "\..\erProxy Package\erProxy"
os.chdir(config_path)


#give correct directory in config file
file = open('erProxyTester.exe.config', 'r')
text = file.readlines()
file.close()

new_config = []
for line in text:
    substring = "PayloadFolder"
    entirestring = "\t<add key=\"PayloadFolder\" value=\""+ xml_dir +"\"/>\n"
    if substring in line:
        line = line.replace(line, entirestring)
    new_config.append(line)

file = open('erProxyTester.exe.config', 'w')
file.writelines(new_config)
file.close()


#Swich to xml_dir directory
os.chdir(xml_dir)

#get current path and remove SIMPLIFILE-SIMPLIFILE folder
current_path = os.getcwd()
if os.path.isdir(current_path+"./SIMPLIFILE-SIMPLIFILE"):
    distutils.dir_util.remove_tree("./SIMPLIFILE-SIMPLIFILE")

# switch to erProxy Package folder and copy SIMPLIFILE-SIMPLIFILE folder with new original xml file
os.chdir(real_path+"\..\erProxy Package")
shutil.copytree('./SIMPLIFILE-SIMPLIFILE', './Payloads/SIMPLIFILE-SIMPLIFILE')

os.chdir(real_path+"\..\erProxy Package\Payloads\SIMPLIFILE-SIMPLIFILE")
#get current date, make it without milsec and rename xml name
current_date = str(datetime.now())
short_current_date = current_date[:-7].replace(":", "-")
os.rename("erProxy.xml",short_current_date+".xml")


#get erProxy_Package path and swich to it
erProxy_path = real_path + "\..\erProxy Package\erProxy"
os.chdir(erProxy_path)

#run erProxyTester.exe, wait for 5 seconds and kill that process
subp = subprocess.Popen(["erProxyTester.exe"])
p = psutil.Process(subp.pid)
try:
    p.wait(timeout=5)
except psutil.TimeoutExpired:
    p.kill()

#swich to real_path
os.chdir(real_path)

#---------------------------------Time to go CRS------------------------#
obj = Actions()

config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...


Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")
try:
    Collect_log.simple_log(log_file, "Info:\t\terProxy creation part is done successfully.")
    obj.click_search_tab(log_file)
    obj.locate_package_search(log_file)
    obj.click_package_search(log_file)
    package_name = "SIMPLIFILE_"+short_current_date
    obj.fill_eRecording_package_Id(package_name, log_file)
    obj.click_search_btn(log_file)


#------------------------------read log_file----------------------------------#
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")


except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)



Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

# search for created package
obj.locate_package_id_in_row(log_file)
golden_found_package_name = package_name
found_package_name = obj.return_package_id_in_row()
Validate.grep_golden_in_actual(golden_found_package_name, found_package_name, log_file, "Found searched package id.")

#found created package order number and  click on it
obj.locate_order_number_package_id(log_file)
order_number = obj.return_order_number_package_id_in_row()
Collect_log.simple_log(log_file, "Info:\t\tCreated erproxy order number is-" + order_number)
obj.click_on_order_number_package_id_in_row(log_file)

#edit created erProxy order
obj.locate_edit_order_btn(log_file)
obj.click_on_edit_order_btn(log_file)

#finalize the order
obj.locate_edit_order_item_icon(log_file)
obj.click_on_edit_icon_in_row(log_file)

#fill in required fileds
time.sleep(2.5)
required_obj = Fill_in_required_filds(obj.driver)
required_obj.crs_fill("CRS", log_file)

#Save order
obj.Click_AddToOrder(log_file)

# Checkout the order
obj.click_on_checkout_button_in_Order_Summary(log_file)

#check that clerk is in order finalization screen via breadcrumb
obj.locate_order_finalization_breadcrumb(log_file)
golden_breadcrumb_value = "Order Finalization"
breadcrumb_value = obj.return_order_finalization_breadcrumb()
Validate.diffutile(golden_breadcrumb_value, breadcrumb_value, log_file, "Breadcrumb Value")

#check order item status is finalized
obj.locate_order_status(log_file)
golden_order_status = "Finalized"
order_status = obj.return_order_status()
Validate.diffutile(golden_order_status, order_status, log_file, "Order Status in Web Page")

#Check that "Get Next" button is displayed
get_next_btn = obj.locate_get_next_btn(log_file)
if get_next_btn:
    Collect_log.simple_log(log_file, "Info:\t\t\"Get Next\" button is located.")
else:
    Collect_log.simple_log(log_file, "Error:\t\t\"Get Next\" button isn't located.")


#---------------------------------Read log_file-------------------------------------#
#open log_file for read
file = open(log_file, 'r')
string = file.read()
file.close()

#search for error on log_file
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

