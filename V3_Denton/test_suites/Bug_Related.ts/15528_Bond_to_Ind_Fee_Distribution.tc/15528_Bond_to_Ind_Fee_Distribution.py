#Owner:Arpine
#
# Steps to reproduce:
# 1. Go to Order queue
# 2. Click on "Add New Order" button
# 3. Add "Bont to Indemnify - Service"
# 4. Fill in required fields and finilize the order
# 5. In order finalization screen click on edit icon
# 6. Check Fee Distribution
#
# Expected result:
# 1. Fee distribution is the following (according to Denton artifacts table):
# - $10 RM-770 Fund,
# - $1 CH Security,
# - $10 Archive,
# - Balance to Recordings.
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil  # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py",
                                                       "tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log", "") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n")

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0])  # 0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file, "--------------------- Run Part -------------------")

try:
    OIT = "Bond to Indemnify - Service"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    # Add new order
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in required fields
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)

    # Do order payments
    obj.go_to_payment_screen(log_file)
    obj.go_to_order_finalization('Cash', log_file)

    # click on Edit button nfrom Finalization page to edit  OrderItem
    obj.click_edit_on_order_row(log_file)

    # click on "View/edit order item Funds button to open Fee dostribution
    obj.click_view_edit_order_item_funds(log_file)


    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# We are done with running part,
# now it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

# Get order funds from Fee Distribution and check their existance
curtoiseSecutityText    = obj.return_Records_Management_and_Preservation_string()
goldenText              = "Courthouse Security"
Validate.diffutile(curtoiseSecutityText, goldenText,log_file, "\"Courthouse Security\" fee exists!" )

RMFundText              = obj.return_Courthouse_Security_string()
goldenText              = "RM-770 Fund"
Validate.diffutile(RMFundText, goldenText, log_file, "\"RM-770 Fund\" fee exists!")

archiveFeeText          = obj.return_County_Clerk_Records_Archive_string()
goldenText              = "Archive Fee"
Validate.diffutile(archiveFeeText, goldenText, log_file, "\"Archive Fee\" fee exists!")

recordingsText          = obj.return_New_Courthouse_Fee_string()
goldenText              = "Recordings"
Validate.diffutile(recordingsText, goldenText, log_file, "\"Recordings\" fee exists!")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)



