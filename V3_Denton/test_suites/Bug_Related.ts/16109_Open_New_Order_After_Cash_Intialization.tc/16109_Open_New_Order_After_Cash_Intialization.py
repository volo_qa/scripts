#Owner:Arpine
#
# Steps to reproduce:
# 1. Click "Balance Drawer" button
# 2. Try to find - Beginning Balance row and uninitialize if already initilized
# 3. Go to Order queue
# 4. Click on "Add New Order" button
# 5. Drawer initializer should appear, Initialize it
#
# Expected result:
# 1. New Order page should be opened
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch, BalanceDrawer
import shutil                                                               # for copy file
from datetime import datetime
import re
from selenium.webdriver.support.ui import WebDriverWait
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)


startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0])  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    PayMtd = "BALANCE"
    obj.click_Balance_Drawer(log_file)

    column_1 = obj.read_balance_row_label(3, log_file)  # drawer_summary_row    3 - 0.00, 11 - 600.00
    if column_1 == "Beginning Balance":
        column_2 = obj.read_balance_row_expected_value(3, log_file)
        Collect_log.simple_log(log_file, "Info:\t\t" + column_1 + " is " + column_2)
    else:
        column_2 = obj.read_balance_row_expected_value(11, log_file)
        Collect_log.simple_log(log_file, "Info:\t\t" + "Beginning Balance" + " is " + column_2)

    if column_2 != '0.00':
        obj.click_Post_Difference(log_file)
        obj.select_Payment_Method(PayMtd, log_file)
        obj.click_Payment_Method_Post(log_file)
        obj.click_Balance_Drawer(log_file)  # TODO Change to go to Balance drawer by URL

        column_1 = obj.read_balance_row_label(3, log_file)  # drawer_summary_row    3 - 0.00, 11 - 600.00
        if column_1 == "Beginning Balance":
            column_2 = obj.read_balance_row_expected_value(3, log_file)
            Collect_log.simple_log(log_file, "Info:\t\t" + column_1 + " is " + column_2)
        else:
            column_2 = obj.read_balance_row_expected_value(11, log_file)
            Collect_log.simple_log(log_file, "Info:\t\t" + "Beginning Balance" + " is " + column_2)

        if column_2 != '0.00':  # 7.
            Collect_log.run_part_status(log_file, "FAIL")
            exit(0)

    # Go to Order queue and Add new order
    obj.click_Orders(log_file)
    obj.AddNewOrder(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:
    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


Collect_log.simple_log(log_file,"------------------ Validation Part ----------------")

try:
    # Initializer pop-up should appear
    obj.Initialize_PopUp_presence(log_file)

    # Initialize drawer
    obj.Initialize(log_file)

    # Locate New Order string in breadcrumb
    obj.locate_new_order_breadcrumb(log_file)

    # Compare golden with founded string
    NewOrderText = obj.return_new_order_breadcrumb()
    golden_new_order = "New Order"
    Validate.diffutile(golden_new_order, NewOrderText, log_file, "New Order string in breadcrumb")

except Exception as e:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
