#Owner:Arpine
#
# Steps to reproduce:
# 1. Go to Order queue
# 2. Click on "Add New Order" button
# 3. Add "Informal Marriage License"
# 4. Fill in required fields and add to order
# 5. In order summary page select 100% discount and click on Apply
# 6. Checkout the order
# 7. In order finalization screen click on discount Reset
#
# Expected result:
# 1. Click on edit icon in order row
# 2. Click on "View/Edit Order item funds"
# 3. Make sure that order funds are not 0
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil  # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py", "tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log", "") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n")

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0])  # 0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file, "--------------------- Run Part -------------------")

try:
    OIT = "Informal Marriage License"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    # Add new order
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in required fields
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)

    # Open order discount list and select 100% discount
    obj.click_on_discount_list(log_file)
    obj.select_hundred_percent_discount(log_file)

    # Click on Apply
    obj.click_on_discount_apply(log_file)

    # Get order price in Order summary screen, it should be 0
    obj.locate_order_total_price(log_file)
    orderPrice = obj.get_order_total_price()
    goldenPrice = "$0.00"
    Validate.diffutile(goldenPrice, orderPrice, log_file, "Order total price comparison")

    # Finilize the order and reset order discount
    obj.go_to_payment_screen(log_file)
    obj.click_on_discount_reset(log_file)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# We are done with running part,
# now it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

# Click on Edit icon in order row
obj.click_edit_on_order_row(log_file)

# Click on "View/Edit Order item funds"
obj.click_view_edit_order_item_funds(log_file)

#Compare Total fee value with fee distribution total value
total_amount_fee = obj.return_total_ammount_fee()
total_fee_distibution = obj.return_total_balance_due_value()
Validate.diffutile(total_amount_fee, total_fee_distibution, log_file, "Order total price and order total fee distribution: ")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)



