#Owner:Nelli

#  coding=utf-8
# Order Creation
#
# Steps to reproduce:
# 1.
#
# Expected result:
# 1.
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    #Variables
    OIT = "Photos for Passport"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"
    NumOfPages = "2"

    #creation order
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.fill_No_of_Photos(NumOfPages, log_file)
    obj.Click_AddToOrder(log_file)

    #read log_file
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)


Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

#validate breadcrumb on Order Summary screen
obj.locate_order_summary_breadcrumb(log_file)
golden_breadcrumb_ordersummary_value = "Order Summary"
breadcrumb_ordersummary_value = obj.return_order_summary_breadcrumb()
Validate.diffutile(golden_breadcrumb_ordersummary_value, breadcrumb_ordersummary_value, log_file, "Breadcrumb_OrderSummary Value")

#validate corectness of  Number_Of value on Order Summary screen
obj.locate_Number_Of(log_file)
golden_ordersummary_Number_Of = NumOfPages
Number_of_ordersummary = obj.return_Number_Of()
Validate.diffutile(golden_ordersummary_Number_Of, Number_of_ordersummary, log_file, "Number Of String on Order Summary screen")

#validate that clerk is on Order Summary screen via url
url_string = obj.driver.current_url
golden_url_string = "OrderSummary"
Validate.grep_golden_in_actual(golden_url_string, url_string, log_file, "URL String")


#Finalize the order
obj.go_to_payment_screen(log_file)
obj.go_to_order_finalization('Cash', log_file)


#validate that order status is Finalized on Order Finalization screen
obj.locate_order_status(log_file)
golden_order_status = "Finalized"
order_status = obj.return_order_status()
Validate.diffutile(golden_order_status, order_status, log_file, "Order Status in Web Page")


#validate corectness of  Number_Of value on Order Finalization screen
obj.locate_Number_Of_orderfinalization(log_file)
golden_orderfinalization_Number_Of = NumOfPages
Number_of_orderfinalization = obj.return_Number_Of_orderfinalization()
Validate.diffutile(golden_orderfinalization_Number_Of, Number_of_orderfinalization, log_file, "Number Of String on Order Finalization screen")


file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)


