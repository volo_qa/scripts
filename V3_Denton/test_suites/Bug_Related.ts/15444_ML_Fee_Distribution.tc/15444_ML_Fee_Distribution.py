#Owner:Naira
#
import os
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    # Store helper variables
    OIT = "Marriage License"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    # Select creting order item from the list in New order screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in all required fileds for the selected order item
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.fill("CRS", log_file)
    required_obj.crs_fill("CRS",log_file)

    # Insert account name and click "Add to Order"
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)

    # Checkout the order
    obj.go_to_payment_screen(log_file)
    obj.go_to_order_finalization('Company Account', log_file)
    # clicn on Edit button nfrom Finalization page to edit  OrderItem
    obj.click_edit_on_order_row(log_file)
    # click on "View/edit order item Funds button to open Fee dostribution
    obj.click_view_edit_order_item_funds(log_file)

    # Open log file and get it's content
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    # check if found any error in run_out.log file:
    # if yes, it means that TC failed. Leave TC!
    if re.search("Error:", string,re.I):
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:
    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
# We are done with running part,
# now it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

total_amount_fee = obj.return_total_ammount_fee()
total_fee_distibution = obj.return_total_balance_due_value()
#Compare Total fee value with fee distribution total value
Validate.diffutile(total_amount_fee,total_fee_distibution,log_file,"Total amount and Total Fee distribution are same" )

# Open log file and read content
file = open(log_file, 'r')
string = file.read()
file.close()

# check if found any error in run_out.log file:
# if yes, it means that TC failed. Leave TC!
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

