#Owner:Arpine
# Order Creation
#
# Steps to reproduce:
# 1. Open "http://crs.qa.kofile.com/48121"
# 2. From Order Queue click on  Add New Order  '+' button.  
# 3. Select an Order Item type from Order type dropdown (Bond to Idemnify-Payment)
# 5. Fill in  email
#
# Expected result:
# 1. OIT typeshould not be changed
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    # Store helper variables
    OIT = "Copies"
    email = "mher.simonyan@volo.global"
    symbol = "mher.simonyan@volo.global"

    # Click on "Add New Order" button
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)

    # Select Copies order item
    obj.select_OIT(OIT, log_file)

    # Get OIT after selection
    OIT_before_email = obj.get_selected_OIT_text(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tSelected OIT text before email input: \"%s\"\t" % OIT_before_email)

    # Input account email
    obj.InputAccountEmail( email, log_file, symbol)

    # Get OIT after email input
    OIT_after_email_input = obj.get_selected_OIT_text(log_file)
    Collect_log.simple_log(log_file, "Info:\t\tSelected OIT text after email input: \"%s\"\t" % OIT_after_email_input)

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")


except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# We are done with running part,
# now it's time for validation
Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

# Compare previously recorder OIT string
# They should be the same
Validate.diffutile(OIT_before_email, OIT_after_email_input, log_file, "Comparing OIT before and after email input")

# Open log file and read content
file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
