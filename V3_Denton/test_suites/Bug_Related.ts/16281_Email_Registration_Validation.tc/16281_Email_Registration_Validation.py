#Owner:Arpine
# Order Creation
#
# Steps to reproduce:
# 1. Go to Order queue
# 2. Click on "Add New Order" button
# 3. Click on "More Options" on order header
# 4. Fill in email field with incorrect value
#
# Expected result:
# 1. Click on New registration button
# 2. Validation Warning dialog should be poped up
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil  # for copy file
from datetime import datetime
import re
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py", "tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log", "") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n")

obj.driver.maximize_window()
obj.login_to_crs(config.crs_urls[0])  # 0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file, "--------------------- Run Part -------------------")

try:
    # Add new order
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)

    # Click on "More Options" button
    obj.open_more_options(log_file)

    # Insert invalid email and user name
    obj.insert_new_email(log_file, "#$%%^@gmail.com")
    obj.insert_new_account_first_name(log_file, "Alex")
    obj.insert_new_account_last_name(log_file, "Clare")

    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# We are done with running part,
# now it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

# Click on "New registration" button
obj.click_on_new_registration_button(log_file)

# "Validation Warning" dialog should pop up
if obj.locate_validation_warning_dialog(log_file):
    Collect_log.simple_log(log_file, "Info:\t\t\"Validation Warning\" dialog is available.")

# Click on Ok button on "Validation Warning" dialog
obj.click_on_validation_warning_confirmation_button(log_file)

#Again locate "New Registration" string, to make sure that dialog was closed
if obj.locate_new_registration_button(log_file):
    Collect_log.simple_log(log_file, "Info:\t\t\"New Registration\" button is available. Validation warning dialog closed.")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)



