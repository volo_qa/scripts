#Owner:Naira
# coding=utf-8
# Order Creation
#
# Steps to reproduce:
# 0. Create any payable OIT(Copies) and process it up  to Add Payment
# 1. Click Payment Method dropdown list, choose Cash
# 2. Enter half of balance owed value to Amount field
# 3. Click Payment Method link
# 4. Click new row Payment Method dropdown list, choose Check, enter Transaction Id and enter balance owed value to Amount field
# 5. Delete firts row
# 6. Click Payment Method link
# 7. Click new row Payment Method dropdown list, choose Cash and enter balance owed value to Amount field
# 8. checkout the order
#
# Expected result:
# 1.
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    OIT = "Copies"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"
    # NumOfPages = "1"
    # payment_method = "Cash"

    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)
    # obj.Click_OrderItem_Tab(log_file)

    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)
    obj.go_to_payment_screen(log_file)  # If making OIT with Account name, Assumed Name is Checked out without going into payment screen!!!
    # select Cash payment Method for the first method


    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

obj.select_payment_method('Cash', log_file)
# calculate Balance_due and set part to the first amount part to the second one.
balance_due = obj.balance_due_amount(log_file)
total_due = re.search('\d+\.\d+', balance_due).group(0)
total_due_float = float(total_due)
total_due_int = int(total_due_float)
balance_1 = int(total_due_int) / 2
balance_2 = total_due_float - balance_1
obj.fill_amount_by_value(str(balance_1), log_file)
obj.click_on_add_payment_method_btn(log_file)
# select Check Payment Method for second one
obj.select_payment_method('Check', log_file, 2)
obj.fill_amount_by_value(str(balance_2), log_file, 2)
obj.fill_transaction_id_field("transactionID", log_file, 2)
# delete first method
obj.click_on_delete_payment_method_btn(log_file)
# add second one for checkout
obj.click_on_add_payment_method_btn(log_file)
obj.select_payment_method('Cash', log_file,2)
obj.fill_amount_by_value(str(balance_1), log_file,2)
# click on Chekcout button to checkout order Payment
obj.click_on_checkout_button_in_AddPayment(log_file)

obj.locate_order_finalization_breadcrumb(log_file)
golden_breadcrumb_value = "Order Finalization"
breadcrumb_value = obj.return_order_finalization_breadcrumb()
Validate.diffutile(golden_breadcrumb_value, breadcrumb_value, log_file, "Breadcrumb Value")

obj.locate_order_number_string(log_file)
order_number_string = obj.return_order_number_string()
Validate.check_isNumber(order_number_string, log_file, "Order Number")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
