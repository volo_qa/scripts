# coding=utf-8
#Owner:Nelli

'''
Steps:
1. Navigate to CRS
2. Create any order except Account Payment
3. Process it up to Order payment screen
4. Check payment methods ddl content

Results:  
Dropdown list  is not empty, contains payment methods.


'''
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...


Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    OIT = "Copies"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"
    value = "Cash"

    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)
    obj.go_to_payment_screen(log_file)

    # get payment methods list
    payment_method_list = obj.get_all_payment_methods(log_file)


#---------------Read log_file-----------------#
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)



Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

#check that payment method ddl is not empty
if len(payment_method_list) == 0:
    Collect_log.simple_log(log_file, "Error:\t\tPayment Method ddl is empty, it's configuration issue.")
else:
    Collect_log.simple_log(log_file, "Info:\t\tPayment Method ddl is not empty.\nInfo:\t\tPayment Method ddl contains"+str(payment_method_list).strip('u')+" payments.")


#---------------Read log_file-----------------#
file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
