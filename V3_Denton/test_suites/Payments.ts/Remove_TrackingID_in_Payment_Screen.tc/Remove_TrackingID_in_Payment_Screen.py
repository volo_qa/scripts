#Owner:Arpine
#
# Steps to reproduce:
# 1. Create an order "Federal Tax Lien"
# 2. Add tracking ID in order Summary page and remember it
#
# Expected result:
# 1. Remove tracking ID in order payment screen
# 2. Verify that order with the given tracking ID is not found
#
import os
from CRS_Lib import Actions, Config, Collect_log, Validate
import shutil
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

# Store helper variables
OIT = "Federal Tax Lien"
customerName = "Test Name"
trackingID = str(startTime.day) + str(startTime.hour) + str(startTime.minute) + str(startTime.second)
createdOrderNumberString=''
createdDocNumberString=''
try:
    # Select creating order item from the list in New order screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in all required fileds for the selected order item
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS", log_file)
    obj.InputCustomerName(customerName, log_file)
    obj.Click_AddToOrder(log_file)

    # Add tracking ID
    obj.click_on_add_tracking_id(log_file)
    obj.locate_trackingID_input(log_file)
    obj.insert_trackingId_value(log_file, trackingID)
    obj.click_on_submit_button_on_addTrackingID(log_file)

    # Get order number string
    obj.locate_order_number_string(log_file)
    createdOrderNumberString = obj.return_order_number_string()
    Validate.check_isNumber(createdOrderNumberString, log_file, "Order Number")

    # Go to search tab and make search with tracking ID to make sure that it was inserted
    obj.click_search_tab(log_file)
    obj.click_on_more_less_options_btn(log_file)

    # Do search with Tracking ID
    obj.fillIn_trackingID_option_filed_value(trackingID, log_file)
    obj.click_search_btn(log_file)

    # Get founded order number
    foundedOrderNumber = obj.return_found_order_number()
    Validate.diffutile(createdOrderNumberString, foundedOrderNumber, log_file, "Founded order number with search via \"Tracking ID\":" + trackingID)

    # Click on edit button and go to payment screen
    obj.locate_edit_order_btn(log_file)
    obj.click_on_edit_order_btn(log_file)
    obj.go_to_payment_screen(log_file)

    # Remove tracking ID in payment screen
    obj.click_on_remove_trackingID(log_file)

    # Finalize the order
    obj.go_to_order_finalization('Cash', log_file)

    # Open log file and get it's content
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    # check if found any error in run_out.log file:
    # if yes, it means that TC failed. Leave TC!
    if re.search("Error:", string, re.I):
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# we done with run part
# it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

# Again go to search tab and make search with tracking ID to make sure that it was removed
obj.click_search_tab(log_file)
obj.click_on_more_less_options_btn(log_file)

# Do search with Tracking ID
obj.fillIn_trackingID_option_filed_value(trackingID, log_file)
obj.click_search_btn(log_file)

# Make sure that "No Match found" string is printed
if obj.locate_no_match_found_string(log_file):
    Collect_log.simple_log(log_file, "Info:\t\t\"No match found\" string found.\t")
else:
    Collect_log.simple_log(log_file, "Error:\t\tSome order was founded with search via Tracking ID:\t" + trackingID)

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

