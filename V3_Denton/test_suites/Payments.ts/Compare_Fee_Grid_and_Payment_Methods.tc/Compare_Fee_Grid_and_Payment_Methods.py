#Owner:Arpine
#
# Steps to reproduce:
# 1. Create an order "Copies"
# 2. Go to order payment screen
# 3. Select all payment types
# 4. Insert Cash payment method amount more than balance due
# 5. Store payment methods and their amount in dictionary
#
# Expected result:
# 1. Validate that all inserted payment types are identical with fee greid
# 2. Validate that "Change Due" amount is shown in fee grid
# 3. Make sure that Checkout button is active and finalize the order
# 4. Locate order finalization string
import os
from CRS_Lib import Actions, Config, Collect_log, Validate
import shutil
from datetime import datetime
import re
import sys

obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

# Store helper variables
trackingID = str(startTime.day) + str(startTime.hour) + str(startTime.minute) + str(startTime.second)
createdOrderNumberString=''
createdDocNumberString=''
insertedPaymentMethodsDict={1:['Cash', '150.00'], 2: ['Cashiers Check', '100.00'], 3:['Check', '100.00'], 4:['Company Account', '100.00'], 5:['LegalEase', '100.00'], 6:['Money Order', '100.00'], 7:['VitalChek', '100.00']}
paymentMethodDictInFeeGrid = {}
try:
    OIT = "Copies"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    # Select creating order item from the list in New order screen
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # Fill in all required fileds for the selected order item
    obj.Input_NumberOfPages(700,log_file)
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)

    # Go to payment screen and select all payment types
    obj.go_to_payment_screen(log_file)
    for i in [1,2,3,4,5,6,7]:
        # Get payment method and filling ammount
        payment_method = insertedPaymentMethodsDict[i][0]
        payment_amount = insertedPaymentMethodsDict[i][1]
        obj.select_payment_method_and_fill_ammount(payment_method, payment_amount, i, log_file)
        # Fill in transaction ID
        obj.fill_transaction_id_field(i, log_file, i)

        # Get payment method and amount from payment grid
        paymentMethodDictInFeeGrid[i]=[obj.return_payment_method_in_fee_grid(log_file, i), obj.return_payment_amount_in_fee_grid(log_file, i)]
        if i != 7:
            obj.click_on_add_payment_method_btn(log_file)

    # Open log file and get it's content
    file = open(log_file, 'r')
    string = file.read()
    file.close()

    # check if found any error in run_out.log file:
    # if yes, it means that TC failed. Leave TC!
    if re.search("Error:", string, re.I):
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

# we done with run part
# it's time for validation
Collect_log.simple_log(log_file, "------------------ Validation Part ---------------")

# We need to compare inserted payment methods with their appropriates in fee grid
for i in [1,2,3,4,5,6,7]:
    # Get 'Cash' payment method
    Validate.diffutile(insertedPaymentMethodsDict[i][0], paymentMethodDictInFeeGrid[i][0], log_file, "\"%s\" payment method comparison"%insertedPaymentMethodsDict[i][0])
    Validate.diffutile('$'+insertedPaymentMethodsDict[i][1], paymentMethodDictInFeeGrid[i][1], log_file, "\"%s\" payment method amount comparison"%insertedPaymentMethodsDict[i][0])

# Compare change due text, it should be 50
Validate.diffutile('50.00', obj.return_change_due_in_fee_grid(log_file), log_file, "\"Change due\"  amount comparison")

# Checkout the order, Checkout button should be active
obj.click_on_checkout_button_in_AddPayment(log_file)

# Get order number
obj.locate_order_number_string(log_file)
order_number_string = obj.return_order_number_string()
Validate.check_isNumber(order_number_string, log_file, "Order Number")

# Locate Finilized string in order finalization page
obj.locate_order_status(log_file)
golden_order_status = "Finalized"
order_status = obj.return_order_status()
Validate.diffutile(golden_order_status, order_status, log_file, "Order Status in Web Page")

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string, re.I):  # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)

