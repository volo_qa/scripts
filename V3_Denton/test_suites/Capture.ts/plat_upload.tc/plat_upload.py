#Owner:Nelli

import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

#objects
obj = Actions()
config = Config()
config.parse_config()

pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...


Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")
try:
    #variables
    OIT = "Plats"
    account_name = "4815 - mher.simonyan@volo.global"
    symbol = "4815"

    #methods
    obj.AddNewOrder(log_file)
    obj.Initialize(log_file)
    obj.select_OIT(OIT, log_file)

    # create object of required field
    required_obj = Fill_in_required_filds(obj.driver)
    required_obj.crs_fill("CRS",log_file)

    #finalize order
    obj.InputAccountName(account_name, log_file, symbol)
    obj.Click_AddToOrder(log_file)
    obj.go_to_payment_screen(log_file)
    obj.go_to_order_finalization('Cash', log_file)

    # take order number
    obj.locate_order_number_string(log_file)
    ordernumber = obj.return_order_number_string()

    #wait for spiner disappearing
    obj.wait_loader_spiner_to_be_absent(log_file)

    #go to Capture Queue
    obj.click_on_capture_queue_tab(log_file)

    #upload image process
    obj.click_on_upload_icon(ordernumber, log_file)
    obj.click_on_death_folder(log_file)
    obj.select_image(log_file)

    #wait for spiner disappearing
    obj.wait_loader_to_be_absent_uploadpopup(log_file)
    obj.click_on_upload_button(log_file)
    obj.wait_loader_to_be_absent_uploadpopup(log_file)

    #wait for capture queue loader spiner disappers
    obj.wait_loader_spiner_to_be_absent(log_file)

    #find order in order search and verify that order wwent to Indexing Queue
    obj.click_search_tab(log_file)
    obj.fill_order_number(ordernumber, log_file)
    obj.click_search_btn(log_file)

# read log_file
    file = open(log_file, 'r')
    string = file.read()
    file.close()

#searching for error on log_file
    if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
        Collect_log.run_part_status(log_file, "FAIL")
        Collect_log.test_case_exit(log_file, startTime, obj.driver)
    else:
        Collect_log.run_part_status(log_file, "PASS\n")

except Exception as e:
    print e

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)



Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")

#validate that searched result gives the right order number
obj.locate_found_order_number(log_file)
golden_found_order_number = ordernumber
found_order_number = obj.return_found_order_number()
Validate.grep_golden_in_actual(golden_found_order_number, found_order_number, log_file, "Found Order Number String")

#validate that workflow step is Indexing
obj.locate_workflowstep_name(log_file)
golden_archive_string = "Indexing"
workflowstep_name = obj.return_workflowstep_name()
Validate.grep_golden_in_actual(golden_archive_string, workflowstep_name, log_file, "Found Workflow step String")


#open log_file for read
file = open(log_file, 'r')
string = file.read()
file.close()

#search for error on log_file
if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)