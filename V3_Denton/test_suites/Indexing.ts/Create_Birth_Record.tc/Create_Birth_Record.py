#Owner:Naira
# coding=utf-8
# Order Creation
#
# Steps to reproduce:
# 1.
#
# Expected result:
# 1.
#
import os
import time
from colorama import Fore
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import subprocess
from CRS_Lib import Actions, Explicit_Wait, Config, Login, Collect_log, Validate, DB_connect, OrderSearch
import shutil                                                               # for copy file
from datetime import datetime
import re
from Required_fileds import Fill_in_required_filds
import sys

obj = Actions()
config = Config()
config.parse_config()


pwd = os.getcwd()
if len(sys.argv) > 1:
    log_file = sys.argv[1]
else:
    test_case_dir = os.path.basename(__file__).replace("py","tc")  # Read Script file name and change scriptName.py to scriptName.tc
    if not os.path.exists(test_case_dir):
        os.mkdir(test_case_dir)  # Make test case dir scriptName.tc

    log_file = str(pwd) + "\\" + test_case_dir + "\\run_out.log"

    if os.path.exists(log_file):
        shutil.copy2(log_file, log_file.replace(".log","") + "_copy.log")
        os.remove(log_file)

startTime = datetime.now()
Collect_log.start_time(log_file)
Collect_log.simple_log(log_file, "Test Case: " + os.path.basename(__file__) + "\n" )

obj.driver.maximize_window()
obj.login_to_crs( config.crs_urls[0] )  #0-local, 1-crs.qa, ...

Collect_log.simple_log(log_file,"--------------------- Run Part -------------------")

try:
    # Navigate to indexing Queue
    obj.navigate_to_indexing_tab(log_file)
    # Click on Add new Button and select Birth Records
    obj.click_on_add_new_indexing_task(log_file)
    obj.select_Birth_Record_doc_type(log_file)
    # click on "Submit" Button from opened dialog
    obj.click_on_add_new_task_submit_btn(log_file)
    # fill required fields
    required_obj = Fill_in_required_filds(obj.driver)
    docNum = required_obj.fill("CRS", log_file)

    obj.click_on_Save_btn(log_file)
    obj.click_yes_from_warming_box(log_file)



    # file = open(log_file, 'r')
    # string = file.read()
    # file.close()
    #
    # if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    #     Collect_log.run_part_status(log_file, "FAIL")
    #     Collect_log.test_case_exit(log_file, startTime, obj.driver)
    # else:
    #     Collect_log.run_part_status(log_file, "PASS\n")
    #
except Exception as e:

    Collect_log.run_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)

Collect_log.simple_log(log_file,"------------------ Validation Part ---------------")


time.sleep(3)
#open database connection
db_con = DB_connect.connection_to_db("LocalDenton")
Validate.check_existence_of_document_in_db(db_con[1],str(docNum['doc_num']),str(docNum['doc_num']),log_file)

file = open(log_file, 'r')
string = file.read()
file.close()

if re.search("Error:", string,re.I):    # if found any error in run_out.log file:
    Collect_log.validate_part_status(log_file, "FAIL")
    Collect_log.test_case_exit(log_file, startTime, obj.driver)
else:
    Collect_log.validate_part_status(log_file, "PASS")
    Collect_log.simple_log(log_file, "\nTest case PASS")
    Collect_log.test_case_exit(log_file, startTime, obj.driver, True)
