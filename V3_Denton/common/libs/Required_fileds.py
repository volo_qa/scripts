import time
from CRS_Lib import Validate
from CRS_Lib import Explicit_Wait
from CRS_Lib import GetFieldValues
from selenium.webdriver.common.keys import Keys
import re
import random
import datetime




class Fill_in_required_filds(object):

    def __init__(self, driver):
        self.driver = driver
        self.FieldValue = GetFieldValues()

    def fill(self,env, log_file):
        returned_dict = {'serial_num':'', 'doc_num':''}

        if env == "CRS":
            checkboxes = self.driver.find_elements_by_xpath("//div[contains(@style,'display: block')]//input[@type='checkbox']")
        elif env == "eFrom":
            checkboxes = self.driver.find_elements_by_xpath(".//input[@type='checkbox']")
        else:
            checkboxes = self.driver.find_elements_by_xpath("//input[@type='checkbox']")

        if checkboxes:
            # print "There are chexboxes"
            for checkbox in checkboxes:
                if checkbox.is_displayed():
                    checkbox.click()
                    if "ret-addr-ret-by-mail" in checkbox.get_attribute("class"):
                        time.sleep(1)


        if env == "CRS":
           # // div[contains( @ style, 'display: block')]
            required_fields = self.driver.find_elements_by_xpath("//*[contains(@class,'koValidationError')]")
        elif env == "eFrom":
            required_fields = self.driver.find_elements_by_xpath("//*[contains(@class,'koValidationError')]")
        else:
            # below list is the same as CRS  - YET
            required_fields = self.driver.find_elements_by_xpath("//div[contains(@style,'display: block')]//*[contains(@class,'koValidationError')]")

        if required_fields:
            # print "There are required fileds" + str(len(required_fields))
            for field in required_fields:
                # print str(field.tag_name) + str(field.get_attribute("placeholder"))
                if field.is_displayed():
                    time.sleep(0.05)
                    self.FieldValue.get_field_values()
                    if field.tag_name == "input":
                        if re.search("First.*Name", str(field.get_attribute("placeholder")) + str(field.get_attribute("id")), re.I):
                            field.send_keys(self.FieldValue.name)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file,"\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("radio", field.get_attribute("type"), re.I):
                            field.click()
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+   str(field.get_attribute("Value"))+ "\" radioButton ")
                        elif re.search("Last.*Name", str(field.get_attribute("placeholder")) + str(field.get_attribute("id")), re.I):
                            field.send_keys(self.FieldValue.surname)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("Zip", field.get_attribute("name"),re.I): # sup. only for eforms
                            # field.send_keys(997700011)
                            field.send_keys(99770)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                            # field.send_keys(997)
                        elif re.search("City", str(field.get_attribute("placeholder")) + str(field.get_attribute("name")), re.I):
                            field.send_keys("Rouen")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("Addr", str(field.get_attribute("placeholder")) + str(field.get_attribute("id")), re.I):
                            field.send_keys("16 Wall Street, New York, NY, United States")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("County", str(field.get_attribute("placeholder")) + str(field.get_attribute("name")), re.I):
                            time.sleep(1)
                            field.send_keys("Normandie")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("Country", str(field.get_attribute("placeholder")) + str(field.get_attribute("name")), re.I):
                            field.send_keys("France")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("Phone", field.get_attribute("placeholder"), re.I):
                            field.send_keys("(095)555-4444")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("mail", str(field.get_attribute("placeholder")) + str(field.get_attribute("id")), re.I):
                            field.send_keys(self.FieldValue.name+"@somemail.com")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("calendar", field.get_attribute("class"),re.I):
                            if re.search("(Identification.*Expiry)|i?d?.*exp.*i?d?", field.get_attribute("name"),re.I):  #  ID exp date - calendar field
                                field.send_keys("01/01/2018")
                            else:
                                field.send_keys("01/01/1980")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Volume", field.get_attribute("class"),re.I):
                            field.send_keys(2)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Page", field.get_attribute("class"),re.I):
                            field.send_keys(10)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Business", str(field.get_attribute("class")) + str(field.get_attribute("name")),re.I):
                            field.send_keys(self.FieldValue.name + self.FieldValue.surname + 'Corp' + str(random.randint(1000, 9999)))
                            field.send_keys(Keys.TAB)
                            time.sleep(1)
                            Explicit_Wait.wait_for_fields_lookup(field, log_file)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                            continue
                        elif re.search("NoOf", field.get_attribute("id"),re.I):
                            field.send_keys(3)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("SSN", field.get_attribute("placeholder"),re.I):
                            field.send_keys(str(88) + str(random.randint(1000000, 9999999)))
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        # elif re.search("doc.*number", str(field.get_attribute("id")) + str(field.get_attribute("name")),re.I): # Redaction Request: Doc# is taken from "id",  Search Request: Doc# is taken from name
                        #     field.send_keys(21)
                        #     field.send_keys(Keys.TAB)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Year", str(field.get_attribute("id")) + str(field.get_attribute("name")),re.I):  # initially was  "doc.*Year"
                            field.send_keys(2016)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Relation", field.get_attribute("name"),re.I):
                            field.send_keys("Uncle")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("id.*(Type|Number)", field.get_attribute("name"),re.I):
                            field.send_keys("IdentityCard6495")
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Name", field.get_attribute("placeholder"),re.I):
                            field.send_keys(self.FieldValue.name +" " + self.FieldValue.surname)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        elif re.search("--", field.get_attribute("class"),re.I):
                            field.send_keys(1)
                            field.send_keys(Keys.TAB)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                        #
                        # elif re.search("--", field.get_attribute("class"),re.I):
                        #     field.send_keys(1)
                        #     Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")

                        elif re.search("Serial Number", str(field.get_attribute("placeholder"))) :
                            serialNumber=''
                            while re.search("koValidationError",str(field.get_attribute("class"))):
                                dateTime = datetime.datetime.now()
                                serialNumber = str(dateTime.year) + str(dateTime.month) + str(dateTime.day) + str(dateTime.hour) + str(dateTime.minute) + str(dateTime.second)
                                field.send_keys(serialNumber)
                                time.sleep(1)
                            Explicit_Wait.wait_for_fields_lookup(field)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                            # Need serial number string for later search
                            returned_dict['serial_num'] = serialNumber

                        elif re.search("Doc Number", str(field.get_attribute("placeholder"))):
                            docNumber=''
                            while re.search("koValidationError",str(field.get_attribute("class"))):
                                dateTime = datetime.datetime.now()
                                docNumber =str(dateTime.month) + str(dateTime.day) + str(dateTime.hour) + str(dateTime.minute)
                                field.send_keys(docNumber)
                                field.send_keys(Keys.TAB)
                                returned_dict['doc_num'] = docNumber
                                time.sleep(1)
                            Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(field.get_attribute("placeholder"))+ "\" field")
                            # Need serial number string for later search


                        else:
                            field.send_keys("112233445")
                    elif field.tag_name == "select":
                        field.click()
                        field.send_keys(Keys.ARROW_DOWN)
                        field.send_keys(Keys.ENTER)
                        field.send_keys(Keys.TAB)
                        fieldFullName = field.get_attribute("name")
                        FieldName = fieldFullName.split('.')
                        Validate.check_required_field_info(re.search("koValidationError", field.get_attribute("class"), re.I), log_file, "\" "+ str(FieldName[-1])+ "\" field")
        return returned_dict
    def crs_fill(self,env, log_file):

        tabs = self.driver.find_elements_by_xpath("//a[@class='error']")
        if tabs:
                for tab in tabs:
                    tab.click()
                    self.fill(env, log_file)
