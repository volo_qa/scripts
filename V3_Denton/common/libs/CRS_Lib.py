'''
---------------------------------------All POM_s in CRS_Lib----------------------------------------------
Config
eForms
GetFieldValues
Login
OrderQueue
InitializeDrawer
BalanceDrawer
Initialization
OrderEntry
OrderSummary
AddPayment
OrderFinalization
VoidOrderSummary
VoidOrderPayment
CaptureQueue
CaptureSummary
UploadPopup
IndexingQueue
IndexingSummary
IndexingEntry
VerificationQueue
OrderSearch
FrontOffice
NavigateToCRS
Actions
Validate
Explicit_Wait
Collect_log
DB_connect
'''

from selenium import webdriver
from selenium.webdriver.common.by import By
import subprocess
from selenium.webdriver.support.ui import Select
import time
import re
import random
import sys
from colorama import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from datetime import datetime
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import autoit
import os.path
import xml.etree.ElementTree as ET
import cursor

class Config():
    crs_urls = []
    eForms_urls = []
    db_con_list = []
    all_env_names = []
    # current_version = '3'
    # config_file = "C:/Python27/lib/Config.txt"
    config_file = "C:/Python27/lib/Configuration.xml"
    # config_file = "E:\Kofile_VS\Kofile\V3_Denton\common\configs\Configuration.xml"

    def __init__(self):
        pass

    def parse_config2(self):
        # config_file = "../Config.txt"
        # config_file = "C:/Python27/lib/Config.txt"
        with open(Config.config_file) as file:
            config = file.readlines()

        for line in config:
            if re.search("^Environment.*\:\s*.*", line):
                self.crs_urls.append(re.search("Environment.?\:\s*(.*)", line).group(1))
            elif re.search("^eForm.*\:\s*.*", line):
                self.eForms_urls.append(re.search("eForm.?\:\s*(.*)", line).group(1))

    # crs_urls = []
    # eForms_urls = []

    def parse_config(self):
        # static variables

        is_version_found = False
        is_TenantConfig_found = False

        flag = True

        env_name = ""
        environment_url = ""

        # three = ET.parse(Config.config_file)
        #parsing Configuration.xml file
        root = ET.parse(Config.config_file).getroot()
        for VersionConfig in root.findall('VersionConfig'):

            for TenantConfig in VersionConfig:
                if TenantConfig.attrib['value'] == 'default':

                    if flag:
                        #print TenantConfig.attrib['TenantName'], TenantConfig.attrib['Url']

                        env_name = TenantConfig.attrib['TenantName']
                        environment_url = TenantConfig.attrib['Url']
                        self.crs_urls.append(environment_url)

                        # if at least one have 'default' value then flag will be set to False
                        flag = False
                    else:
                        # Will exit in case there are 2 or more 'default' values set by chanse, in xml configuration file
                        print "Error: There is more than one TenantConfig with 'default' value in configuration xml file.\n       Please correct and run again."
                        exit(0)
                else:
                    is_TenantConfig_found = True

                #DB cofigs parsing part
                tenant_name = TenantConfig.attrib['TenantName']
                tenant_code = TenantConfig.attrib['TenantCode']
                server_ip= TenantConfig.getchildren()[1].attrib['SERVER']
                database_name = TenantConfig.getchildren()[1].attrib['DATABASE']
                uid = TenantConfig.getchildren()[1].attrib['UID']
                password =TenantConfig.getchildren()[1].attrib['PASSWD']
                Config.db_con_list.append([tenant_name, tenant_code, server_ip, password, uid, database_name])

            #list of all environment_names
            for i in Config.db_con_list:
                Config.all_env_names.append(i[0])
                # Config.env_name.append(Config.db_con_list[0])
                # print Config.env_name


            # Will exit the script in case  <TenantConfig value='default'>  is not found in config xml
            if flag:
                    print "  Error : TenantConfig with 'default' value doesn't exist. Couldn't get environment URL."
                    print "   Info : In order to get environment URL. Configuration.xml should contain at least one '<TenantConfig' tag which have 'value' attribute equal to 'default'."
                    print "Example : <TenantConfig value='default' TenantName='Denton' TenantCode='48121' Url='http://crs.qa.kofile.com/48121'>"
                    exit(0)

            # if current version Tag is found, will not look for further versions
            is_version_found = True
            break

        return env_name, environment_url

    # @staticmethod
    # def get_all_envionments():
    #     name_plus_url = []
    #
    #     root = ET.parse(Config.config_file).getroot()
    #     for VersionConfig in root.findall('VersionConfig'):
    #         # Tries to find current version in all <VersionConfig> xml Tags.
    #         if VersionConfig.attrib['version'] == Config.current_version: # '3'
    #             for TenantConfig in VersionConfig:
    #                 env_name = TenantConfig.attrib['TenantName']
    #                 environment_url = TenantConfig.attrib['Url']
    #                 name_plus_url.append(env_name+" - "+environment_url)
    #
    #     if not name_plus_url:
    #         print "Error: Something went wrong. Couldn't get environment NAME and/or URL for Current \"" + Config.current_version + "\" version from <TenantConfig> tag from conf xml file."
    #         print " Info: Please check the config and run again. Exiting..."
    #         exit(0)
    #
    #     return name_plus_url

    @staticmethod
    def get_env_name(file1=config_file):
        # config_file = CRS_Lib.config_file
        with open(file1) as file:
            config = file.readlines()

        # print "CONFIG:", config
        env_name=""
        environment_url=""

        for line in config:
            # We need to change only lines wich contains environment
            if re.search("^Environment.*\:\s*.*", line):
                environment_url = re.search("^Environment.?\:\s*(.*)", line).group(1)
                print "environment_url = ", environment_url

                if (environment_url == "http://10.0.1.73:8002/48121"):
                    env_name = "V3_Denton_local"
                    print "env_name = " + env_name
                    break
                elif (environment_url == "http://crs.qa.kofile.com/48121"):
                    env_name = "V3_Denton"
                    print "env_name = " + env_name
                    break
                elif (environment_url == "http://10.0.1.73:8002/48215"):
                    env_name = "V3_Hidalgo_local"
                    print "env_name = " + env_name
                    break
                elif (environment_url == "http://crs.qa.kofile.com/48215"):
                    env_name = "V3_Hidalgon"
                    print "env_name = " + env_name
                    break
                else:
                    env_name = ""
                    print "Could not find \"", env_name, "\" environment."
                    exit(0)

        return env_name, environment_url



# Config.format_checker()

# print Config.get_all_envionments_names()


# obj = Config()
# obj.parse_config()
# print obj.crs_urls


class eForms(object):

    #variables
    submit_btn_xpath                   = (By.XPATH, ".//input[@value='Submit']")
    Order_Confirmation_header_title    = (By.XPATH, ".//div[contains(text(),'Submission Successful')]")
    Order_number_xpath                 = (By.XPATH, ".//*[@id='OrderNumber']")
    order_number                       = None

    #methods
    def __init__(self):
        # self.driver = webdriver.Firefox()
        # binary = FirefoxBinary(r"F:\QA Test\FirefoxPortable\FirefoxPortable.exe")

		# FireFoxPortable Support:
        # binary = FirefoxBinary(r"E:\FirefoxPortable\FirefoxPortable.exe")
        # self.driver = webdriver.Firefox(firefox_binary=binary)

		# Google Chrome Support:
        self.driver = webdriver.Chrome("C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe")
        # self.driver = webdriver.Chrome()
    def navigate_to(self, env_url):
        driver = self.driver
        driver.get(env_url)
        # driver.maximize_window()

    def submit(self):
        Explicit_Wait.wait_for_element_to_be_present(self.driver, 3, eForms.submit_btn_xpath)
        Explicit_Wait.wait_for_element_clickable(self.driver, 8, eForms.submit_btn_xpath).click()
        Explicit_Wait.wait_for_element_to_be_present(self.driver, 8, eForms.Order_Confirmation_header_title)
        self.order_number = 10000000000000
        self.order_number = Explicit_Wait.wait_for_element_to_be_present(self.driver, 5, eForms.Order_number_xpath).text


    def tearDown(self):
        self.driver.close()


class GetFieldValues():

    #variables
    names_list        = []
    Surnames_list     = []
    name              = None
    surname           = None

    #methods
    def __init__(self):
        self.all_names = None
        self.all_surnames = None

    def get_field_values(self):
        with open("C:/Python27/lib/fields_values") as file:
            values = file.readlines()
        for line in values:
            if re.search("^Name.*\:\s*.*", line):
                self.all_names = re.search("^Name.*\:\s*(.*)", line).group(1)
                self.names_list = self.all_names.split(" ")
            if re.search("^Surname.*\:\s*.*", line):
                self.all_surnames = re.search("^Surname.*\:\s*(.*)", line).group(1)
                self.surnames_list = self.all_surnames.split(" ")
        if self.names_list:
            self.name = self.names_list[random.randint(0, len(self.names_list) - 1)]
        if self.surnames_list:
            self.surname = self.surnames_list[random.randint(0, len(self.surnames_list) - 1)]


class Login(object):

    username        = "User3{TAB}"
    password        = "Volo2020C{Enter}"       # If Password contains !, #, +, ^, {, } or space these symbold should be written  {!}, {#}, {+}, {^}, {{}, {}} or {space}
    # username      = "msimonyan{TAB}"         # For CRS.QA server
    # password      = "KYwkg5Xnnf{Enter}"      # For CRS.QA server

    def __init__(self):
        # self.driver = webdriver.Firefox()
        # self.driver = webdriver.Ie("IEDriverServer.exe")
        # binary = FirefoxBinary(r"F:\QA Test\FirefoxPortable\FirefoxPortable.exe")

        # FireFox Portable Support:
        # binary = FirefoxBinary(r"E:\FirefoxPortable\FirefoxPortable.exe")
        # self.driver = webdriver.Firefox(firefox_binary=binary)

        # Google Chrome Support:
        self.driver = webdriver.Chrome("C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe")
        # self.driver = webdriver.Chrome()


    # def setUp(self, exe_file, env_url):
    def setUp(self, env_url, exe_file):
        driver = self.driver
        # time.sleep(5) # TODO - what is this ??? try to remove
        time.sleep(1) # TODO - new added only for Chrome

        #For FireFox:
        # subprocess.Popen(exe_file) # uncomment

        #For IE:
        # subprocess.Popen("User3IE.exe") # Changed - Raf
        driver.get(env_url)
        driver.switch_to.window(driver.current_window_handle)
        # driver.maximize_window()

        # For Google Chrome
        autoit.send(Login.username)
        autoit.send(Login.password)

    def tearDown(self):
        self.driver.close()

    @staticmethod
    def driver_close(driver, file_path=""):
        try:
            driver.close()
            # Collect_log.log(file_path, 'a+', "Browser closed successfully", 0)
        except Exception as e:
            # Collect_log.log(file_path, 'a+', "Could not close browser." + '\n' + e, 1)
            exit(0)

    @staticmethod
    def driver_quit(driver, file_path=""):
        try:
            driver.quit()
        except Exception as e:
            exit(0)


class OrderQueue(Login):

    # all page's attributes
    order_queue_tab_xpath       = (By.XPATH, ".//*[@id='orders']/a")
    order_queue_title_xpath     = (By.XPATH, ".//*[@id='results-table']/div/div[3]/h3")
    initialize_drawer_xpath     = (By.XPATH, ".//*[@id='initializeDrawer']/a")
    balance_drawer_xpath        = (By.XPATH, ".//*[@id='balanceDrawer']/a")
    add_new_order_id            = (By.ID, 'addNewOrder')
    administrative_id           = (By.ID, 'icon-key')
    next_order_id               = (By.ID, 'icon-nextorder')
    administrative_btn_id       = (By.XPATH, ".//a[contains(@class,'icon-admin-key')]")
    assignNames_xpath           = (By.XPATH,".//*[@id='assignNames_chosen']/a")  # click
    # assignNames_select        = (By.XPATH,".//*[@id='aassignNames']")  # click
    assignNames_input_xpath     = (By.XPATH, ".//*[@id='assignNames_chosen']/div/div/input")
    # assignNames_lookup_xpath  = (By.XPATH, ".//*[@id='assignNames_chosen']/a")
    assignNames_lookup_xpath    = (By.XPATH, ".//*[@id='assignNames_chosen']/div/ul/li[1]")

    assignBubble_xpath          = (By.XPATH, ".//*[@id='assignBubble']")
    assignBubble_orderNum_xpath = (By.XPATH, ".//*[@id='orderNum']")
    add_xpath                   = (By.XPATH,".//*[@id='addassignedtoBtn']")
    cancel_xpath                = (By.XPATH,".//*[@id='widget-kofileinfobubble-cancelui-id1']")

    i = ""
    order_number_xpath          = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[%s]/td[4]/span")
    order_assigned_to_xpath     = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(i) + "]/td/input")
    refresh_queue_xpath         = (By.XPATH, ".//a[contains(@class,'icon-refresh-custom')]")

    order_in_actionReasonsBubble_xpath  = (By.XPATH, ".//*[@id='order']")
    enter_reason_xpath          = (By.XPATH, ".//*[@id='actionReason']")
    enter_description_xpath     = (By.XPATH, ".//*[@id='actionDescription']")
    submit_xpath                = (By.XPATH, ".//*[@id='actionReasonsBtn']")
    reject_action_template_xpath= (By.XPATH, ".//*[@id='actionTemplate']")

    order_queue_loader_xpath    = (By.XPATH, "html/body/div[3]/div")
    ordere_queue_quantity       = (By.XPATH, ".//*[@id='orders']/a/span")

    order_assignedTo_for_specified_order = (By.XPATH, "//*[contains(text(),'%s')]/following::td[6]")

    # TD 10 /span).text should be Cancelled


    def __init__(self, driver):
        Login.__init__(self)

    # def AddNewOrder(self):
    #     add_new_order = self.driver.find_element(*OrderQueue.add_new_order_id)
    #     add_new_order.click()

    def is_in_order_queue(self):
        Explicit_Wait.wait_for_element_visibility(self, OrderQueue.order_queue_title_xpath)

    def click_Initialize_Drawer(self,log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, OrderQueue.initialize_drawer_xpath[1], "\"Initialize Drawer\" click.", log_file)

    def click_Balance_Drawer(self,log_file):    # TODO Log Just added, should be tested
        time.sleep(5)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, OrderQueue.balance_drawer_xpath[1], "\"Balance Drawer\" click.", log_file)

    def AddNewOrder(self,log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.ID, OrderQueue.add_new_order_id[1], "\"Add New Order\" button click.", log_file)

    def Administrative_button(self,log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderQueue.administrative_btn_id[1], "\"Administrative\" button click.", log_file)

    def Refresh_button(self,log_file):    # TODO Log Just added, should be tested
        time.sleep(5)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, OrderQueue.refresh_queue_xpath[1], "\"Refresh\" button click.", log_file)
        time.sleep(5) # TODO should be removed (After refresh should be used any identifier to validate that page has been refreshed but not wait some seconds)

    def running_man_button(self,row_number, log_file):    # TODO Log Just added, should be tested
        running_man_xpath = ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td[15]/a"
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, running_man_xpath, "\"Running man\" button click.", log_file)

    def order_queue_title(self,log_file):    # TODO Log Just added, should be tested
        return  Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 15, By.XPATH, OrderQueue.order_queue_title_xpath[1], "\"Reading\" Order Queue string.", log_file).text

    def Assign_icon_button(self, row_number, log_file):    # TODO Log Just added, should be tested
        Assign_icon_xpath = ".//*[@id='OrderQueue']/tbody/tr[" + row_number + "]/td[16]/a"
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, Assign_icon_xpath, "\"Assign icon\" button click.", log_file)

    def click_assignnames(self,log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, OrderQueue.assignNames_xpath[1], "\"Assign Names\" listbox click.", log_file)

    # def select_assignnames(self,log_file, assignName):    # TODO Log Just added, should be tested
    #     Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "select", 5, By.XPATH, OrderQueue.assignNames_select[1], "Select \"" + assignName + "\" listbox click.", log_file, "", "", assignName)
    #
    def InputAssignName(self, assignName, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderQueue.assignNames_input_xpath[1], "\"Assign Name \" field is writable.", log_file, "", assignName)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderQueue.assignNames_lookup_xpath[1], "\"Assign Name \"" + assignName +" lookup.", log_file, "", assignName)


    def click_add(self,log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 40, By.XPATH, OrderQueue.add_xpath[1], "\"Add\" button click.", log_file)
        time.sleep(10)

    def click_cancel(self,log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderQueue.cancel_xpath[1], "\"Cancel\" button click.", log_file)

    def read_order_number(self,row_number,log_file):    # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 25, By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td[4]/span", "", log_file).text

    def read_order_number_new(self,row_number,log_file):    # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 25, By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td[4]", "", log_file).text

    def read_order_status(self,row_number,log_file):    # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 25, By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td[10]/span", "", log_file).text

    def read_specified_order_status(self, order_num, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 25, By.XPATH, OrderQueue.order_assignedTo_for_specified_order[1]%order_num, "", log_file).text

    def read_order_assigned_to(self,row_number,log_file):    # TODO Log Just added, should be tested
        time.sleep(3)
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 25, By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td/input", "", log_file).get_attribute("value")

    #list of order_numbers
    def get_all_order_numbers(self, log_file):
        List_order_numbers = []
        order_quantity = OrderQueue.return_ordere_queue_quantity(self, log_file)
        for i in range(1, int(order_quantity)+1):
            order_number = OrderQueue.read_order_number(self, i, log_file)
            List_order_numbers.append(order_number)
        return List_order_numbers

    # def read_assignBubble_orderNum(self,log_file):    # TODO Add this validation code later
    #     return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, OrderQueue.assignBubble_orderNum_xpath[1], "", log_file).text
    #     # return Explicit_Wait.wait_for_element_not_to_be_present_and_log(self.driver, "read", 25, By.XPATH, OrderQueue.assignBubble_xpath[1], "", log_file).text

    def close_icon_button(self, row_number, log_file):  # TODO Log Just added, should be tested
        close_icon_xpath = ".//*[@id='OrderQueue']/tbody/tr[" + row_number + "]/td[19]/a"
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, close_icon_xpath, "\"Close or Cancel icon\" button click.", log_file)
        time.sleep(5)

    # Finds the first order which has "Assign to clerk" icon and clicks on it
    def find_and_click_close_icon_button(self, row_number, log_file): # TODO Log Just added, should be tested
        # Store close icon xpath
        close_icon_xpath = ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td[19]/a"

        # Get icon style. We need this because the icon may be hidden
        iconStyle = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, close_icon_xpath, "\"Close icon\" founded.",log_file).get_attribute("style")

        # Check, if assign icon is not hidden
        if iconStyle != "display: none;":
            Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 20, By.XPATH, close_icon_xpath, "\"Close icon\" clicked.", log_file)
            return True

        return False

    def read_order_in_actionReasonsBubble(self, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 15, By.XPATH, OrderQueue.order_in_actionReasonsBubble_xpath[1], "", log_file).text

    def enter_reason(self, string = "test reason", log_file=""):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderQueue.enter_reason_xpath[1], "\"Enter Reason\" field is writable.", log_file, "", string)

    def enter_reject_template(self, string = "rejectionReason", log_file=""):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderQueue.reject_action_template_xpath[1], "\"Enter template\" field is writable.", log_file, "", string)

    def enter_description(self, string = "test description", log_file=""):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderQueue.enter_description_xpath[1], "\"Enter Description\" field is writable.", log_file, "", string)

    def click_submit(self, log_file):  # TODO Log Just added, should be tested
        time.sleep(3)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderQueue.submit_xpath[1], "\"Submit\" button click.", log_file)

    # Returns all orders count, which are in Order Queue
    def find_all_orders_in_order_queue(self, log_file):
        order_xpath = ".//*[@id='OrderQueue']/tbody/tr"
        return len(self.driver.find_elements_by_xpath(order_xpath))

    # Finds an order which has "Assign to clerk" icon and clicks on it
    def find_assign_icon_button(self, row_number, log_file, assignName=""):
        Assign_icon_xpath = ".//*[@id='OrderQueue']/tbody/tr[" + str(row_number) + "]/td[16]/a"

        # Get icon style. We need this because the icon may be hidden
        iconStyle = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, Assign_icon_xpath, "\"Assign icon\" founded.",log_file).get_attribute("style")

        # Check, if assign icon is not hidden
        if iconStyle != "display: none;":
            # if current assignee is different from the given one, click on icon
            if assignName == "":
                if self.read_order_assigned_to(row_number, log_file) == "":
                    Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 20, By.XPATH, Assign_icon_xpath, "\"Assign icon\" clicked.", log_file)
                    return True
                return False
            elif self.read_order_assigned_to(row_number, log_file) != assignName:
                Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 20, By.XPATH,Assign_icon_xpath, "\"Assign icon\" clicked.",log_file)
                return True
        return False

    # Opens Orders tab
    def go_to_order_queue(self):
        orders_tab = self.driver.find_element(*OrderQueue.order_queue_tab_xpath)
        orders_tab.click()

    def click_order_queue_tab(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.order_queue_tab_xpath[1], "\"Orders\" tab click.", log_file)

    # Waits until loader element is present
    def wait_loader_to_be_absent(self, log_file):
        Explicit_Wait.wait_for_element_NOT_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderQueue.order_queue_loader_xpath[1], "Loader is absent.", log_file)

    #Orders quantity
    def return_ordere_queue_quantity(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderQueue.ordere_queue_quantity[1], "Orders quantity given.", log_file).text


        # TD 10 /span).text should be Cancelled

    # order_number_xpath = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(i) + "]/td[4]/span")
    # order_assigned_to_xpath = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr[" + str(i) + "]/td/input")


class InitializeDrawer(Login):

    #variables
    init_amount_xpath           = (By.XPATH, ".//*[@id='cashDrawerInitializationTable']/div/div[3]/div[2]/input")
    init_submit_id              = (By.ID, "submitInitializeDrawer")
    init_cancel_id              = (By.ID, "cancelinitializeDrawer")

    #methods
    def click_init_submit(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, InitializeDrawer.init_submit_id[1], "Click Submit button on Initialize Drawer Window.", log_file)

    def input_init_amount(self, value, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "clear", 5, By.XPATH, InitializeDrawer.init_amount_xpath[1], "Remove value from amount field of Initialize Drawer Window.", log_file, "")
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, InitializeDrawer.init_amount_xpath[1], "Input " + str(value) + " in amount field of Initialize Drawer Window.", log_file, "", value)


class BalanceDrawer(Login):

    #variables
    balance_drawer_xpath        = (By.XPATH, ".//*[@id='balanceDrawer']/a")
    post_difference_id          = (By.ID, "postDiffreenceSingle")
    Payment_Method_id           = (By.ID, "paymentMethod")
    Payment_Method_Post_id      = (By.ID, "postDifference")

    #methods
    def read_balance_row_label(self, row_number, log_file):  # TODO Log Just added, should be tested   9,10,11,12 with label the others without it
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, ".//*[@id='drawerSummaryClerk']/table/tbody/tr[" + str(row_number) + "]/td[1]", "", log_file).text
    def read_balance_row_string(self, row_number, log_file):  # TODO Log Just added, should be tested   9,10,11,12 with label the others without it
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, ".//*[@id='drawerSummaryClerk']/table/tbody/tr[" + str(row_number) + "]/td[1]", "", log_file).text
    def read_balance_row_expected_value(self, row_number, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, ".//*[@id='drawerSummaryClerk']/table/tbody/tr[" + str(row_number) + "]/td[2]", "", log_file).text
    def read_balance_row_actual_value(self, row_number, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, ".//*[@id='drawerSummaryClerk']/table/tbody/tr[" + str(row_number) + "]/td[3]/input", "", log_file).text
    def read_balance_row_difference_value(self, row_number, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, ".//*[@id='drawerSummaryClerk']/table/tbody/tr[" + str(row_number) + "]/td[4]/div", "", log_file).text
    def read_balance_row_button(self, row_number, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, ".//*[@id='drawerSummaryClerk']/table/tbody/tr[" + str(row_number) + "]/td[5]/a", "", log_file).text

    def click_Post_Difference(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, BalanceDrawer.post_difference_id[1],"Open Post Difference Bubble", log_file)

    def select_Payment_Method(self, PayMtd, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "select", 5, By.ID, BalanceDrawer.Payment_Method_id[1], "\"" + PayMtd + "\" Payment Method selection.", log_file, "", "", PayMtd)

    def click_Payment_Method_Post(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, BalanceDrawer.Payment_Method_Post_id[1],"Click Payment Method Post", log_file)

    # For Cancel use 'obj.click_cancel(log_file)' everyware in bubbles is same!


class Initialization(OrderQueue):

    def __init__(self, driver):
        OrderQueue.__init__(self, driver)

    #variables
    beginning_balance_id      = (By.ID, 'beginingBalance')
    initialize_button_id      = (By.ID, 'cashdrawerinitialize-btn')

    #methods
    # def Initilializ(self):
    #     try:
    #         initialize_button = self.driver.find_element(*Initialization.initialize_button_id)
    #         initialize_button.click()
    #     except NoSuchElementException:
    #         pass


    def Initialize(self, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, Initialization.initialize_button_id[1], "\"Cash Drawer Initialization\" Window selection.", log_file)

    def Not_Initialize(self, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_NOT_to_be_present_and_log(self.driver, "click", 5, By.ID, Initialization.initialize_button_id[1], "\"Cash Drawer Initialization\" Window was not appeared.", log_file)

    def Initialize_PopUp_presence(self, log_file):    # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, ".//*[@id='dialog-content-holder']/div[1]/div", "\"Cash Drawer Initialization\" Window TITLE.", log_file).text


class OrderEntry(Initialization):

    # all page's atributes
    orders_xpath                    = (By.XPATH, ".//*[@id='orders']/a")

    new_order_breadcrumb_xpath      = (By.XPATH, ".//*[@id='wrapper']/div/section/div[1]/ul/li[2]")

    guest_name_id                   = (By.ID, 'CustomerName')
    account_name_id                 = (By.ID, 'accountName')
    account_email_id                = (By.ID, 'AccountEmail')

    orderType_ddl_Id                = (By.ID, 'orderTypeId')
    oit_inDDL_xpath                 = (By.XPATH, ".//*[@id='orderTypeId']/option[text()='Plats']")
    NoOfPages_field_xpath           = (By.XPATH, ".//input[@id='NoOfPage']")
    add_to_order_id                 = (By.ID, 'addToOrder')
    schedule_order_xpath            = (By.XPATH, ".//*[@id='scheduleOrder']")

    account_email_lookup_xpath      = (By.XPATH, ".//*[@id='accounts']/a[1]")                #TODO Should be updated as a[1] is taking only the first symbol
    email_lookup_xpath              = (By.XPATH, ".//*[@id='emails']/a[1]")                  # TODO Should be updated as a[1] is taking only the first symbol
    names_lookup_xpath              = (By.XPATH, ".//*[@id='names']")                   # TODO Should be updated as a[1] is taking only the first symbol

    NoOfContributions_id            = (By.ID, 'NumContributions')                            # TODO Variable Just added, should be tested
    NoOfPages_id                    = (By.ID, 'NoOfPage')                                    # TODO Variable Just added, should be tested
    Order_Item_tab_xpath            = (By.XPATH, "//a[@data-bind-id='orderTab']")            # TODO Variable Just added, should be tested
    Parties_tab_xpath               = (By.XPATH, "//a[@data-bind-id='partiesTab']")          # TODO Variable Just added, should be tested
    New_GRANTOR_plus_btn_xpath      = (By.XPATH, "//a[text()='New GRANTOR']")                # TODO Variable Just added, should be tested
    New_GRANTEE_plus_btn_xpath      = (By.XPATH, "//a[text()='New GRANTEE']")                # TODO Variable Just added, should be tested
    Parties_tab_Delete_xpath        = (By.XPATH, "//div[@id='partiesTab']//span[@class='parties-tools-delete' and not(@style)]")
                                            #TODO New GRANTOR Delete xpaths are ".//*[@id='parties-content']/div[1]/div[2]/div[2(,3,4, ...)]/div[2]/span[1]" which are not optimal
                                            #TODO New GRANTEE Delete xpaths are ".//*[@id='parties-content']/div[1]/div[5]/div[2(,3,4, ...)]/div[2]/span[1]" which are not optimal
                                            #TODO New GRANTEE or New GRANTOR Delete xpaths are for all "//div[@id='partiesTab']//span[@class='parties-tools-delete' and not(@style)]" which is not optimal
    # Amount_id = (By.ID, 'NoOfNames')
    Amount_id                       = (By.ID, 'Payment')
    No_of_Photos                    = (By.XPATH,".//*[@id='NoOfPhotos'] ")
    No_of_Passports                 = (By.XPATH, ".//*[@id='NoOfPassports']")
    selected_OIT_xpath              = (By.XPATH, ".//*[@id='orderTypeId']/option[@selected='selected']")
    department                      = (By.XPATH, ".//*[@id='DepartmentTypeList']/select")
    recorded_date_range_from        = (By.XPATH, ".//*[@id='fromdate']")
    recorded_date_range_to          = (By.XPATH, ".//*[@id='todate']")


    #methods
    def __init__(self, driver):
        Initialization.__init__(self, driver)

    def Click_OIT_DDl(self):
        orderType_ddl = self.driver.find_element(*OrderEntry.orderType_ddl_Id)
        orderType_ddl.click()

    # def select_OIT(self, value):
    #     orderType_ddl = Select(self.driver.find_element(*OrderEntry.orderType_ddl_Id))
    #     orderType_ddl.select_by_visible_text(value)

    def click_Orders(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.orders_xpath[1], "\"Orders\" click.", log_file)


    def select_OIT(self, OIT, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "select", 5, By.ID, OrderEntry.orderType_ddl_Id[1], "\"" + OIT + "\" Order_Type_list_item selection.", log_file, "", "", OIT)

    # Get selected OIT string
    def get_selected_OIT_text(self, log_file):
        return  Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderEntry.selected_OIT_xpath[1], "Located \"New Order\" string in breadcrumb.", log_file).text


    # def Click_AddToOrder(self):
    #     add_to_order = self.driver.find_element(*OrderEntry.add_to_order_id)
    #     add_to_order.click()

    # Locate "New order" in breadcrumb
    def locate_new_order_breadcrumb(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderEntry.new_order_breadcrumb_xpath[1], "Located \"New Order\" string in breadcrumb.", log_file)
    def return_new_order_breadcrumb(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderEntry.new_order_breadcrumb_xpath[1]).text

    def InputGuestName(self, value):
        guest_name = self.driver.find_element(*OrderEntry.guest_name_id)
        guest_name.send_keys(value)

    def InputAccountName1(self, value):
        account_name = self.driver.find_element(*OrderEntry.account_name_id)
        account_name.send_keys(value)
        time.sleep(1)
        account_email_lookup = self.driver.find_element(*OrderEntry.account_email_lookup_xpath)
        account_email_lookup.click()
        time.sleep(2)

    # def InputAccountEmail(self, value):
    #     account_email = self.driver.find_element(*OrderEntry.account_email_id)
    #     account_email.send_keys(value)
    #     time.sleep(1)
    #     email_lookup = self.driver.find_element(*OrderEntry.email_lookup_xpath)
    #     email_lookup.click()
    #     time.sleep(2)

    # def InputNumberOfPages(self, value):
    #     NoOfPages_field = self.driver.find_element(*OrderEntry.NoOfPages_field_xpath)
    #     NoOfPages_field.send_keys(value)

    def Input_NumberOfPages(self, value, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderEntry.NoOfPages_id[1], "\"No. Of Pages\" field writable.", log_file, "", value)

    def Input_Ammount(self, value, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderEntry.Amount_id[1], "\"Amount\" field writable and filled with " + str(value), log_file, "", value)

    def Input_NumberOfContributions(self, value, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderEntry.NoOfContributions_id[1] , "\"No. Of Contributions\" field writable.", log_file, "", value)

    def Click_OrderItem_Tab(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.Order_Item_tab_xpath[1], "\"Order Item tab\" detection.", log_file)

    def Click_Parties_Tab(self, log_file):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.Parties_tab_xpath[1], "\"Parties tab\" detection.", log_file)

    def Click_NewGrantor_Plus_Btn(self, log_file): # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.New_GRANTOR_plus_btn_xpath[1], "\"New_GRANTOR\" plus button click.", log_file)

    def Click_Parties_Delete_Btn(self, log_file):   # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.Parties_tab_Delete_xpath[1], "\"GRANTOR/GRANTEE\" delete button click.", log_file)

    def Click_NewGrantee_Plus_Btn(self, log_file): # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.New_GRANTEE_plus_btn_xpath[1], "\"New_GRANTEE\" plus button click.", log_file)

    def Click_AddToOrder(self, log_file):   # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, OrderEntry.add_to_order_id[1], "\"Add To order\" button click.", log_file)

    def Click_Schedule_Order(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.schedule_order_xpath[1], "\"Schedule Order\" button click.", log_file)

    def InputAccountName(self, account_email="", log_file="", symbol="1"):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderEntry.account_name_id[1],"\"Account Name\" field writable.", log_file, "", symbol)
        # account_name_id = (By.ID, 'accountName')
        # account_name = self.driver.find_element(*OrderEntry.account_name_id)
        # account_name.send_keys(value)
        # time.sleep(1)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.account_email_lookup_xpath[1], "\"Account Email\" lookup.", log_file, "", account_email)
        # account_email_lookup_xpath = (By.XPATH, ".//*[@id='accounts']/a[1]")
        # account_email_lookup = self.driver.find_element(*OrderEntry.account_email_lookup_xpath)
        # account_email_lookup.click()
        # time.sleep(2)

    def InputAccountEmail(self, email="", log_file="", symbol="1"):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderEntry.account_email_id[1],"\"Email\" field writable.", log_file, "", email, symbol)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.email_lookup_xpath[1], "\"Email\" lookup.", log_file, "", email)

    def InputCustomerName(self, name="", log_file=""):    # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderEntry.guest_name_id[1],"\"Customer Name\" field writable.", log_file, "", name)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.names_lookup_xpath[1], "\"Customer Name\" lookup.", log_file, "", name)

    def fill_No_of_Photos(self, value,  log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.No_of_Photos[1], "\"" + value + "\" was entered in No. Of Photos field.", log_file, "", value)

    def fill_No_of_Passports(self, value,  log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.No_of_Passports[1], "\"" + value + "\" was entered in No. Of Passports field.", log_file, "", value)

    # This part was added for testing Marks and Brands OI
    def find_all_animal_types(self):
        animal_type_xpath = ".//*[@id='brand-animals']/div"
        count = len(self.driver.find_elements_by_xpath(animal_type_xpath))
        animalNames = []
        i = 2
        while i <= count:
            animal_string_xpath = (By.XPATH, ".//*[@id='brand-animals']/div[" + str(i) + "]/span")
            name = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, animal_string_xpath[1]).text
            animalNames.append(name)
            i = i+1
        return  animalNames

    def go_to_marks_and_brands_tab(self, log_file):
        tabs = self.driver.find_elements_by_xpath("//a[@class='error']")
        if tabs:
            tabs[1].click()

    # Email new registration related fields
    more_options_icon_xpath             = (By.XPATH, ".//*[@id='orderentrypanel']/div[4]/div[2]/a")
    new_email_field_xpath               = (By.XPATH, ".//*[@id='CustomerEmail']")
    new_email_user_first_name_xpath     = (By.XPATH, ".//*[@id='CustomerFirstName']")
    new_email_user_last_name_xpath      = (By.XPATH, ".//*[@id='CustomerLastName']")
    new_registration_button_xpath       = (By.XPATH, ".//*[@id='orderHeaderCustomerInfo']/div[3]/a")
    validation_warning_dialog_xpath     = (By.XPATH, ".//*[@id='dialog-content-holder']/div")
    validation_warning_dialog_ok_button = (By.XPATH, ".//*[@id='infobox_Ok']")

    # Click on "More options" icon
    def open_more_options(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.more_options_icon_xpath[1], "\"More Options\" button clicked.", log_file)
        time.sleep(3)

    # Locate email field and insert value
    def insert_new_email(self, log_file, inputEmail=""):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.new_email_field_xpath[1], "\"" + inputEmail + "\" was entered inemail field.", log_file, "", inputEmail)

    # Locate new user first name field and insert value
    def insert_new_account_first_name(self, log_file, firstName = ""):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.new_email_user_first_name_xpath[1], "\"" + firstName + "\" was entered in FirstName field.", log_file, "", firstName)

    # Locate new user last name field and insert value
    def insert_new_account_last_name(self, log_file, lastName = ""):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.new_email_user_last_name_xpath[1], "\"" + lastName + "\" was entered in LastName field.", log_file, "", lastName)

    # Click on "New Registration" button
    def locate_new_registration_button(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderEntry.new_registration_button_xpath[1], "\"New Registration\" button is available.", log_file)

    def click_on_new_registration_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.new_registration_button_xpath[1], "\"New Registration\" button clicked.", log_file)

    def locate_validation_warning_dialog(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderEntry.validation_warning_dialog_xpath[1], "Validation warning dialog is present.", log_file)

    # Click on validation warning dialog Ok button
    def click_on_validation_warning_confirmation_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderEntry.validation_warning_dialog_ok_button[1], "\"Ok\" button clicked in Validation Warning dialog.", log_file)

    #select department for all exports
    def select_department(self, dept, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "select", 5, By.XPATH, OrderEntry.department[1], "Department is selected.", log_file, "", "", dept)

    def insert_recorded_date_range_from(self, date, log_file):
         Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.recorded_date_range_from[1], "\"" + date + "\" was entered in Date Range From field.", log_file, "", date)

    def insert_recorded_date_range_to(self, date, log_file):
         Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderEntry.recorded_date_range_to[1], "\"" + date + "\" was entered in Date Range To field.", log_file, "", date)


class OrderSummary(OrderEntry):

    #variables
    checkout_id                      = (By.ID, 'orderSummaryCheckout')
    price_xpath                      = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[7]")
    total_price_id                   = (By.ID, "orderTotalAmt")
    doc_number_id_in_summary         = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[@data-column='Document_InstrumentNumber']")
    breadcrumb_ordersummary_xpath    = (By.XPATH, ".//*[@id='wrapper']/div/section/div/ul/li[3]")
    Number_of                        = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[7]" )
    serial_number_xpath              = (By.XPATH,".//*[@id='OrderQueue']/tbody/tr/td[17]" )
    submit_serial_number_id          = (By.ID, "setSerialNumberBtn")
    hundred_percent_discount         = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[9]/div/select/option[2]")
    discount_apply_xpath             = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[9]/div/a[1]")
    order_price_xpath                = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[8]")
    discount_drop_down_xpath         = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[9]/div/select")
    cancel_entire_order_link_xpath   = (By.XPATH, ".//*[@id='cancelEntireOrder']")
    reject_entire_order_link_xpath   = (By.XPATH, ".//*[@id='rejectEntireOrder']")
    trackingID_validation_message    = (By.XPATH, ".//*[@id='trackingIdValMessage']")
    add_trackingID_xpath             = (By.XPATH, ".//*[@id='addTrackingId']")
    trackingID_input_xpath           = (By.XPATH, ".//*[@id='trackingIdBubble']/div/div[1]/form/div/input")
    submit_trackingID_button         = (By.XPATH, ".//*[@id='copyOrderItemBtn']")
    remove_trackingID_xpath          = (By.XPATH, ".//*[@id='removeTrackingId']")
    add_new_order_item_bnt           = (By.XPATH, ".//*[@id='newOrderItem']/a")
    edit_order_item_row              = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[15]/a")



    #methods
    def __init__(self, driver):
        OrderEntry.__init__(self, driver)

    def get_order_id(self, log_file):
        url =  self.driver.current_url
        str_url = str(url)
        str1 = re.findall('[O,o]rderId=\d+', url)
        if str1:
            OrderId = re.findall("\d+", str(str1))
            Collect_log.log(log_file, 'a+', "Order Id is "+ str(OrderId), 0)
            return OrderId
        else:
            Collect_log.log(log_file,'a+', "Order Id is not created yet ",1)


    def click_on_submit_serial_number_button(self):
        submitBtn = self.driver.find_element(*OrderSummary.submit_serial_number_id)
        submitBtn.click()

    def click_on_serial_number_in_Order_summary(self):
        serialNumber = self.driver.find_element(*OrderSummary.serial_number_xpath)
        serialNumber.click()

    def click_on_checkout_button_in_Order_Summary1(self):
        checkout = self.driver.find_element(*OrderSummary.checkout_id)
        checkout.click()

    # def click_on_checkout_button_in_Order_Summary(self):
        # checkout = self.driver.find_element(*OrderSummary.checkout_id)
        # checkout.click()

    def locate_edit_order_item_icon(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.edit_order_item_row[1], "Found \"Edit\" icon in the row.", log_file)
    def click_on_edit_icon_in_row(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.edit_order_item_row[1], "Edit icon clicked.", log_file)

    def click_on_submit_button_in_serial_number_dialog(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, OrderSummary.submit_serial_number_id[1], "\"SerialNumber Submit\" button click.",log_file)

    def click_on_checkout_button_in_Order_Summary(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, OrderSummary.checkout_id[1], "\"Checkout\" button click.",log_file)

    def return_total_price_in_Order_Summary(self, log_file):  # TODO Log Just added, should be tested
        element = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.ID, OrderSummary.total_price_id[1], "Read \"Total Price\" in Order Summary page.",log_file).text
        return element

    def locate_doc_number_string_in_order_summary(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.doc_number_id_in_summary[1], "Found \"Doc Number\" string.", log_file)
    def return_doc_number_string_in_order_summary(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.doc_number_id_in_summary[1]).text

    def locate_order_summary_breadcrumb(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderSummary.breadcrumb_ordersummary_xpath[1], "Located string in breadcrumb.", log_file)
    def return_order_summary_breadcrumb(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.breadcrumb_ordersummary_xpath[1]).text

    def locate_Number_Of(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.Number_of[1], "Found \"Number Of\" string.", log_file)
    def return_Number_Of(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.Number_of[1]).text

    # Click on dropdown list for order discount
    def click_on_discount_list(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.discount_drop_down_xpath[1], "Discount dropdown list clicked.", log_file)

    # Select 100% discount
    def select_hundred_percent_discount(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.hundred_percent_discount[1], "100% discount selected.", log_file)

    # Click on discount "Apply"
    def click_on_discount_apply(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.discount_apply_xpath[1], "Discount \"Apply\" clicked.", log_file)

    # Get order price form the order summary screen
    def locate_order_total_price(self, log_file):
        time.sleep(5)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.order_price_xpath[1], "Found Order's total price string.", log_file)
    def get_order_total_price(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.order_price_xpath[1]).text

    # Click on "Cancel Entire Order" link
    def click_on_cancel_entire_order_link(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.cancel_entire_order_link_xpath[1], "\"Cancel Entire Order\" clicked.", log_file)

    # Click on "Reject Entire Order" link
    def click_on_reject_entire_order_link(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.reject_entire_order_link_xpath[1], "\"Reject Entire Order\" clicked.", log_file)

    # Make sure that trackingID validation message is available
    def locate_trackingID_validation_message(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.trackingID_validation_message[1]).text

    # Click on TrackingID in order summary
    def click_on_add_tracking_id(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.add_trackingID_xpath[1], "\"Add TrackingID\" clicked.", log_file)

    # Locate trackingID input filed
    def locate_trackingID_input(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.trackingID_input_xpath[1], "Found trackingID input field.", log_file)

    # Input trackingID into field
    def insert_trackingId_value(self, log_file, trackingID='1256'):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.trackingID_input_xpath[1], "\"Add TrackingID\" input field clicked.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, OrderSummary.trackingID_input_xpath[1], "Inserted \"" + trackingID + "\" Tracking ID", log_file, "", trackingID)

    # Locate "Remove Tracking ID" and click on it
    def click_on_remove_trackingID(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.remove_trackingID_xpath[1], "\"Remove TrackingID\" clicked.", log_file)

    # Click on Submit button on "Add Tracking ID" dialog
    def click_on_submit_button_on_addTrackingID(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.submit_trackingID_button[1], "\"Submit\" button on Add trackingID dialog clicked.", log_file)
        time.sleep(5)

    # Locate +New Order Item button
    def locate_add_new_order_item_btn(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSummary.add_new_order_item_bnt[1], "Found add neworder item button.", log_file)

    # Click on add new Order otem button
    def click_on_add_new_order_item_btn(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSummary.add_new_order_item_bnt[1], "\"+New Order Item\" button on clicked.", log_file)



class AddPayment(OrderSummary):

    #variables
    payment_method_xpath        = (By.XPATH, ".//*[@id='paymentMethods']/div[%s]/ul/li[1]/select")
    cash_method_xpath           = (By.XPATH, ".//*[@id=\'paymentMethods\']/div[1]/ul/li[1]/select/option[2]")
    amount_xpath                = (By.XPATH, ".//*[@id='paymentMethods']/div[%s]/ul/li[4]/input")
    comment_field_xpath         = (By.XPATH, ".//*[@id='paymentMethods']/div/ul/li[3]/input")
    transaction_id_xpath        = (By.XPATH, ".//*[@id='paymentMethods']/div[%s]/ul/li[2]/input")
    checkout_id                 = (By.ID, 'orderPaymentCheckout')
    balance_due_id              = (By.ID, 'balanceDue')
    saveorder_id                = (By.ID, 'saveOrderLaterCheckout')
    add_payment_method_xpath    = (By.XPATH, ".//a[contains(@class,'newPaymentMethodRow')]")
    edit_order_payment_id       = (By.ID, 'editOrderPayment')
    delete_payment_method_xpath = (By.XPATH, ".//*[@id='paymentMethods']/div[%s]/ul/li[4]/a[2]")
    payment_method_in_fee_grid  = (By.XPATH, ".//*[@id='orderPaymentForm']/div[2]/div[1]/div[%s]/ul/li[1]/span[1]")
    payment_amount_in_fee_grid  = (By.XPATH, ".//*[@id='orderPaymentForm']/div[2]/div[1]/div[%s]/ul/li[2]/span")
    change_due_amount_fee_grid  = (By.XPATH, ".//*[@id='changeDue']")

    #methods
    def __init__(self, driver):
        OrderSummary.__init__(self, driver)

    # def select_payment_method(self, value):
    #     payment_method = Select(self.driver.find_element(*AddPayment.payment_method_xpath))
    #     payment_method.select_by_visible_text(value)

    # def fill_amount(self):
    #     amount_field = self.driver.find_element(*AddPayment.amount_xpath)
    #     balance_due = self.driver.find_element(*AddPayment.balance_due_id).text
    #     amount_field.send_keys(balance_due)
    #
    # def click_on_checkout_button(self):
    #     checkout = self.driver.find_element(*AddPayment.checkout_id)
    #     checkout.click()

    def select_payment_method(self, value, log_file, payment_method_row=1):   # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "select", 10, By.XPATH, AddPayment.payment_method_xpath[1]%payment_method_row, "\"" + value + "\" payment method selection.", log_file, "", "", value)

    #read payment_methods
    def read_payment_method_from_ddl(self, payment_method_row, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return",5, By.XPATH, ".//*[@id='paymentMethods']/div[1]/ul/li[1]/select/option["+str(payment_method_row)+"]",
                                    msg = "There is no row number-" + str(payment_method_row) + " in Payment Method ddl.", file_path=log_file, ignor_errors= "yes").text

    #list of payment_methods
    def get_all_payment_methods(self, log_file):
        List_payment_methods = []
        i = 2
        try:
            while AddPayment.read_payment_method_from_ddl(self, i, log_file):
                payment_methods = str(AddPayment.read_payment_method_from_ddl(self, i, log_file))
                List_payment_methods.append(payment_methods)
                i = i+1
        except:
            return List_payment_methods


    # Old
    # def fill_amount(self, value, log_file):  # TODO Log Just added, should be tested
    #     Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, AddPayment.amount_xpath[1], "\"" + value + "\" was entered in Amount field.", log_file, "", value)
    #

    def fill_amount(self, log_file, ammount_field_line = 1):  # TODO Log Just added, should be tested
        balance_due = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.ID, AddPayment.balance_due_id[1], "Read \"Total Price\" in Order Summary page.", log_file).text
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, AddPayment.amount_xpath[1]%ammount_field_line, "\"" + balance_due + "\" was entered in Amount field.", log_file, "", balance_due)

    def fill_amount_by_value(self,value, log_file, ammount_field_line = 1):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, AddPayment.amount_xpath[1]%ammount_field_line, "\"" + value + "\" was entered in Amount field.", log_file, "", value)

    def fill_transaction_id_field(self, value, log_file, transaction_id_field_line = 1):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, AddPayment.transaction_id_xpath[1]%transaction_id_field_line, "\"" + str(value) + "\" was entered in  transaction ID field.", log_file, "", str(value))

    def check_transaction_id_is_required(self, log_file):
        is_required = "True"
        transaction_id_class_attribute = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, ".//li[contains(@data-bind,'Transaction')]").get_attribute('class')
        if re.search("Error", transaction_id_class_attribute, re.I):
           pass
        else:
            is_required = "False"
        return is_required

    def balance_due_amount(self, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.ID, AddPayment.balance_due_id[1], "Read \"Total Price\" in Order Summary page.", log_file).text

    def click_on_checkout_button_in_AddPayment(self, log_file): # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.ID, AddPayment.checkout_id[1], "\"Checkout\" button click.", log_file)
        # checkout = self.driver.find_element(*AddPayment.checkout_id)
        # checkout.click()
    def is_checkout_btn_disabled(self):
        elem = Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.ID, AddPayment.checkout_id[1])
        return Explicit_Wait.is_attribtue_present(elem, "disabled")

    def click_Save_Order(self, log_file): # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, AddPayment.saveorder_id[1], "\"Save Order\" button click.", log_file)

    # Fills saving reason in "Save Order" dialog
    def fill_order_save_reason(self, log_file):
        save_reason_xpath   = (By.XPATH, ".//*[@id='actionReason']")
        submit_button_xpath = (By.XPATH, ".//*[@id='actionReasonsBtn']")
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, save_reason_xpath[1], "Fill in order's save reason.", log_file, "", "Save for later processing")
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, submit_button_xpath[1], "\"Submit\" button click.", log_file)

    def fill_payment_methods_comment_field(self,comment_msg, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 10, By.XPATH, AddPayment.comment_field_xpath[1], "\"" + comment_msg + "\" was entered in comment field.", log_file, "", comment_msg)

    def return_comment_field_text(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, AddPayment.comment_field_xpath[1]).get_attribute('value')



    def return_amount_field_value(self,ammount_field_line = 1):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, AddPayment.amount_xpath[1]%ammount_field_line).get_attribute('value')

     #Click on add new Order otem button
    def click_on_add_payment_method_btn(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, AddPayment.add_payment_method_xpath[1], "\"+Payment Method\" button clicked.", log_file)

    def click_on_edit_order_payment(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, AddPayment.edit_order_payment_id[1], "\"Edit Order Payment \" button clicked.", log_file)

    #click on "X" button to delete payment Method
    def click_on_delete_payment_method_btn(self,log_file, x_button_line = 1):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, AddPayment.delete_payment_method_xpath[1]%x_button_line, "\"X\" button clicked.", log_file)

    # Find payment method in fee gredd for the given line
    # Note: this function will work only if in given row there is an payment method
    def return_payment_method_in_fee_grid(self, log_file, method_line = 1):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, AddPayment.payment_method_in_fee_grid[1]%method_line, "Read selected payment method in Fee grid.", log_file).text
    def return_payment_amount_in_fee_grid(self, log_file, method_line = 1):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, AddPayment.payment_amount_in_fee_grid[1]%method_line, "Read selected payment amount in Fee grid.", log_file).text

    # Get change due amount in fee grid
    def return_change_due_in_fee_grid(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, AddPayment.change_due_amount_fee_grid[1], "Read change due amount in Fee grid.", log_file).text


    # def return_js_var(self):
    #     return self.driver.execute_script("return orderPayments", AddPayment.comment_field_xpath)


class OrderFinalization(AddPayment):

    def __init__(self, driver):
        AddPayment.__init__(self, driver)

    #variables
    breadcrumb_xpath                = (By.XPATH, ".//*[@id='wrapper']/div/section[2]/div[1]/ul/li[2]")
    oit_row_edit_icon_xpath         = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[13]/a")
    print_barecode_xpath            = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[14]/a")
    email_receipt_xpath             = (By.XPATH, ".//*[@id='email-duplicate-receipt']")
    print_receipt_xpath             = (By.XPATH, ".//*[@id='print-duplicate-receipt']")
    edit_order_payments_id          = (By.ID, 'editOrderPayment')
    order_queue_button_id           = (By.ID, 'orderFinalCheckout')
    void_button_id                  = (By.ID, 'VoidOrder')
    get_next_btn                    = (By.XPATH, ".//*[@id='nextOrder']")
    finalize_string_xpath           = (By.XPATH, "//*[@id='left-block']/div[2]/div[1]/span")
    order_number_string_id          = (By.ID, "orderNumber")
    doc_number_string_id            = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[@data-column = 'Document_InstrumentNumber']")
    order_status_xpath              = (By.XPATH, "//*[@data-column='Order_OrderStatus']")
    view_edit_order_item_funds_id   = (By.ID, 'feeDistribution')
    total_ammount                   = (By.XPATH, ".//*[@id='orderTotalAmt']")
    total_ammount_fee               = (By.XPATH, ".//*[@id='totalAmount']")
    Number_Of_orderfinalization     = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[7]")
    discount_reset_xpath            = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[9]/div/a[2]")
    load_spiner_order_finalization  = (By.XPATH, "html/body/div[3]/div")

    #methods
    def verify_breadcrumb(self):
        breadcrumb = self.driver.find_element(*OrderFinalization.breadcrumb_xpath)
        if breadcrumb.is_displayed():
            print ""
        else:
            print ("Fail - Couldn't reach Order Finalization screen")
    def locate_get_next_btn(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderFinalization.order_status_xpath[1], "Located \"Get Next\" button.", log_file)

    def click_on_void_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, OrderFinalization.void_button_id[1], "\"Void\" button in order finalization click.", log_file)

    def locate_order_finalization_breadcrumb(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderFinalization.breadcrumb_xpath[1], "Located string in breadcrumb.", log_file)
    def return_order_finalization_breadcrumb(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.breadcrumb_xpath[1]).text

    def locate_order_finalize_string(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.finalize_string_xpath[1], "Found \"Order Finalize\" string.", log_file)
    def return_order_finalize_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.finalize_string_xpath[1]).text

    def locate_order_number_string(self, log_file):  # TODO Log is not working, message should be added in Log file
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 15, By.ID, OrderFinalization.order_number_string_id[1], "Found \"Order Number\" string.", log_file)
    def return_order_number_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 15, By.ID, OrderFinalization.order_number_string_id[1]).text

    def locate_doc_number_string(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.doc_number_string_id[1], "Found \"Doc Number\" string.", log_file)
    def return_doc_number_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.doc_number_string_id[1]).text

    def locate_order_status(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderFinalization.order_status_xpath[1], "Located order status string.", log_file)
    def return_order_status(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.order_status_xpath[1]).text

    def locate_total_amount(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderFinalization.total_ammount[1], "Located Total Ammount", log_file)
    def return_total_ammount(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.total_ammount[1]).text

    def locate_Number_Of_orderfinalization(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.Number_Of_orderfinalization[1], "Found \"Number Of\" string on Order Finalization screen.", log_file)
    def return_Number_Of_orderfinalization(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.Number_Of_orderfinalization[1]).text

    def return_total_ammount_fee(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH,OrderFinalization.total_ammount_fee[1]).text


    def click_edit_on_order_row(self,log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderFinalization.oit_row_edit_icon_xpath[1], "Click Edit on order row", log_file)

    def click_view_edit_order_item_funds(self,log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.ID, OrderFinalization.view_edit_order_item_funds_id[1], "Click on View/Edit Order Item Funds", log_file)

    # Click on discount "Reset"
    def click_on_discount_reset(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderFinalization.discount_reset_xpath[1] , "Discount \"Reset\" clicked.", log_file)
        time.sleep(5)

    def wait_loader_spiner_to_be_absent(self, log_file):
        Explicit_Wait.wait_for_element_NOT_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderFinalization.load_spiner_order_finalization[1], "Queue loader is absent .", log_file)



    RMaP_xpath          = (By.XPATH,".//*[@id='feeDistributionContent']/div/table/tbody/tr[2]/td[1]")                 # Records Management and Preservation
    CS_xpath            = (By.XPATH,".//*[@id='feeDistributionContent']/div/table/tbody/tr[3]/td[1]")                 # Courthouse Security
    CCRA_xpath          = (By.XPATH,".//*[@id='feeDistributionContent']/div/table/tbody/tr[4]/td[1]")                 # County Clerk Records Archive
    NCF_xpath           = (By.XPATH,".//*[@id='feeDistributionContent']/div/table/tbody/tr[5]/td[1]")                 # New Courthouse Fee
    CCF_xpath           = (By.XPATH,".//*[@id='feeDistributionContent']/div/table/tbody/tr[6]/td[1]")                 # County Clerk Fees

    balance_due_xpath = (By.XPATH, ".//*[@id='feeDistributionContent']/div/table/tbody/tr[last()]/th[2]")             # Total Value


    def return_Records_Management_and_Preservation_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.RMaP_xpath[1]).text
    def return_Courthouse_Security_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.CS_xpath[1]).text
    def return_County_Clerk_Records_Archive_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.CCRA_xpath[1]).text
    def return_New_Courthouse_Fee_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.NCF_xpath[1]).text
    def return_County_Clerk_Fees_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.CCF_xpath[1]).text
    def return_total_balance_due_value(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.balance_due_xpath[1]).text

    RMaP_value_xpath          = (By.XPATH, ".//*[@id='feeDistributionContent']/div/table/tbody/tr[2]/td[1]")  # Records Management and Preservation
    CS_value_xpath            = (By.XPATH, ".//*[@id='feeDistributionContent']/div/table/tbody/tr[3]/td[1]")  # Courthouse Security
    CCRA_value_xpath          = (By.XPATH, ".//*[@id='feeDistributionContent']/div/table/tbody/tr[4]/td[1]")  # County Clerk Records Archive
    NCF_value_xpath           = (By.XPATH, ".//*[@id='feeDistributionContent']/div/table/tbody/tr[5]/td[1]")  # New Courthouse Fee
    CCF_value_xpath           = (By.XPATH, ".//*[@id='feeDistributionContent']/div/table/tbody/tr[6]/td[1]")  # County Clerk Fees

    def return_Records_Management_and_Preservation_value(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.RMaP_value_xpath[1]).text
    def return_Courthouse_Security_value(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.CS_value_xpath[1]).text
    def return_County_Clerk_Records_Archive_value(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.CCRA_value_xpath[1]).text
    def return_New_Courthouse_Fee_value(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.NCF_value_xpath[1]).text
    def return_County_Clerk_Fees_value(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.CCF_value_xpath[1]).text

    # This part is added for validation printing options presence in "Marriage License" order item page
    print_front_page_icon_xpath = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[@data-bind=\"visible: CanPrintCertFrontPage\"]/a" )     # Print front page icon "F"
    print_back_page_icon_xpath  = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[@data-bind=\"visible: CanPrintCertBackPage\"]/a")      # Print back page icon "B"
    print_all_pages_icon_xpath  = (By.XPATH, ".//*[@id='orderSummary']/tbody/tr/td[@data-bind=\"visible: CanPrintCertPages\"]/a")      # Print all pages of certificate "L"

    def return_print_front_page_icon_element_title(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.print_front_page_icon_xpath[1]).get_attribute("title")
    def return_print_back_page_icon_element_title(self): # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.print_back_page_icon_xpath[1]).get_attribute("title")
    def return_print_all_pages_icon_element_title(self): # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderFinalization.print_all_pages_icon_xpath[1]).get_attribute("title")


class VoidOrderSummary(Login):

    def __init__(self, driver):
        Login.__init__(self)

    #variables
    # select_all_void_id                         = (By.ID, 'selectAllVoid')   TODO it's for Hidalgo only
    # deselect_all_void_id                       = (By.ID, 'deSelectAllVoid')
    void_button_on_voidordersummary_screen       = (By.XPATH, ".//*[@id='left-block']/div[5]/div/input[2]")
    cancel_button_on_voidordersummary_screen     =(By.XPATH, ".//*[@id='voidOrderSummaryCancel']")

    #methods
    def click_on_void_button_voidordersummary_screen(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver,"click", 5, By.XPATH, VoidOrderSummary.void_button_on_voidordersummary_screen[1],"\"Void\" button in VoidOrderSummary click.", log_file)

    def click_on_Cancel_button_voidordersummary_screen(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, VoidOrderSummary.cancel_button_on_voidordersummary_screen[1], "\"Cancel\" button click", log_file)


class VoidOrderPayment(Login):

    def __init__(self, driver):
        Login.__init__(self)

    #variables
    finalize_void_button_voidorderpayment_screen      = (By.XPATH, ".//*[@id='voidOrderPaymentCheckout']")
    cancel_button_voidorderpayment_screen             = (By.XPATH, ".//*[@id='orderPaymentHistoryBack']")

    #methods
    def Finalize_Void(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, VoidOrderPayment.finalize_void_button_voidorderpayment_screen[1], "\"Finalize Void\" button click.", log_file)

    def Cancel_Finalize_Void(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, VoidOrderPayment.cancel_button_voidorderpayment_screen[1], "Void Finalization cancell", log_file)


class CaptureQueue(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    capture_queue_title_xpath          = (By.XPATH, ".//*[@id='results-table']/div/div[3]/h3")
    administrative_key                 = (By.XPATH, ".//*[@id='icon-key']/a/span")
    scan_xpath                         = (By.XPATH, ".//*[@id='startBatchScan']/span")
    refresh_xpath                      = (By.XPATH, ".//*[@id='icon-refresh']/a/span")
    capture_tab_xpath                  = (By.XPATH, ".//*[@id='capture']/a")
    upload_image_icon                  = (By.XPATH, ".//tr/td[@data-value = '%s']/following-sibling::td[9]")
    start_scan                         = (By.XPATH, ".//[@id='wrapper']/div/section/div[2]/div[1]/div/div[1]/div/div[1]/div/div[1]")

    #methods
    def locate_upload_icon(self, ordernumber,  log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 15, By.XPATH, CaptureQueue.upload_image_icon[1]%ordernumber, "Found Upload button.", log_file)
    def click_on_upload_icon(self, ordernumber, log_file):
         Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, CaptureQueue.upload_image_icon[1]%ordernumber, "Upload button clicked.", log_file)

    def locate_capture_queue_tab(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 20, By.XPATH, CaptureQueue.capture_tab_xpath[1], "Found Capture tab.", log_file)

    def click_on_capture_queue_tab(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 20, By.XPATH, CaptureQueue.capture_tab_xpath[1], "Capture Queue tab clicked.", log_file)

    def is_capture_queue(self):
        Explicit_Wait.wait_for_element_visibility(self,CaptureQueue.capture_queue_title_xpath)

    def click_start_batch_scan(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, CaptureQueue.scan_xpath[1], "Click \"Start Batch Scan\"", log_file)

    # start_scan = (By.XPATH, ".//*[@class='startBatchScanBtn clickable']")
    # def click_start_scan(self, log_file):  # TODO Log Just added, should be tested
    #     Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, CaptureQueue.start_scan[1], "Click \"Start Scan\"", log_file)

    def click_start_scan(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, CaptureQueue.start_scan[1], "Click \"Start Scan\"", log_file)


class CaptureSummary(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    start_scan              = (By.XPATH, ".//*[@id='wrapper']/div/section/div[2]/div[1]/div/div[1]/div/div[1]/div/div[1]/a")
    # start_scan            = (By.XPATH, ".//*[@class='startBatchScanBtn clickable']")
    capture_setup_button    = (By.XPATH, ".//*[@class='captureSetUpBtn clickable']")
    scan_label              = (By.XPATH, ".//*[@id='wrapper']/div/section/div[2]/div[1]/div/div[1]/div/div[1]/div/div[1]/span")

    #methods
    def click_start_scan1(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, CaptureSummary.start_scan[1], "Click \"Start Scan\"", log_file)

    def click_capture_setup(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, CaptureSummary.capture_setup_button[1], "Click \"Capture setup button\"", log_file)

    # TODO: check if it is used somewhere, otherwise remove or comment this method
    def read_start_scan_label(self, log_file):  # TODO Log Just added, should be tested
        print Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 5, By.XPATH, CaptureSummary.scan_label[1], "Read \"Start Scan\" label", log_file).text


class UploadPopup(Login):

    def __init__(self, driver):
        Login.__init__(self)

    #variables
    recorded_year                      = (By.XPATH, ".//*[@id='docRecYearInput']")
    document_number                    = (By.XPATH, ".//*[@id='docNumberInput']")
    search_button                      = (By.XPATH, ".//*[@id='searchDocBtn']")
    images_link                        = (By.XPATH, ".//*[@id='imagesHome']")
    reset_link                         = (By.XPATH, ".//*[@id='resetBtn']")
    toggle_view_mode_button            = (By.XPATH, ".//*[@id='btnToggleViewMode']")
    return_current_page_to_folder_link = (By.XPATH, ".//*[@id='returnPageBtn']")
    upload_button                      = (By.XPATH, ".//*[@id='UploadBtn']")
    cancel_button                      = (By.XPATH, ".//*[@id='closeDialogBtn']")
    birth_folder                       = (By.XPATH, ".//*[@id='folderContent']/div[1]/div[1]/span")
    death_folder                       = (By.XPATH, ".//*[@id='folderContent']/div[1]/div[2]/span")
    plats_folder                       = (By.XPATH, ".//*[@id='folderContent']/div[1]/div[3]/span")
    firts_image                        = (By.XPATH, ".//*[@id='folderContent']/div[2]/div[1]")
    load_spiner_upload_popup           = (By.XPATH, ".//*[@id='UploadImageDialog']/div[2]")


    #methods
    def click_on_plats_folder(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, UploadPopup.plats_folder[1], "Click on \"Plats\" folder.", log_file)

    def click_on_birth_folder(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, UploadPopup.birth_folder[1], "Click on \"Birth\" folder.", log_file)

    def click_on_death_folder(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, UploadPopup.death_folder[1], "Click on \"Death\" folder.", log_file)

    def select_image(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, UploadPopup.firts_image[1], "Image is selected.", log_file)

    def click_on_upload_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, UploadPopup.upload_button[1], "Upload button is clicked.", log_file)

    def wait_loader_to_be_absent_uploadpopup(self, log_file):
        Explicit_Wait.wait_for_element_NOT_to_be_present_and_log(self.driver, "return", 10, By.XPATH, UploadPopup.load_spiner_upload_popup[1], "Upload Popup Loader is absent .", log_file)



class IndexingQueue(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    indexing_queue_title_xpath                    = (By.XPATH, ".//*[@id='results-table']/div/div[3]/h3")
    add_new_indexing_task_xpath                   = (By.XPATH, ".//*[@id='addNewIndexingTask']")
    administrative_key_xpath                      = (By.XPATH, ".//*[@id='icon-key']/a")
    process_scheduler_next_task_xpath             = (By.XPATH, ".//*[@id='icon-processnextscheduled']/a")
    process_next_task_xpath                       = (By.XPATH, ".//*[@id='icon-processnext']/a")
    refresh_xpath                                 = (By.XPATH, ".//*[@id='icon-refresh']/a")
    indexing_tab_xpath                            = (By.XPATH, ".//*[@id='indexing']/a")
    running_guy                                   = (By.XPATH, ".//tr/td/span[text()='%s']/following::td[10]")
    assign_to                                     = (By.XPATH, ".//tr/td/span[text()='%s']/following::td[11]")
    cancel_indexing_task                          = (By.XPATH, ".//tr/td/span[text()='%s']/following::td[14]")
    document_group_drop_down                      = (By.XPATH, ".//*[@id='certificateType']")
    birth_recording_doc                           = (By.XPATH, ".//*[@id='certificateType']/option[2]")
    death_recording_doc                           = (By.XPATH, ".//*[@id='certificateType']/option[3]")
    add_new_task_submit_btn                       = (By.XPATH, ".//*[@id='addNewCertificateSubmit']")
    task_save_btn                                 = (By.XPATH, ".//*[@id='vitalAccept-Btn']")
    task_cancel_btn                               = (By.XPATH, ".//*[@id='vitalCancel-Btn']")
    info_box_yes_btn                              = (By.XPATH, ".//*[@id='infobox_Yes']")
    info_box_no_btn                               = (By.XPATH, ".//*[@id='infobox_No']")



    #methods
    def is_indexing_queue(self):
        Explicit_Wait.wait_for_element_visibility(self, IndexingQueue.indexing_queue_title_xpath)

    def click_on_running_guy(self, ordernumber, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.running_guy[1]%ordernumber, "Upload button clicked.", log_file)

    def click_on_assign_to(self, ordernumber, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.assign_to[1]%ordernumber, "Assign To button clicked.", log_file)

    def click_on_cancel_indexing_task(self, ordernumber, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.cancel_indexing_task[1]%ordernumber, "Assign To button clicked.", log_file)

    def click_on_add_new_indexing_task(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.add_new_indexing_task_xpath[1], "\"+\"  button  clicked.", log_file)

    def navigate_to_indexing_tab(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.indexing_tab_xpath[1], "Indexing Queue opened.", log_file)

    def select_Birth_Record_doc_type(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.document_group_drop_down[1], "drop down opened.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.birth_recording_doc[1], "Birth recording doc type selected .", log_file)

    def click_on_add_new_task_submit_btn(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.add_new_task_submit_btn[1], "selected doc type submitted .", log_file)

    def click_on_Save_btn(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.task_save_btn[1], "\"Save\" Button Clicked", log_file)

    def click_yes_from_warming_box(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.info_box_yes_btn[1], "\"Yes\" Button from \"Warming\" box clicked", log_file)

    def click_no_from_warming_box(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingQueue.info_box_no_btn[1], "\"No\" Button from \"Warming\" box clicked", log_file)


class IndexingSummary(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    row_edit_icon                     = (By.XPATH, ".//*[@id='indexingTaskhistory']")
    save_order                        = (By.XPATH, "")
    next_order                        = (By.XPATH, ".//*[@id='orderSummaryNextOrder']")
    send_to_administrator             = (By.XPATH, ".//*[@id='orderSummaryBottomBlock']/div[3]/a")
    return_to_indexing_queue          = (By.XPATH, "")




    #methods
    def click_on_edit_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingSummary.row_edit_icon[1], "Edit button clicked.", log_file)

    def click_on_save_order_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingSummary.save_order[1], "\"Save Order\" button clicked.", log_file)

    def click_on_next_order_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingSummary.next_order[1], "Next Order button clicked.", log_file)

    def click_on_return_to_indexing_queue(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingSummary.return_to_indexing_queue[1], "\"Return to Indexing Queue\" clicked.", log_file)

    def click_on_send_to_administrator(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingSummary.send_to_administrator[1], "\"Send to Administrator\" clicked.", log_file)


class IndexingEntry(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    save_and_advance                     = (By.XPATH, ".//*[@id='SaveAdvance']")
    cancel_indexing_entry_button         = (By.XPATH, ".//*[@id='indexTaskItemCancel']")


    #methods
    def click_on_save_and_advance(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingEntry.save_and_advance[1], "\"Save & Advance\" button clicked.", log_file)

    def click_on_cancel_indexing_entry_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 15, By.XPATH, IndexingEntry.save_and_advance[1], "\"Cancel\" button clicked.", log_file)


class VerificationQueue(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    verification_queue_title_xpath         = (By.XPATH, ".//*[@id='results-table']/div/div[3]/h3")
    administrative_key_xpath               = (By.XPATH, ".//*[@id='icon-key']/a")
    process_scheduler_next_task_xpath      = (By.XPATH, ".//*[@id='icon-processnextscheduled']/a")
    process_next_task_xpath                = (By.XPATH, "..//*[@id='icon-processnext']/a")
    refresh_xpath                          = (By.XPATH, ".//*[@id='icon-refresh']/a")
    verification_tab_xpath                 = (By.XPATH, ".//*[@id='verification']/a")

    #methods
    def is_verification_queue(self):
        Explicit_Wait.wait_for_element_visibility(self, VerificationQueue.verification_queue_title_xpath)


class OrderSearch(Login):
    def __init__(self, driver):
        Login.__init__(self)

    #variables
    order_number_xpath                    = (By.XPATH, ".//*[@id='OrderNumber']")
    account_code_xpath                    = (By.XPATH, ".//*[@id='AccountCode']")
    email_xpath                           = (By.XPATH, ".//*[@id='EmailId']")
    order_date_range_radio_button_xpath   = (By.XPATH, ".//*[@id='daterange-block']/div/label[1]")
    recorded_date_range_xpath             = (By.XPATH, ".//*[@id='daterange-block']/div/label[2]")
    search_button_xpath                   = (By.XPATH, ".//*[@id='search']")
    reset_search_button_xpath             = (By.XPATH, ".//*[@id='resetOrderSearch']")
    search_tab_xpath                      = (By.XPATH, ".//*[@id='searching']/a")
    found_order_number_xpath              = (By.XPATH, "//*[@id='orderSearchBlock']/tbody/tr/td[@data-column='OrderHeader.OrderNumber']")
    number_of_order_items_xpath           = (By.XPATH, ".//*[@id='orderSearchBlock']/tbody/tr/td[@data-column='NoOfItems']")
    # archive_string_xpath                = (By.XPATH, "//*[@id='orderSearchBlock']/tbody/tr/td[15]") as this td[15] was changed to td[16] 8/25/16 and it can be changed also in future, adding uniq XPATH
    archive_string_xpath                  = (By.XPATH, ".//*[@id='orderSearchBlock']/tbody/tr/td[@data-column='OrderSearchStatus']")
    workflowstep_name                     = (By.XPATH, ".//*[@id='orderSearchBlock']/tbody/tr/td[@data-column='OrderSearchStatus']")
    from_date_field_id                    = (By.ID, 'FromDate')
    to_date_field_id                      = (By.ID, 'ToDate')
    more_options_btn                   	  = (By.XPATH, ".//*[@id='links-block']/span[1]")
    name_field_xpath                      = (By.XPATH, ".//*[@id='Name']")
    tracking_id_xpath                     = (By.XPATH, ".//*[@id='TrackingId']")
    location_field_xpath                  = (By.XPATH, ".//*[@id='LocationId']")
    department_field_xpath                = (By.XPATH, ".//*[@id='DepartmentId']")
    origin_field_xpath                    = (By.XPATH, ".//*[@id='OriginId']")
    assigned_to_field_xpath               = (By.XPATH, ".//*[@id='AssignedTo']")
    payment_type_field_xpath              = (By.XPATH, ".//*[@id='OrderPaymentTypeId']")
    transaction_id_field_xpath            = (By.XPATH, ".//*[@id='OrderPaymentId']")
    serial_number_field_xpath             = (By.XPATH, ".//*[@id='SerialNumber']")
    start_document_number_xpath           = (By.XPATH, ".//*[@id='docNumberStart']")
    end_document_number_xpath             = (By.XPATH, ".//*[@id='docNumberEnd']")
    edit_order_button_xpath               = (By.XPATH, ".//a[contains(@class,'iconEditOrder')]")
    print_rejection_letter_icon_xpath     = (By.XPATH, ".//a[contains(@class,'iconPrintRejectionLetter')]")
    print_duplicate_receipt_icon_xpath    = (By.XPATH, ".//a[contains(@class,'iconDuplicateRecipt')]")
    send_to_admin_icon_xpath              = (By.XPATH, ".//a[contains(@class,'iconSendAdmin actionReasonsPopup normalmode')]")
    receipt_preview_dialog_xpath          = (By.XPATH, ".//*[@id='dialog-content-holder']")
    receipt_preview_dialog_title_xpath    = (By.XPATH, ".//*[@class='dialog-title-left']")
    reEntry_canceled_order_icon_xpath     = (By.XPATH, ".//a[contains(@class,'sendCancelledOrderBack')]")
    receipt_preview_dialog_close_btn      = (By.XPATH, ".//a[contains(@class,'fancybox-item fancybox-close')]")
    receipt_block_content_xpath           = (By.XPATH, ".//*[@id='receiptPreviewBlock']/ul/li/div[@class = 'receiptPreview']")
    no_match_found_string_xpath           = (By.XPATH, ".//*[@id='wrapper']/div/section/div[2]/div/div/em")
    day_range_error_xpath                 = (By.XPATH, ".//*[@id='birtherror']")
    enter_reason_xpath                    = (By.XPATH, ".//*[@id='actionReason']")
    enter_description_xpath               = (By.XPATH, ".//*[@id='actionDescription']")
    submit_xpath                          = (By.XPATH, ".//*[@id='actionReasonsBtn']")
    sent_message_xpath                    = (By.XPATH, ".//*[@id='actionReasonsMessage']")
    order_search_tab_xpath                = (By.XPATH, ".//*[@id='orderSearch']")
    package_search_tab_xpath              = (By.XPATH, ".//*[@id='packageSearch']")
    document_search_tab                   = (By.XPATH, ".//*[@id='searchsub']/li[4]")
    search_breadcrumb                     = (By.XPATH, ".//*[@id='wrapper']/div/section/div[1]/ul/li[2]")
    search_result_order_header            = (By.XPATH, ".//*[@id='orderSearchBlock']/thead/tr/th[4]")
    eRecording_package_id                 = (By.XPATH, ".//*[@id='PackageId']")
    package_id_in_row                     = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[@data-column='PackageId']")
    order_number_package_id_in_row        = (By.XPATH, ".//*[@id='OrderQueue']/tbody/tr/td[@data-column='OrderNumber']")

    #methods
    def is_search_screen(self):
        Explicit_Wait.wait_for_element_visibility(self, OrderSearch.search_button_xpath)

    def click_on_more_less_options_btn(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.more_options_btn[1], "\"More option\" button click.", log_file)
    def return_more_less_btn_text(self,log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.more_options_btn[1]).text

    def click_search_tab(self, log_file):# TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.search_tab_xpath[1], "\"Search\" Tab click.", log_file)

    def fill_order_number(self, order_number, log_file):# TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.order_number_xpath[1], "\"" + order_number + "\" was entered in Order Number field.", log_file, "", order_number)

    def fill_email(self, email, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.email_xpath[1], "\"" + email + "\" was entered in Email field.", log_file, "", email)

    def fill_account_code(self, account_code, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.account_code_xpath[1], "\"" + account_code + "\" was entered in Account Code field.", log_file, "", account_code)

    def click_search_btn(self, log_file):# TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.search_button_xpath[1], "Click \"Search\" button.", log_file)

    def click_reset_search_btn(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.reset_search_button_xpath[1], "Click \"Reset Search\" button.", log_file)

    def locate_search_result_order_header(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.search_result_order_header[1], log_file, ignor_errors="yes")

    def locate_edit_order_btn(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.edit_order_button_xpath[1], "Found \"Edit Order \" button.", log_file)

    def click_on_edit_order_btn(self,log_file): # TODO should be added wait_for_page_refresh function instead of sleep
        time.sleep(3)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 25, By.XPATH, OrderSearch.edit_order_button_xpath[1], "\"Click\" Edit order button.", log_file)

    def locate_found_order_number(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.found_order_number_xpath[1], "Found \"Order Number\" string.", log_file)
    def return_found_order_number(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.found_order_number_xpath[1]).text

    def locate_number_of_order_items(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.number_of_order_items_xpath[1], "Found \"Order Number\" string.", log_file)
    def return_number_of_order_items(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.number_of_order_items_xpath[1]).text


    def locate_archive_string(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.archive_string_xpath[1], "Found \"Archive\" string.", log_file)
    def return_archive_string(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.archive_string_xpath[1]).text

    def locate_workflowstep_name(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.workflowstep_name[1], "Found \"Workflow step \" string.", log_file)
    def return_workflowstep_name(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.workflowstep_name[1]).text

    def locate_FromDate_field(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.ID, OrderSearch.from_date_field_id[1], "from Date field", log_file)

    def return_FromDate_field_value(self, log_file):  # TODO Log Just added, should be tested
        return  Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.ID, OrderSearch.from_date_field_id[1], "from Date field", log_file).get_attribute("value")

    def fillIn_FromDate_filed_value(self, dateValue, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "clear", 5, By.ID, OrderSearch.from_date_field_id[1], "Remove search date from \"From date\" field", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderSearch.from_date_field_id[1], "Write new date in \"From date\" field", log_file, "", dateValue)

    def locate_ToDate_field(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.ID, OrderSearch.to_date_field_id[1], "To Date field", log_file)

    def return_ToDate_field_value(self, log_file):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.ID, OrderSearch.to_date_field_id[1], "To Date field", log_file).get_attribute("value")

    def fillIn_ToDate_filed_value(self, dateValue, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "clear", 5, By.ID, OrderSearch.to_date_field_id[1], "Remove search date from \"To date\" field", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.ID, OrderSearch.to_date_field_id[1], "Write new date in \"To date\" field", log_file, "", dateValue)

    # Find print rejection letter icon
    def locate_print_rejection_letter_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.print_rejection_letter_icon_xpath[1], "Print Rejection Letter is available", log_file)
    def click_print_rejection_letter_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.print_rejection_letter_icon_xpath[1], "Print Rejection Letter clicked", log_file)

    # Find reEntry cancelled order icon
    def locate_reEntry_cancelled_order_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.reEntry_canceled_order_icon_xpath[1],"ReEntry cancelled order icon is available", log_file)
    def click_reEntry_cancelled_order_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.reEntry_canceled_order_icon_xpath[1],"ReEntry cancelled order clicked", log_file)
    def verify_reEntry_cancelled_order_icon_notAvailable(self, log_file):
        Explicit_Wait.wait_for_element_NOT_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.reEntry_canceled_order_icon_xpath[1],"ReEntry cancelled order icon is not available", log_file)

    # Go to Package search tab
    def locate_package_search(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.package_search_tab_xpath[1], "\"Packages Search\" Tab is available.", log_file)
    def click_package_search(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.package_search_tab_xpath[1], "\"Packages Search\" Tab click.", log_file)

    def fill_eRecording_package_Id(self, package_name, log_file):# TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.eRecording_package_id[1], "\"" + package_name + "\" was entered in ERecording Package Id field.", log_file, "", package_name)

    def locate_package_id_in_row(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.package_id_in_row[1], "Found \"Package Id\" string.", log_file)
    def return_package_id_in_row(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.package_id_in_row[1]).text

    def locate_order_number_package_id(self, log_file):  # TODO Log Just added, should be tested is not working
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.order_number_package_id_in_row[1], "Found \"Order Number-Package\" string.", log_file)
    def return_order_number_package_id_in_row(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.order_number_package_id_in_row[1]).text

    def click_on_order_number_package_id_in_row(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.order_number_package_id_in_row[1], "\"Order number-Package\"  click.", log_file)

    # Go to Order search tab
    def locate_order_search(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.order_search_tab_xpath[1], "\"Orders Search\" Tab is available.", log_file)
    def click_order_search(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.order_search_tab_xpath[1], "\"Orders Search\" Tab click.", log_file)

    #Go to Documents search tab
    def locate_document_search(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.order_search_tab_xpath[1], "\"Documents Search\" Tab is available.", log_file)
    def click_document_search(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.document_search_tab[1], "\"Documents Search\" Tab click.", log_file)

    # Find "No Match Found" string
    def locate_no_match_found_string(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.no_match_found_string_xpath[1], "\"No Match Found\" string located", log_file)

    # Find "Maximum date range is 90 days" string
    def locate_search_day_range_error_string(self,log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.day_range_error_xpath[1], "\"Maximum date range is 90 days\" string located", log_file)

    # Find print Reprint Receipt icon
    def locate_print_duplicate_receipt_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.print_duplicate_receipt_icon_xpath[1], "Print Duplicate Receipt is available", log_file)

    def click_on_print_duplicate_receipt_icon(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.print_duplicate_receipt_icon_xpath[1], "\"Click\" Duplicate Receipt button.", log_file)

    def locate_receipt_preview_dialog(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.receipt_preview_dialog_xpath[1], "Receipt Preview Dialog is located", log_file)

    def locate_receipt_preview_dialog_title(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.receipt_preview_dialog_title_xpath[1], "Receipt Preview Dialog's title is located", log_file)

    def close_receipt_preview_dialog(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.receipt_preview_dialog_close_btn[1], "Close button clicked on Duplicate receipt dialog.", log_file)

    def return_receipt_block_content(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.receipt_block_content_xpath[1]).text

    # Find send to Adninistrator  icon
    def locate_send_to_admin_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.send_to_admin_icon_xpath[1], "send to Admin icon is available", log_file)

    def click_on_send_to_admin_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.send_to_admin_icon_xpath[1], "send to Admin icon  clicked", log_file)

    def enter_reason(self, string = "test reason", log_file=""):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.enter_reason_xpath[1], "\"Enter Reason\" field is writable.", log_file, "", string)

    def enter_description(self, string = "test description", log_file=""):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.enter_description_xpath[1], "\"Enter Description\" field is writable.", log_file, "", string)

    def click_submit(self, log_file):  # TODO Log Just added, should be tested
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.submit_xpath[1], "\"Submit\" button click.", log_file)

    def locate_sent_message(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.sent_message_xpath[1], "message sent to admin", log_file)

    def locate_search_breadcrumb(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, OrderSearch.search_breadcrumb[1], "Located string in breadcrumb.", log_file)
    def return_search_breadcrumb(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.search_breadcrumb[1]).text


    def get_order_number_from_receipt_block(self, content):
        line_list = content.splitlines()
        for line in line_list:
            if "Order #" in line:
                return re.findall('\d+', line)[0]

    def get_doc_number_from_receipt_block(self,content,log_file):
        line_list = content.splitlines()
        i = 0
        for line in line_list:
            i += 1
            if "Document#" in line:
                if "Total Paid:" in line_list[i]:
                    Collect_log.simple_log(log_file,"The Order has no Document number")
                else :
                    return line_list[i]
            else:
                continue


    def get_recorded_date_from_receipt_block(self, content, log_file):
        line_list = content.splitlines()
        i = 0
        for line in line_list:
            i += 1
            if "Recorded Date" in line:
                if "Total Paid:" in line_list[i]:
                    Collect_log.simple_log(log_file, "The Order has no Document number and Recorded Date")
                else:
                    return line_list[i+1]
            else:
                continue


    def get_total_price_from_receipt_block(self, content):
        line_list = content.splitlines()
        i = 0
        for line in line_list:
            i += 1
            if "Total Paid:" in line:
                return re.findall('\d+', line)[0]
            else:
                continue

    ###------------------- Search Additional options field------------------------
    # ------------------------Name------------------------------#
    def get_name_option_field_value(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.name_field_xpath[1]).get_attribute('value')

    def fillIn_name_option_filed_value(self, name, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.name_field_xpath[1], "Click in \"Name\" field.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.name_field_xpath[1], "Write name in \"Name\" field", log_file, "", name)

    # ------------------------Tracking ID------------------------------#
    def get_trackingID_option_field_value(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.tracking_id_xpath[1]).get_attribute('value')

    def fillIn_trackingID_option_filed_value(self, trackingID, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.tracking_id_xpath[1], "Click in \"Tracking ID\" field.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.tracking_id_xpath[1], "Write in \"Tracking ID\" field", log_file, "", trackingID)

    # ------------------------Location ID------------------------------#
    def get_location_field_value(self):
        select_DDL_element = Select(Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.location_field_xpath[1]))
        selectedOption = select_DDL_element.first_selected_option
        return str(selectedOption.text)

    def get_location_field_options_list(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.location_field_xpath[1]).text

    def fillIn_location_option_filed_value(self,log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.location_field_xpath[1], "Click in \"Location\" field.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.location_field_xpath[1], "Select location in \"Location\" field", log_file, "", 'D')

    # ------------------------Department ID------------------------------#
    def get_department_field_value(self):
        select_DDL_element = Select(Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.department_field_xpath[1]))
        selectedOption = select_DDL_element.first_selected_option
        return str(selectedOption.text)

    def get_department_field_options_list(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH,OrderSearch.department_field_xpath[1]).text

    # ------------------------Origin ID------------------------------#
    def get_origin_field_value(self):
        select_DDL_element = Select(Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.origin_field_xpath[1]))
        selectedOption = select_DDL_element.first_selected_option
        return str(selectedOption.text)

    def get_origin_field_options_list(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.origin_field_xpath[1]).text

    # ------------------------Assigned To------------------------------#
    def get_assignedTo_field_value(self):
        select_DDL_element = Select(Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.assigned_to_field_xpath[1]))
        selectedOption = select_DDL_element.first_selected_option
        return str(selectedOption.text)

    # ------------------------Payment Type------------------------------#
    def get_payment_type_field_value(self):
        select_DDL_element = Select(Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.payment_type_field_xpath[1]))
        selectedOption = select_DDL_element.first_selected_option
        return str(selectedOption.text)

    def get_payment_type_field_options_list(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.payment_type_field_xpath[1]).text

    # ------------------------Transaction ID------------------------------#
    def get_transactionID_option_field_value(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.transaction_id_field_xpath[1]).get_attribute('value')

    #------------------------Serial Number------------------------------#
    def get_serial_number_option_field_value(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.serial_number_field_xpath[1]).get_attribute('value')

    def fillIn_serialNumber_option_filed_value(self, serialNumber, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.serial_number_field_xpath[1], "Click in \"Serial Number\" field.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.serial_number_field_xpath[1], "Write in \"Serial Number\" field", log_file, "", serialNumber)

    #------------------------Document Number Start------------------------------#
    def get_document_number_start_field_value(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.start_document_number_xpath[1]).get_attribute('value')

    def fillIn_document_number_start_filed_value(self, docNumber, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.start_document_number_xpath[1], "Click in \"Document Number Start\" field.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.start_document_number_xpath[1], "Write in \"Document Number Start\" field", log_file, "", docNumber)

    #------------------------Document Number end------------------------------#
    def get_document_number_end_field_value(self):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, OrderSearch.end_document_number_xpath[1]).get_attribute('value')

    def fillIn_document_number_end_filed_value(self, docNumber, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, OrderSearch.end_document_number_xpath[1], "Click in \"Document Number end\" field.", log_file)
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, OrderSearch.end_document_number_xpath[1], "Write in \"Document Number end\" field", log_file, "", docNumber)
	 # This method ignors the last ordernumber+1 in order search result list
    def read_order_number_in_search_ignor_error(self, row_number,log_file):    # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 10, By.XPATH, ".//*[@id='orderSearchBlock']/tbody/tr["+str(row_number)+"]/td[4]", msg="There is no row number-" + str(row_number) + " in Order Search list", file_path=log_file, ignor_errors= "yes").text

    def read_OrderedOn_field_ignor_error(self, row_number,log_file):    # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 10, By.XPATH, ".//*[@id='orderSearchBlock']/tbody/tr["+str(row_number)+"]/td[9]", msg="There is no row number-" + str(row_number) + " in Order Search list", file_path=log_file, ignor_errors= "yes").text


    #list of order_numbers_on Search page
    def get_all_order_numbers_on_search(self, log_file):
        List_order_numbers = []
        i = 1
        try:
            while OrderSearch.read_order_number_in_search_ignor_error(self, i, log_file):
                order_number = OrderSearch.read_order_number_in_search_ignor_error(self, i, log_file)
                List_order_numbers.append(order_number)
                i = i+1
        except:
            return List_order_numbers

    #List of Ordered_On
    def get_all_OrderedOon_fields_on_search(self, log_file):
        List_Ordered_On = []
        i = 1
        try:
            while OrderSearch.read_OrderedOn_field_ignor_error(self, i, log_file):
                Ordered_On = OrderSearch.read_OrderedOn_field_ignor_error(self, i, log_file)
                List_Ordered_On.append(Ordered_On)
                i = i+1
        except:
            return List_Ordered_On


class FrontOffice(Login):
    def __init__(self, driver):
        Login.__init__(self)

    # Store helper variables
    front_office_xpath              = (By.XPATH, ".//*[@id='frontoffice']/a")
    front_office_breadcrum_xpath    = (By.XPATH, ".//*[@id='wrapper']/div/section/div[1]/ul/li[2]")
    account_code_field_xpath        = (By.XPATH, ".//*[@id='AccountCode']")
    search_button_xpath             = (By.XPATH, ".//*[@id='searchCmpAccount']")
    list_all_button_xpath           = (By.XPATH, ".//*[@id='listAll']")
    first_account_row_xpath         = (By.XPATH, ".//*[@id='CompanyAccountResultTable']/tbody/tr")
    first_account_code_xpath        = (By.XPATH, ".//*[@id='CompanyAccountResultTable']/tbody/tr[1]/td[@data-column=\"AccountCode\"]")
    first_edit_icon_xpath           = (By.XPATH, ".//*[@id='CompanyAccountResultTable']/tbody/tr[1]/td[5]/a")
    company_user_daily_amount_xpath = (By.XPATH, ".//*[@id='companyAccountSearchForm']/div[3]/div[6]/table/tbody/tr/td[3]/input")


    # Open front office tab
    def go_to_front_office(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 5, By.XPATH, FrontOffice.front_office_xpath[1], "Balance Drawer clicked", log_file)

    # Find Front Office breadcrumb in opened page
    def locate_front_office_breadcrumb(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, FrontOffice.front_office_breadcrum_xpath[1], "Located \"Front Office\" string in breadcrumb.", log_file)
    def return_front_office_breadcrumb(self):  # TODO Log Just added, should be tested
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, FrontOffice.front_office_breadcrum_xpath[1]).text

    # Find account code field
    def locate_account_code_field(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 10, By.XPATH, FrontOffice.account_code_field_xpath[1], "Located \"Account Code\" field.", log_file)

    # Fill in account code field with the given string
    def fillIn_account_code_field(self, account_code, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, FrontOffice.account_code_field_xpath[1], "\"Account Code\" field is writable.", log_file, "", account_code)

    # Click on search button
    def click_on_search_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, FrontOffice.search_button_xpath[1], "\"Search\" button clicked.", log_file)

    # Click on List All button
    def click_on_list_all_button(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, FrontOffice.list_all_button_xpath[1], "\"List All\" button clicked.", log_file)

    # Get all founded accounts count
    def find_all_accounts_with_given_code(self):
        account_xpath =  ".//*[@id='CompanyAccountResultTable']/tbody/tr"
        return len(self.driver.find_elements_by_xpath(account_xpath))

    # Find the first account in the list
    def locate_first_acount_row_in_table(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 25, By.XPATH, FrontOffice.first_account_row_xpath[1], "First account founded", log_file)

    # Get the account code
    def get_account_code(self, row_number):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 5, By.XPATH, FrontOffice.account_code_field_xpath[1]).text

    # Click on edit button in the first row
    def click_on_edit_icon(self, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "click", 10, By.XPATH, FrontOffice.first_edit_icon_xpath[1], "\"Edit\" icon clicked.", log_file)

    # Find company users daily amount
    def locate_account_user_daily_amount(self, log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "return", 25, By.XPATH, FrontOffice.company_user_daily_amount_xpath[1], "User account daily amount founded.", log_file)

    # Input account max daily amount
    def input_account_user_daily_amount(self, daily_amount, log_file):
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "clear", 5, By.XPATH, FrontOffice.company_user_daily_amount_xpath[1])
        Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "send_key", 5, By.XPATH, FrontOffice.company_user_daily_amount_xpath[1], "\"Max Daily Amount\" field is writable.", log_file, "", daily_amount)

    # get daily amount
    def get_account_user_daily_amount(self,log_file):
        return Explicit_Wait.wait_for_element_to_be_present_and_log(self.driver, "read", 25, By.XPATH,
                                                                    FrontOffice.company_user_daily_amount_xpath[1], "", log_file).text


class NavigateToCRS():
    def perform_(self, driver, exe_file, env_url):

        # For FireFox:
        # subprocess.Popen(exe_file)

        # For Google Chrome
        autoit.send(Login.username)
        autoit.send(Login.password)

        driver.get(env_url)
        driver.switch_to.window(driver.current_window_handle)
        # driver.maximize_window()


class Actions(OrderFinalization, VoidOrderSummary, VoidOrderPayment,CaptureQueue, CaptureSummary, IndexingQueue,IndexingSummary,IndexingEntry,
              VerificationQueue, OrderSearch,  FrontOffice, UploadPopup,  BalanceDrawer, InitializeDrawer):
    def __init__(self):
        Login.__init__(self)

    #methods
    def login_to_crs(self, env_url, exe_file="C:/Python27/lib/User3_login.exe"):
        Login.setUp(self, env_url, exe_file)

    def go_to_order_entry(self, log_file = ''):
        OrderQueue.AddNewOrder(self, log_file)
        Initialization.Initialize(self, log_file)

    def go_to_order_summary(self, oit, guest_name_out, NoOfPages, log_file = ''):
        OrderEntry.Click_OIT_DDl(self)
        OrderEntry.select_OIT(self, oit, log_file)
        OrderEntry.InputGuestName(self, guest_name_out)
        OrderEntry.Input_NumberOfPages(self, NoOfPages, log_file)
        time.sleep(2)
        OrderEntry.Click_AddToOrder(self, log_file)

    def go_to_order_summary1(self,guest_name_out, account_email_out, account_name_out):

        guestField = self.driver.find_element_by_xpath(".//*[@id='CustomerName']")
        emailField = self.driver.find_element_by_id('AccountEmail')
        accountField = self.driver.find_element_by_id('accountName')


        if guestField.get_attribute('readonly') is None or not guestField.get_attribute('readonly'):
            OrderEntry.InputGuestName(self, guest_name_out)
        elif emailField.get_attribute('readonly') is None or not emailField.get_attribute('readonly'):
            OrderEntry.InputAccountEmail(self, account_email_out)
        elif accountField.get_attribute('readonly') is None or not accountField.get_attribute('readonly'):
            OrderEntry.InputAccountName(self, account_name_out)
        time.sleep(2)
        OrderEntry.Click_AddToOrder(self, log_file = '')


    def go_to_payment_screen(self, log_file):
        OrderSummary.click_on_checkout_button_in_Order_Summary(self, log_file)


    def go_to_order_finalization(self, pmethod, log_file):
        # if account == "yes":
        #     AddPayment.click_on_checkout_button_in_AddPayment(self, log_file)   # TODO Log Just added, should be tested
        # else:
        AddPayment.select_payment_method(self, pmethod, log_file)           # TODO Log Just updated, should be tested
        AddPayment.fill_amount(self, log_file)                              # TODO Log Just updated, should be tested
        AddPayment.click_on_checkout_button_in_AddPayment(self, log_file)   # TODO Log Just added, should be tested

    # This method selects the given payment method and fills given amount
    def select_payment_method_and_fill_ammount(self, pMethod, amount, i, log_file):
        AddPayment.select_payment_method(self, pMethod, log_file, i)
        AddPayment.fill_amount_by_value(self, amount, log_file, i)

    def go_to_capture_queue(self):
        capture_tab = self.driver.find_element(*CaptureQueue.capture_tab_xpath)
        capture_tab.click()

    def go_to_indexing_queue(self):
        indexing_tab = self.driver.find_element(*IndexingQueue.indexing_tab_xpath)
        indexing_tab.click()

    def go_to_verification_queue(self):
        verification_tab = self.driver.find_element(*VerificationQueue.verification_tab_xpath)
        verification_tab.click()

    def go_to_search_screen(self):
        search_tab = self.driver.find_element(*OrderSearch.search_tab_xpath)
        search_tab.click()

    def void_order(self, log_file = ''):
        OrderFinalization.click_on_void_button(self, log_file)
        VoidOrderSummary.click_on_void_button_voidordersummary_screen(self, log_file)
        VoidOrderPayment.Finalize_Void(self, log_file)
        # Explicit_Wait.wait_for_element_visibility(self, OrderFinalization.breadcrumb_xpath)
        OrderFinalization.verify_breadcrumb(self)

    def do_Initialization(self,log_file):
        Initialization.Initialize(self, log_file)


class Validate():
    @staticmethod
    def diffutile(golden,actual_value, log_file, extra_msg=""):
        if actual_value == golden:
            Collect_log.log(log_file, 'a+', "Comparing Golden vs Actual - " +  extra_msg + " - SUCCESS", 0)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Golden: " + golden,          0)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Actual: " + actual_value,    0)  # 0-Info, 1-Errror or 2-Warning

        else:
            Collect_log.log(log_file, 'a+', "Comparing Golden vs Actual - " +  extra_msg + "", 1)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Golden: " +  golden,         1)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Actual: " + actual_value,    1)  # 0-Info, 1-Errror or 2-Warning


    @staticmethod
    def grep_golden_in_actual(golden, actual_value, log_file, extra_msg=""):
        if re.search(golden, actual_value, re.I):
            Collect_log.log(log_file, 'a+', "Grepping Golden in Actual - " + extra_msg + " - SUCCESS", 0)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Golden: " + golden,          0)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Actual: " + actual_value,    0)  # 0-Info, 1-Errror or 2-Warning
        else:
            Collect_log.log(log_file, 'a+', "Comparing Golden in Actual - " + extra_msg + "", 1)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Golden: " + golden,          1)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', "- Actual: " + actual_value,    1)  # 0-Info, 1-Errror or 2-Warning

    @staticmethod
    def check_required_field_info(checkField, log_file, extra_msg=""):
        if checkField:
            Collect_log.log(log_file, 'a+', extra_msg+" is NOT filled", 1)  # 0-Info, 1-Errror or 2-Warning
        else :
            Collect_log.log(log_file, 'a+', extra_msg +" is filled successfully",0)  # 0-Info, 1-Errror or 2-Warning


    @staticmethod
    def check_isNumber(value, log_file, extra_msg=""):
        if value.isdigit():
            Collect_log.log(log_file, 'a+', "Found \"" + extra_msg + "\" " + value, 0)  # 0-Info, 1-Errror or 2-Warning
        else:
            Collect_log.log(log_file, 'a+', "Found \"" + extra_msg + "\" " + value, 1)  # 0-Info, 1-Errror or 2-Warning

    @staticmethod
    def diffutile_db(db_value, actual_value, log_file, extra_msg=""):
        if actual_value == db_value:
            Collect_log.log(log_file, 'a+', "Comparing db_value vs Actual " + extra_msg + " - SUCCESS", 0)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - db_value: " + str(db_value), 0)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Actual:\t" + str(actual_value), 0)  # 0-Info, 1-Errror or 2-Warning

        else:
            Collect_log.log(log_file, 'a+', "Comparing db_value vs Actual " + extra_msg + "", 1)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - db_value: " + str(db_value), 1)  # 0-Info, 1-Errror or 2-Warning
            Collect_log.log(log_file, 'a+', " - Actual:\t" + str(actual_value), 1)  # 0-Info, 1-Errror or 2-Warning

    @staticmethod
    def check_existence_of_document_in_db(cursor, docNum, extr_msg, log_file, database="VANGUARD_TX_DPB_NEW",tenant="VG48121"):
        find_last_few_rows = "SELECT TOP 10 * FROM [" + database + "].[" + tenant + "].[DOC_MASTER]\
                              ORDER BY DM_ID DESC"

        cursor.execute(find_last_few_rows)
        results = cursor.fetchall()
        flag = 0
        for row in results:
            if docNum == row[6]:
                flag = 1
                Collect_log.log(log_file, "a+", extr_msg + " Document is added ", 0)
                break;

        if flag == 0:
            Collect_log.log(log_file, "a+", extr_msg + " Document is not added ", 1)


class Explicit_Wait():
    @staticmethod
    def wait_for_element_visibility(self, selector):

        try:
            WebDriverWait(self.driver, 10) \
                .until(expected_conditions.visibility_of_element_located(selector))
        except NoSuchElementException:
            sys.exit(1)


    @staticmethod
    def wait_for_element_selected(self, value):
        subscription = self.driver.find_element_by_name("value")
        WebDriverWait(self.driver, 10) \
            .until(expected_conditions.element_to_be_selected(subscription))

    @staticmethod
    def wait_for_title_contain(self, text):
        WebDriverWait(self.driver, 10) \
            .until(expected_conditions.title_contains(text))

    @staticmethod
    # wait for the alert to present
    def wait_for_alert_present(self):
        alert = WebDriverWait(self.driver, 10) \
            .until(expected_conditions.alert_is_present())
        # get the text from alert
        alert_text = alert.text
        # check alert text
        self.assertTrue("Are you sure you would like to remove all products from your comparison?", alert_text)
        # click on Ok button
        alert.accept()

    @staticmethod
    # wait for dropdown all optiona are available
    def wait_for_ddl_all_options_visibility(self):
        WebDriverWait(self.driver, 10). \
            until(lambda s: s.find_element_by_id("select-language").get_attribute("length") == "3")

    @staticmethod
    def wait_for_element_clickable(driver, timeout, selector):
        try:
            element = WebDriverWait(driver, timeout).until(expected_conditions.element_to_be_clickable(selector))
            return element
        except TimeoutException as e:
            print(Fore.RED + "Timeout while checking for element '%s' to be clickable" % selector)
            sys.exit(1)

    @staticmethod
    def wait_for_element_to_be_present(driver, timeout, selector):
        try:
            element = WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(selector))
            return element
        except TimeoutException:
            print(Fore.RED + "Timeout while waiting for element '" + str(selector) + "' to be present")
            exit(0)



    @staticmethod
    def wait_for_element_to_be_present1(driver, timeout, selector, log_file = "run_out.log"):
        try:
            element = WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(selector))

            return element
        except TimeoutException:
            print(Fore.RED + "Timeout while waiting for element '" + str(selector) + "' to be present")
            exit(0)



    # @staticmethod
    # def log(file_path, attribute, msg, error_msg = 0):
    #     file = open(file_path, attribute)
    #
    #     if error_msg == 0:
    #         file.write('Info: ' + msg + '\n')
    #     elif error_msg == 1:
    #         file.write('Error: ' + msg + '\n')
    #     elif error_msg == 2:
    #         file.write('Warning: ' + msg + '\n')
    #     else:
    #         file.write('Script Error: ' + error_msg + ' should take 0-Info, 1-Errror or 2-Warning\n')
    #         exit(0)
    #
    #     file.close()




    @staticmethod
    def wait_for_element_to_be_present_and_log( driver, action="", timeout=3, By_strategy=By.XPATH, selector="", msg="None", file_path="./run_out.log", exit_="Exit_TRUE" , key_send="", selection="", assertion = "", ignor_errors="no"):
    # def wait_for_element_to_be_present_and_log(driver, action="return", timeout=3, By_strategy=By.XPATH, selector="", msg="None", file_path="", exit_="Exit_TRUE" , key_send="", selection=""):
        tuple_selector = (By_strategy, selector)
        try:
            if action == "click" or action == "c":
                WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector))
                WebDriverWait(driver, timeout).until(expected_conditions.element_to_be_clickable(tuple_selector)).click()
                # TODO time.sleep(2) should be removed from cases, here should be written if element is enabled and clickable then click on element
                # time.sleep(2)
                # assert assertion in driver.current_url
            elif action == "send_key" or action == "sk" or action == "fill":
                WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector)).send_keys(key_send)
            elif action == "return" or action == "r" or action == "read":
                # WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector))
                return WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector))
            elif action == "select" or action == "sl":
                element = WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector))
                Select(element).select_by_visible_text(selection)
            elif action == "clear":
                element = WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector)).clear()
            else:
                str1 = "Action Should be (click, send_key, return or select). You entered: \"" + action + "\"\n\nTest case CRASHED"
                Collect_log.log(file_path, 'a+', str1, 1)  # 0-Info, 1-Errror or 2-Warning
                Collect_log.save_screenShot(file_path, driver, False)
                exit(0)

            Collect_log.log(file_path, 'a+', msg, 0)      # 0-Info, 1-Errror or 2-Warning

        except TimeoutException:
        # except Exception as e:
            if selector == "cashdrawerinitialize-btn" or ignor_errors != "no":        # in future we can have a list of warning selectors
                Collect_log.log(file_path, 'a+', msg, 2)      # 0-Info, 1-Errror or 2-Warning
            else:
                Collect_log.log(file_path, 'a+', msg, 1)      # 0-Info, 1-Errror or 2-Warning
                # Collect_log.run_part_status(file_path, "FAIL")
                Collect_log.save_screenShot(file_path, driver, False)
                # Removed "raise Exception" part, as, if we are here, that means TC CRASHed, that is why we nee exit(0) here
                exit(0)

            #print(Fore.RED + "Timeout while waiting for element '" + str(selector) + "' to be present")
            # if exit_ == "Exit_TRUE" or exit_ == "Exit_true" or exit_ == "Exit_True" or exit_ == "exit_TRUE" or exit_ == "exit_true" or exit_ == "exit_True":
            #     exit(0)

    @staticmethod
    def wait_for_element_NOT_to_be_present_and_log(driver, action="", timeout=3, By_strategy=By.XPATH, selector="", msg="None", file_path="./run_out.log", exit_="Exit_TRUE", key_send="", selection="", assertion=""):
        tuple_selector = (By_strategy, selector)
        try:
            if action == "click" or action == "c": # TODO should be used on some cases
                while WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector)) and WebDriverWait(driver, timeout).until(expected_conditions.element_to_be_clickable(tuple_selector)).click():
                    continue

            # wait for element disappears then go ahead
            if action == "return" or action == "r" or action == "read":
                while WebDriverWait(driver, timeout).until(expected_conditions.presence_of_element_located(tuple_selector)):
                    continue

            else:
                str1 = "Action Should be (return). You entered: \"" + action + "\"\n\nTest case CRASHED"
                Collect_log.log(file_path, 'a+', str1, 1)  # 0-Info, 1-Errror or 2-Warning
                Collect_log.save_screenShot(file_path, driver, False)
                exit(0)

            Collect_log.log(file_path, 'a+', msg, 1)  # 0-Info, 1-Errror or 2-Warning

        except TimeoutException:
            Collect_log.log(file_path, 'a+', msg, 0)  # 0-Info, 1-Errror or 2-Warning

    @staticmethod
    def wait_for_fields_lookup(field,  log_file ="./run_out.log", timeout = 2000):
        try:
            time = 0
            while re.search("iconLoaderSmall",str(field.get_attribute("class"))) and time < timeout:
                time+=1
            if time == 2000:
                Validate.check_required_field_info(re.search("iconLoaderSmall", field.get_attribute("class"), re.I), log_file, "TIMEOUT :\"" + str(field.get_attribute("placeholder"))+"\" "+" field ")

        except TimeoutException:
            Collect_log.log(log_file, 'a+', TimeoutException, 0)  # 0-Info, 1-Errror or 2-Warning

    @staticmethod
    def is_attribtue_present(element,attribute, file_path="./run_out.log"):
        result = False;
        try :
            value = element.get_attribute(attribute);
            if (value != None ):
                result = True
        except TimeoutException:
            Collect_log.log(file_path, 'a+', TimeoutException, 0)
        return result

class Collect_log():

    @staticmethod
    def log(file_path, attribute, msg, error_msg=0):
        file = open(file_path, attribute)

        if error_msg == 0:
            file.write('Info:\t\t' + msg + '\n')
        elif error_msg == 1:
            file.write('Error:\t\t' + msg + '\n')
        elif error_msg == 2:
            file.write('Warning:\t' + msg + '\n')
        else:
            file.write('Script Error: ' + str(error_msg) + ' should take 0-Info, 1-Errror or 2-Warning\n')
            exit(0)

        file.close()

    @staticmethod
    def start_time(file_path):
        file = open(file_path, 'a+')
        start = time.asctime( time.localtime(time.time()) )
        file.write('Start Date: ' + str(start) + '\n\n')
        file.close()
        return start
        # startTime = datetime.now()

    @staticmethod
    def end_time(file_path):
        file = open(file_path, 'a+')
        end = time.asctime(time.localtime(time.time()))
        file.write('\nFinish Date: ' + str(end) + '\n')
        file.close()
        return end

    @staticmethod
    def duration_time(file_path, start):
        file = open(file_path, 'a+')
        duration = datetime.now() - start
        # file.write('Finish Date2: ' + str(datetime.now()) + '\n')
        file.write('Duration: ' + str(duration) + '\n')
        file.close()
        return duration

    @staticmethod
    def run_part_status(file_path, status):
        file = open(file_path, 'a+')
        file.write("\nRun part " + status + "\n")
        file.close()

    @staticmethod
    def simple_log(file_path, msg):
        file = open(file_path, 'a+')
        file.write( msg + '\n')
        file.close()


    @staticmethod
    def validate_part_status(file_path, status):
        file = open(file_path, 'a+')
        file.write("\nValidate part " + status + "\n")
        file.close()

    @staticmethod
    def save_screenShot(log_file, driver, isPass = False):
        screenshot_dir_path = log_file.replace("run_out.log", "")
        screenshot_path     = screenshot_dir_path + 'screeshot.png'

        # If there is screenshot and the TC passed, we need to remove the screen shot
        if os.path.isfile(screenshot_path):
            os.remove(screenshot_path)

        # Do screen shot for the current view only when TC fails
        if not isPass:
            try:
                driver.save_screenshot(screenshot_path)
            except:
                Collect_log.simple_log(log_file, "\nWarning:\t\tScreenshot doesn't saved. Webdriver is not reachable.")

        # Try to close driver and quit the whole browser
        try:
            Login.driver_close(driver, log_file)
            Login.driver_quit(driver, log_file) # TODO should be tested for all cases
        except:
            Collect_log.simple_log(log_file, "\nWarning:\t\tWebdriver was unexpectedly closed.")

    @staticmethod
    def test_case_exit(log_file, startTime, driver, isPass = False):
        # Save screenShot for failed TCs
        Collect_log.save_screenShot(log_file, driver, isPass)

        # Get TC end time
        Collect_log.end_time(log_file)
        Collect_log.duration_time(log_file, startTime)
        exit(0)


class DB_connect():
    @classmethod
    def connection_to_db(self, tenant_name):
        import pymssql
        for list in Config.db_con_list:
            if list[0] == tenant_name:
                db_con = pymssql.connect(list[2], list[4], list[3], list[5])
                cursor = db_con.cursor()
                return db_con, cursor, list

    @staticmethod
    def get_order_doc_number(order_num):    #TODO This part will be used only when service types are 1-Recording, 23-Marriage, 24-Fillings select * from [VANGUARD].SERVICE_TYPE
        import pymssql
        db = pymssql.connect("10.0.1.73", "sa", "Volo2020V", "VANGUARD_TX_DPB_NEW")
        cursor = db.cursor()

        querry = "use [VANGUARD_TX_DPB_NEW] \
                    SELECT dm_instnum \
                    FROM [vg48121].[ORDER], [VG48121].[ORDER_ITEM], [VG48121].[DOC_MASTER] \
                    WHERE [vg48121].[ORDER].ORDER_ID = [VG48121].[ORDER_ITEM].ORDER_ID and [VG48121].[ORDER_ITEM].DM_ID = [VG48121].[DOC_MASTER].DM_ID and ORDER_NUM = '%s'" % order_num

        try:
            cursor.execute(querry)
            results = cursor.fetchall()
            return results[0][0]

        except:
            print "Error: unable to fecth data \"" + order_num + "\""
        db.close()



    @staticmethod
    def get_order_status(order_status):
        import pymssql
        db = pymssql.connect("10.0.1.73", "sa", "Volo2020V", "VANGUARD_TX_DPB_NEW")
        cursor = db.cursor()

        querry = "use [VANGUARD_TX_DPB_NEW] SELECT ORDER_STATUS_DESC FROM [VANGUARD].[ORDER_STATUS] where ORDER_STATUS_DESC like '%" + order_status + "%'"

        try:
            cursor.execute(querry)
            results = cursor.fetchall()
            return results[0][0]

        except:
            print "Error: unable to fecth data \"" + order_status + "\""
        db.close()

