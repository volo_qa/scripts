#Owner:Nelli

import pymssql
from prettytable import PrettyTable
from CRS_Lib import DB_connect, Config

config = Config()
config.parse_config()


def Info_via_OrderNumber(database, county,cursor,  db):

    order_number = raw_input("Input order Number_________")

    #prepare sql_queries
    sql_info_via_order_number = "SELECT  oi.DM_ID, o.ORDER_DATE, o.ORDER_TOTAL,oi.WORKFLOW_STEP_ID, oit.ORDER_ITEM_TYPE_DISPLAY_NAME, oi.NOOFPAGES,\
         dt.DOC_TYPE_DESC,dg.DOC_GROUP_DESC, d.DEPT_DESC, op.PAYMENT_DATE, pm.PAYMENT_METHOD_NAME, dm.DM_INSTNUM, \
         dm.DM_RECORDED,oi.IS_VOIDED,  oit.HAS_COVER_PAGE,dp.PARTY_NAME1\
        FROM ["+ str(database) +"].[VG" + str(county) + "].[ORDER] o\
        INNER JOIN ["+ str(database) +"].[VG" + str(county) + "].[ORDER_ITEM] oi ON o.ORDER_ID = oi.ORDER_ID\
        INNER JOIN ["+ str(database) +"].[VG" + str(county) + "].[DOC_GROUP] dg ON oi.DOC_GROUP_ID = dg.DOC_GROUP_ID\
        INNER JOIN ["+ str(database) +"].[VG" + str(county) + "].[ORDER_ITEM_TYPE] oit ON oi.ORDER_ITEM_TYPE_ID = oit.ORDER_ITEM_TYPE_ID\
        Left JOIN ["+ str(database) +"].[VG" + str(county) + "].[ORDER_PAYMENT] op ON o.ORDER_ID=op.ORDER_ID\
        LEFT JOIN ["+ str(database) +"].[VG" + str(county) + "].[PAYMENT_METHOD] pm ON op.PAYMENT_METHOD_ID = pm.PAYMENT_METHOD_ID\
        LEFT JOIN ["+ str(database) +"].[VG" + str(county) + "].[DOC_MASTER] dm ON dm.DM_ID = oi.DM_ID\
        LEFT JOIN ["+ str(database) +"].[VG" + str(county) + "].[DM_PARTIES] dp ON oi.DM_ID = dp.DM_ID\
        LEFT JOIN ["+ str(database) +"].[VG" + str(county) + "].[DOC_TYPE] dt ON dm.DOC_TYPE_ID = dt.DOC_TYPE_ID\
        LEFT JOIN ["+ str(database) +"].[VANGUARD].[DEPT] d ON dm.DEPT_ID = d.DEPT_ID\
        WHERE o.ORDER_NUM = '%s' " % order_number


    try:
            t = PrettyTable(['DM_ID', 'ORDER_DATE', 'ORDER_TOTAL', 'WORKFLOW_STEP_ID','ORDER_ITEM_TYPE_DISPLAY_NAME','NOOFPAGES','DOC_TYPE_DESC','DOC_GROUP_DESC','DEPT_DESC','PAYMENT_DATE','PAYMENT_METHOD_NAME',
                                  'DM_INSTNUM','DM_RECORDED','IS_VOIDED','HAS_COVER_PAGE','PARTY_NAME1'])
            cursor.execute(sql_info_via_order_number)
            results = cursor.fetchall()
            for row in results:
                DM_ID = row[0]
                ORDER_DATE = row[1]
                ORDER_TOTAL = row[2]
                WORKFLOW_STEP_ID = row[3]
                ORDER_ITEM_TYPE_DISPLAY_NAME = row[4]
                NOOFPAGES = row[5]
                DOC_TYPE_DESC = row[6]
                DOC_GROUP_DESC = row[7]
                DEPT_DESC = row[8]
                PAYMENT_DATE = row[9]
                PAYMENT_METHOD_NAME = row[10]
                DM_INSTNUM = row[11]
                DM_RECORDED = row[12]
                IS_VOIDED = row[13]
                HAS_COVER_PAGE = row[14]
                PARTY_NAME1 = row[15]
                t.add_row([str(DM_ID), str(ORDER_DATE), str(ORDER_TOTAL), str(WORKFLOW_STEP_ID),str(ORDER_ITEM_TYPE_DISPLAY_NAME),str(NOOFPAGES),str(DOC_TYPE_DESC),str(DOC_GROUP_DESC),
                    str(DEPT_DESC),str(PAYMENT_DATE),str(PAYMENT_METHOD_NAME),str(DM_INSTNUM),str(DM_RECORDED),str(IS_VOIDED),str(HAS_COVER_PAGE),str(PARTY_NAME1)])
            print t


    except:
        #Rollback in case there is any error
        db.rollback()

        #disconnect from server
        db.close()

#Program start
print Config.all_env_names
environment_name = raw_input("Input environment name where you need to check you user:___ ")

#open database connection
db_con = DB_connect.connection_to_db(environment_name)
Info_via_OrderNumber(db_con[2][5],db_con[2][1],  db_con[1], db_con[2][0])

