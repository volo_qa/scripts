#Owner:Nelli

import pymssql
import datetime
import os
from CRS_Lib import DB_connect, Config

config = Config()
config.parse_config()

# it's log file for reporting previous and current location and usergroup
def Log(db, USER_NAME, previous, current):

        if os.path.exists('Log_user_change.html'):
            print ""

        else:
            fo = open("Log_user_change.html", "a+")
            fo.write("<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;} th, td {padding: 5px;}</style></head><body><table><tr><th width='200'>Database</th><th width='200'>Username</th><th width='200'>Previous</th><th width='200'>Current</th ><th width='189'>Date</th></tr></table></body></html>")

        x = datetime.datetime.now()
        fo = open("Log_user_change.html", "a+")
        fo.write("<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;} td {padding: 5px;}</style></head><body><table ><tr><td width='200'>" + str(db) + "</td><td width='200'>" + USER_NAME+ "</td><td width='200'>" + previous + "</td><td width='200'>" + current + "</td><td>" + str(x) + " </td></tr></table></body></html>")
        fo.close()



def RP_Vital(database, county, db ,cursor,  db_name):

    #input username and IP
    USER_NAME = raw_input("Please input username:")
    IP = raw_input("Please input your IP:")

    #prepare sql_queries
    sql_find_locationid_workstationid = "SELECT lws.LOCATION_ID, lws.WORK_STATION_ID  \
          FROM ["+ str(database) +"].[VG" + str(county) + "].[WORK_STATION] ws \
          INNER JOIN ["+ str(database) +"].[VG" + str(county) + "].[LOCATION_WORK_STATION] lws ON ws.WORK_STATION_ID = lws.WORK_STATION_ID \
          WHERE WORK_STATION_NAME LIKE '%s%%'" % IP

    sql_find_aduserid_adusergroupid = "SELECT auau.AD_USERGROUP_ID, au.ADUSER_ID  \
          FROM ["+ str(database) +"].[VG" + str(county) + "].[AD_USER] au \
          INNER JOIN ["+ str(database) +"].[VG" + str(county) + "].[AD_USER_AD_USERGROUP] auau ON au.ADUSER_ID = auau.ADUSER_ID \
          WHERE ADUSER_DOMAIN LIKE '%" + str(USER_NAME) + "%'"

    sql_update_location_adusergroup = "UPDATE VG" + str(county) + ".AD_USER_AD_USERGROUP SET AD_USERGROUP_ID = '%d' WHERE ADUSER_ID = '%d'\
                                       UPDATE VG" + str(county) + ".LOCATION_WORK_STATION SET LOCATION_ID = '%d' WHERE WORK_STATION_ID = '%d'"

    sql_location = "SELECT [LOCATION_ID],[LOCATION_NAME] FROM ["+ str(database) +"].[VG" + str(county) + "].[LOCATION]"
    sql_ad_usergroup = "SELECT  [AD_USERGROUP_ID] ,[AD_USERGROUP_NAME] FROM ["+ str(database) +"].[VG" + str(county) + "].[AD_USERGROUP]"



    try:
        #get you worstation_id, location_id, adusergroup_id, aduser_id
        cursor.execute(sql_find_locationid_workstationid)
        result_1 = cursor.fetchone()
        location_id = result_1[0]
        workstation_id = result_1[1]

        cursor.execute(sql_find_aduserid_adusergroupid)
        result_2 = cursor.fetchone()
        adusergroup_id = result_2[0]
        aduser_id = result_2[1]


        #get your location_name from DB
        cursor.execute(sql_location)
        result_location_names = cursor.fetchall()
        for row_location in result_location_names:
            if location_id == row_location[0]:
                previous_location = "Your location is:" + row_location[1]

        #get your usergroup_name from DB
        cursor.execute(sql_ad_usergroup)
        result_adusergroup_names  = cursor.fetchall()
        for row_usergroup in result_adusergroup_names:
             if adusergroup_id == row_usergroup[0]:
                 previous_usergroup = "Your usergroup is:" + row_usergroup[1]

        previous = str(previous_location) +"\n" +str(previous_usergroup)
        print previous

        print "\nHere is the locations and usergroups" + "\nAll Locations____" + str(result_location_names) + "\nAll usergroups____" \
              + str(result_adusergroup_names) + "\nInput location and usergroup numbers which you want to update "

        # lication_id and usergroup_id which you want to update
        update_location = eval(raw_input("location_id______ "))
        update_adusergroup = eval(raw_input("usergroup_id______ "))

        #update location and usergroup
        cursor.execute(sql_update_location_adusergroup, (update_adusergroup, aduser_id, update_location, workstation_id ))
        db.commit()

        for row_location in result_location_names:
            if update_location == row_location[0]:
                current_location ="Your updated location is:"+ row_location[1]

        for row_usergroup in result_adusergroup_names:
            if update_adusergroup ==row_usergroup[0]:
                current_usergroup = "Your usergroup is:" + row_usergroup[1]

        current = str(current_location) +"\n"+ str(current_usergroup)
        print current

        Log(db_name, USER_NAME, previous, current)

    except:
        #Rollback in case there is any error
        db.rollback()

        #disconnect from server
        db.close()

#Program start
print Config.all_env_names
environment_name = raw_input("Input environment name where you need to check you user:___ ")

#open database connection
db_con = DB_connect.connection_to_db(environment_name)
RP_Vital(db_con[2][5],db_con[2][1], db_con[0], db_con[1], db_con[2][0])

