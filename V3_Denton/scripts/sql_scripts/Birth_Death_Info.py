#Owner:Nelli

import pymssql
from prettytable import PrettyTable
from CRS_Lib import DB_connect, Config

config = Config()
config.parse_config()

def Birth_Death(database, county, cursor,  db):

    number = raw_input("For Birth Record input 1\nFor Death Record input 2\n____")

    #prepare sql_queries
    sql_info_birth = "SELECT TOP 20 p.PARTY_NAME1, DM_INSTRUMENT,\
    CASE \
    WHEN PTYPE_CODE ='C' THEN\
        'Birth'\
         ELSE 'Death'\
      END AS CERT\
      FROM ["+ str(database) +"].[VG" + str(county) + "].[DOC_MASTER] dm\
    JOIN ["+ str(database) +"].[VG" + str(county) + "].[DM_PARTIES] dp ON dp.DM_ID = dm.DM_ID\
    JOIN ["+ str(database) +"].[VG" + str(county) + "].[PARTIES] p ON p.PARTY_ID = dp.PARTY_ID\
    JOIN ["+ str(database) +"].[VG" + str(county) + "].[PARTY_TYPE] pt ON pt.PTYPE_ID = pt.PTYPE_ID\
    WHERE \
    PTYPE_CODE LIKE 'C'"



    sql_info_death = "SELECT TOP 100 p.PARTY_NAME1, DM_INSTRUMENT,\
    CASE \
    WHEN PTYPE_CODE ='C' THEN\
        'Birth'\
         ELSE 'Death'\
      END AS CERT\
      FROM ["+ str(database) +"].[VG" + str(county) + "].[DOC_MASTER] dm\
    JOIN ["+ str(database) +"].[VG" + str(county) + "].[DM_PARTIES] dp ON dp.DM_ID = dm.DM_ID\
    JOIN ["+ str(database) +"].[VG" + str(county) + "].[PARTIES] p ON p.PARTY_ID = dp.PARTY_ID\
    JOIN ["+ str(database) +"].[VG" + str(county) + "].[PARTY_TYPE] pt ON pt.PTYPE_ID = pt.PTYPE_ID\
    WHERE PTYPE_CODE LIKE  'DE'\
    ORDER BY CERT"

    try:
        if number==str(1):
            t = PrettyTable(['party_name', 'Birth_Death_Date', 'Type'])
            cursor.execute(sql_info_birth)
            results = cursor.fetchall()
            for row in results:
                party_name = row[0]
                Birth_Death_Date = row[1]
                Type = row[2]
                t.add_row([str(party_name), str(Birth_Death_Date), str(Type)])
            print t

        elif number==str(2):
            t = PrettyTable(['party_name', 'Birth_Death_Date', 'Type'])
            cursor.execute(sql_info_death)
            results = cursor.fetchall()
            for row in results:
                party_name = row[0]
                Birth_Death_Date = row[1]
                Type = row[2]
                t.add_row([str(party_name), str(Birth_Death_Date), str(Type)])
            print t
    except:
        print  "Error: unable to fecth data"

        #disconnect from server
        db.close()



#Program start
print Config.all_env_names

environment_name = raw_input("Input environment name where you need to check you user:___ ")

#open database connection
db_con = DB_connect.connection_to_db(environment_name)

#call Birth_Death function
Birth_Death(db_con[2][5],db_con[2][1], db_con[1], db_con[2][0])
