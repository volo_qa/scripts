#Owner:Nelli

import pymssql
from prettytable import PrettyTable
from CRS_Lib import DB_connect, Config

config = Config()
config.parse_config()

def Documents_with_images(database, county,cursor, db):

    #prepare sql_queries
    sql_doc_numbers_with_images = "WITH TOPTWO AS (\
                                   SELECT *, ROW_NUMBER()\
                                    over ( PARTITION BY DMC_PAGECOUNT order by  DM_INSTNUM desc) AS RowNo\
                                    FROM (\
                                     SELECT dm.DM_INSTNUM DM_INSTNUM, dc.DMC_PAGECOUNT DMC_PAGECOUNT\
                                     FROM [VANGUARD_TX_DPB_NEW].[VG48121].[DOC_MASTER] dm\
                                     INNER JOIN [VANGUARD_TX_DPB_NEW].[VG48121].[DM_CONTENT] dc ON dm.DM_ID = dc.DM_ID\
                                     INNER JOIN [VANGUARD_TX_DPB_NEW].[VANGUARD].[DEPT] d ON d.DEPT_ID = dm.DEPT_ID\
                                     WHERE d.DEPT_ID = '%d' AND dc.DM_CONTENT_TYPE_ID IN (1, 2, 6) AND dc.DMC_PAGECOUNT > 0 AND dc.DMC_PAGECOUNT < 122\
                                     )s \
                                    )\
                                    SELECT DM_INSTNUM, DMC_PAGECOUNT FROM TOPTWO WHERE RowNo <= 2\
                                    ORDER BY DMC_PAGECOUNT"

    sql_dept = "SELECT  DEPT_ID, DEPT_DESC FROM ["+ str(database) +"].[VANGUARD].[DEPT]"


    try:
        #get all departments
        table = PrettyTable(['DEPT_ID', 'DEPT_DESC'])
        cursor.execute(sql_dept)
        results = cursor.fetchall()
        for x in results:
            dept_id = x[0]
            dept_desc = x[1]
            table.add_row([str(dept_id), str(dept_desc)])
        print "\nHere are all departments\n " + str(table)

        input_dept = eval(raw_input("\nInput department nuber which doc numbers with images you need___"))
        t = PrettyTable(['DOC_NUMBER', 'DM_PAGECOUNT'])
        cursor.execute(sql_doc_numbers_with_images, input_dept)
        doc_numbers = cursor.fetchall()
        for row in doc_numbers:
            doc_number = row[0]
            pagecount = row[1]
            t.add_row([str(doc_number), str(pagecount)])
        print t


    except:
        #Rollback in case there is any error
        db.rollback()

        #disconnect from server
        db.close()


#Program start
print Config.all_env_names
environment_name = raw_input("Input environment name where you need to check you user:___ ")

#open database connection
db_con = DB_connect.connection_to_db(environment_name)
Documents_with_images(db_con[2][5],db_con[2][1], db_con[1], db_con[2][0])
