#Owner:Nelli

from CRS_Lib import DB_connect, Config

config = Config()
config.parse_config()


def Scheduler_Job_Update(database, county, db, tenant_id, scheduler_job_code, cursor,  db_name):


    sql_get_values = "SELECT IS_CANCELED, NEXT_EXECUTE_DATE  FROM ["+ str(database) +"].[VANGUARD].[SCHEDULE_JOB] WHERE TENANT_ID = "+ str(tenant_id )+" AND SCHEDULE_JOB_CODE = \'"+ str(scheduler_job_code) +"\'"

    sql_update_scheduler_job = "UPDATE  ["+ str(database) +"].[VANGUARD].[SCHEDULE_JOB] SET NEXT_EXECUTE_DATE = DATEADD(SECOND, 1, GETDATE()), IS_CANCELED = '0' \
                             WHERE TENANT_ID = "+ str(tenant_id )+"  AND SCHEDULE_JOB_CODE = \'"+ str(scheduler_job_code) +"\'"

    sql_update_is_canceled_value = "UPDATE ["+ str(database) +"].[VANGUARD].[SCHEDULE_JOB] SET IS_CANCELED = '1'\
                                 WHERE TENANT_ID = "+ str(tenant_id )+"  AND SCHEDULE_JOB_CODE = \'"+ str(scheduler_job_code) +"\'"



    try:
        cursor.execute(sql_get_values)
        result_before = cursor.fetchone()
        is_canceled_before = result_before[0]
        next_execute_date_before = result_before[1]
        print "\nIs_Canceled before update = " + str(is_canceled_before) + ", Next_Execute_Date before update = " + str(next_execute_date_before)

        cursor.execute(sql_update_scheduler_job)
        db.commit()
        cursor.execute(sql_get_values)
        result_after = cursor.fetchone()
        is_canceled_after = result_after[0]
        next_execute_date_after = result_after[1]
        print "Is_Canceled after update = " + str(is_canceled_after) + ", Next_Execute_Date after update = " + str(next_execute_date_after)



        if is_canceled_before == True:
            cursor.execute(sql_update_is_canceled_value)
            db.commit()
        else:
            print " "


    except:
        #Rollback in case there is any error
        db.rollback()

        #disconnect from server
        db.close()



#Program start
print Config.all_env_names
environment_name = raw_input("Input environment name where you need to check you user:___ ")

if environment_name == 'LocalHidalgo' or 'Hidalgo':

    #open database connection
    db_con = DB_connect.connection_to_db(environment_name)

    x = raw_input("Which scheduler job you want to update input appropriate number________\n scheduler_job for export-1\n scheduler_job for set export document-2")
    if x==str(1):
        export= 'ExportOneTime'
        Scheduler_Job_Update(db_con[2][5],db_con[2][1] ,db_con[0], 8, export,  db_con[1], db_con[2][0])

    elif x== str(2):
        DEPT = raw_input("\n Land Records-RP\n Plats-PL\n Birth Record -BR\n Marriage-ML\nDeath Certificates-DC\nAssumed Names-AN\nMilitary Discharges-MD\nInput department abrivation:__________ ")
        Scheduler_Job_Update(db_con[2][5],db_con[2][1] ,db_con[0], 8, "Lucene"+str(DEPT),  db_con[1], db_con[2][0])



elif environment_name =='LocalDenton' or 'Denton':

     #open database connection
     db_con = DB_connect.connection_to_db(environment_name)

     x = raw_input("Which scheduler job you want to update input appropriate number________\n scheduler_job for export-1\n scheduler_job for set export document-2")

     if x==str(1):
        export_type = raw_input("\n ExportOneTime-1\n ExportDaily-2\n ExportWeekly-3\n ExportMonthly -4\n ExportYearly-5\n Input appropriate number:__________ ")
        if export_type== str(1):
            export= 'ExportOneTime'
        elif export_type==str(2):
            export='ExportDaily'
        elif export_type==str(3):
            export='ExportWeekly'
        elif export_type==str(4):
            export='ExportMonthly'
        elif export_type==str(5):
            export='ExportYearly'
        Scheduler_Job_Update(db_con[2][5],db_con[2][1] ,db_con[0], 12, export, db_con[1], db_con[2][0])


     elif x== str(2):
        DEPT = raw_input("\n Land Records-1\n Plats-2\n UCC-3\n Vitals -5\n Marriage-6\n Deputations-7\nCommissioners Court-8\n"
                 "Death Certificates-9\nMarks and Brands-11\nMilitary Discharges-12\nForeclosures-13\n Press number of department:__________ ")
        Scheduler_Job_Update(db_con[2][5],db_con[2][1] ,db_con[0], 12, "EsExportDep"+str(DEPT), db_con[1], db_con[2][0])




