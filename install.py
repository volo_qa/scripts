import os
import shutil
from datetime import datetime

date = datetime.now()
version = "1"
main_script = "kofile_test.py"

daily_build  = version + "." + str(date.month) + "." + str(date.day)
hourly_build = version + "." + str(date.month) + "." + str(date.day) + "." + str(date.hour)

print "Start Building " + main_script
print "Daily_build  = " + daily_build
print "Hourly_build = " + hourly_build

# Create folder named Kofile_test_build_1.10.7
# Build = "Kofile_test_build_" + daily_build
Build = "Kofile_test_build_" + hourly_build
print "Creating " + Build + " build folder"

if os.path.exists(Build):
    shutil.rmtree(Build)

# Install kofile_test.py and get *.exe file (default path is: ".\dist\kofile\kofile.exe")
os.system("pyinstaller --distpath " + Build + " --onedir --console --name=\"kofile\" " + main_script)

shutil.copy2('./report.py', Build + '/kofile/report.py')
shutil.copy2('./kofile_test.py', Build + '/kofile/kofile_test.py')
shutil.copy2('./app-combined.css', Build + '/kofile/app-combined.css')
shutil.copytree('./V3_Denton', Build + '/kofile/V3_Denton')
shutil.copytree('./Common_scripts', Build + '/kofile/Common_scripts')
shutil.copytree('C:/Python27/Lib/site-packages/autoit', Build + '/kofile/autoit')

# os.system("pyinstaller --onedir --console --name=\"kofile.exe\" \"kofile_test.py\"")
# os.system("pyinstaller --onedir --windowed --name=\"kofile.exe\" \"kofile_test.py\"")
