import distutils
from distutils import dir_util
import os
import sys, getopt
import re
import subprocess
from subprocess import call
import shutil
import PyQt4
from PyQt4 import QtCore, QtGui
import ctypes
import getpass
import xml.etree.ElementTree as ET

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from Common_scripts.lib.CommonLib import Read_Config

#store a flag to detect if we have interface or running the script by terminal 
isInterface = False
log_root = ""
username = getpass.getuser()
kofile_test_script_dir_path = os.path.dirname(os.path.realpath(__file__))
date = datetime.now()
today = str(date.year) + "." + str(date.month) + "." + str(date.day)


class Ui_Form(QtGui.QWidget):

    # Helper private variables
    TC_Path_file = "TC_Path.txt"
    image = ""
    Denton_logo_path    = os.getcwd() + "//V3_Denton//common//images//Denton_logo"
    Hidalgo_logo_path   = os.getcwd() + "//V3_Denton//common//images//Hidalgo_logo"
    Volo_logo_path      = os.getcwd() + "//V3_Denton//common//images//VOLO_logo"

    # Store a variable, for selected current tenant configuration file path
    current_tenant_config_path=""
    current_version_dir = ""
    versionString = ""

    def setupUi(self, Form):
        Form.setWindowTitle("VOLO QA Automation tool")
        Form.setWindowFlags(QtCore.Qt.WindowMinimizeButtonHint)
        self.setWindowIcon(QtGui.QIcon(self.Volo_logo_path))
        self.setFixedSize(600,250)

        self.runTC_groupBox = QtGui.QGroupBox("Run TC")

        # Create radio buttons for tc run control
        self.runTcRadioButton = QtGui.QRadioButton("Run TC", self.runTC_groupBox)
        self.runTcRadioButton.setChecked(True)
        self.runGtlRadioButton = QtGui.QRadioButton("Run GTL", self.runTC_groupBox)

        # Create all needing fields for layout
        self.tcPathLabel = QtGui.QLabel("TC path: ")
        self.tcPathInput = QtGui.QLineEdit(self.runTC_groupBox)
        self.tcPathInput.setMinimumSize(450, 20)
        self.tcPathBrowseButton = QtGui.QPushButton("...", self.runTC_groupBox)
        self.tcPathBrowseButton.setFixedSize(30,20)
        self.tcLogPathLabel = QtGui.QLabel("TC log path: ")
        self.tcLogPathInput = QtGui.QLineEdit(self.runTC_groupBox)
        self.tcLogPathInput.setMinimumSize(450, 20)
        self.tcLogBrowseButton = QtGui.QPushButton("...", self.runTC_groupBox)
        self.tcLogBrowseButton.setFixedSize(30, 20)

        # Create run button and set connection
        self.runTCButton = QtGui.QPushButton("Run",self.runTC_groupBox)
        self.runTCButton.setFixedSize(60, 25)
        self.runTCButton.clicked.connect(self.TcRunButtonClicked)

        # Create tenant logo and dropDown list for choosing the tenant
        self.choose_tenant_comboBox = QtGui.QComboBox(Form)
        self.choose_tenant_comboBox.setFixedSize(300,20)
        self.logo_label = QtGui.QLabel(Form)
        self.fillInTenantComboBox()
        font = QtGui.QFont("Arial", 12, QtGui.QFont.Bold)
        self.chooseTenant_label = QtGui.QLabel("Select tenant:", Form)
        self.chooseTenant_label.setFont(font)

        # Create Layout for teant logo and dropDown list
        self.tenatLayout = QtGui.QGridLayout()
        self.tenatLayout.addWidget(self.logo_label, 0, 0, 1, 5)
        self.helper_layout = QtGui.QGridLayout()
        self.helper_layout.addWidget(self.chooseTenant_label,0, 0, 1, 3, QtCore.Qt.AlignBottom)
        self.helper_layout.addWidget(self.choose_tenant_comboBox, 1, 0, 1, 1)
        self.tenatLayout.addLayout(self.helper_layout, 0, 1, 3, 3)


        ## create a grid layout for "Run TC" groupBox
        self.gridLayout = QtGui.QGridLayout()

        # Add radio buttons
        self.gridLayout.addWidget(self.runTcRadioButton, 0, 1 , 1, 1)
        self.gridLayout.addWidget(self.runGtlRadioButton, 0, 2, 1, 1)

        #Add labels
        self.gridLayout.addWidget(self.tcPathLabel, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.tcPathInput, 1, 1, 1, 3, QtCore.Qt.AlignTop)
        self.gridLayout.addWidget(self.tcPathBrowseButton, 1, 4, 1, 1)

        self.gridLayout.addWidget(self.tcLogPathLabel, 2, 0, 1, 1, QtCore.Qt.AlignTop)
        self.gridLayout.addWidget(self.tcLogPathInput, 2, 1, 1, 3, QtCore.Qt.AlignTop)
        self.gridLayout.addWidget(self.tcLogBrowseButton, 2, 4, 1, 1)

        # Add run TC button
        self.gridLayout.addWidget(self.runTCButton, 3, 2, 1, 1, QtCore.Qt.AlignBottom)

        # Add the layout to "Run TC" groupbox
        self.runTC_groupBox.setLayout(self.gridLayout)

        # Create ui layout
        self.UI_layout = QtGui.QVBoxLayout()
        self.UI_layout.addLayout(self.tenatLayout)
        self.UI_layout.addWidget(self.runTC_groupBox)
        self.setLayout(self.UI_layout)


        # Set widget connections
        self.tcPathBrowseButton.clicked.connect(self.TcPathBrowseButtonClicked)
        self.tcLogBrowseButton.clicked.connect(self.TcLogPathBrowseButtonClicked)
        self.runTcRadioButton.clicked.connect(self.setTCFieldState)
        self.runGtlRadioButton.clicked.connect(self.setTCFieldState)
        self.choose_tenant_comboBox.currentIndexChanged.connect(self.onComboBoxChanged)

        QtCore.QMetaObject.connectSlotsByName(Form)

    # Fill in tenant combobox with current list in "Config.txt"
    def fillInTenantComboBox(self):
        # Getting Env name and Url from all xml configs, to show them in Drop-Down-List of UI
        # checking if could get list of environments data
        if not obj.version_plus_name_plus_url:
            errorMessage = "kofile_test Error: Didn't get Version, TenantName and Url to show in UI ddl!"
            ctypes.windll.user32.MessageBoxA(0, errorMessage, "Error", 0)
            exit(0)

        # Add all tenants and appropriate URLs into combobox
        for version_env_name_and_url in obj.version_plus_name_plus_url:
            self.choose_tenant_comboBox.addItem(version_env_name_and_url)

        # Set Denton logo for the begining
        self.image = QtGui.QPixmap(self.Denton_logo_path)
        self.logo_label.setPixmap(self.image.scaled(80, 80, QtCore.Qt.KeepAspectRatio))

        # This is for later filling of config xml
        self.get_selected_tenant_config_path()
        self.configFileNewContent = self.changeConfigFile()

    # This function is called when tenant in combobox is changed
    def onComboBoxChanged(self, index):
        boxText = self.choose_tenant_comboBox.currentText()
        if boxText.contains("Denton"):
            self.image = QtGui.QPixmap(self.Denton_logo_path)
        elif boxText.contains("Hidalgo"):
            self.image = QtGui.QPixmap(self.Hidalgo_logo_path)

        # Set Logo
        self.logo_label.setPixmap(self.image.scaled(80, 80, QtCore.Qt.KeepAspectRatio))

        # store config file content
        self.get_selected_tenant_config_path()
        self.configFileNewContent = self.changeConfigFile()

        # Make sure that specified TC path belongs to specified version
        TC_path_txt = self.tcPathInput.text()
        if TC_path_txt:
            if not (self.versionString in TC_path_txt):
                errorMessage = "Specified TC or GTL doesn't belong to selected version.\n Please Select appropriate TC or GTL."
                ctypes.windll.user32.MessageBoxA(0, errorMessage, "Error", 0)
                self.tcPathInput.clear()

    # This function is added to handle config file changes when a tenant selected from ui.
    # If tenant changed, the function reads from "Config.txt" file, and change the content appropriatly.
    # All environments, except of tenant which was selected, are made to be comment
    # The function returns "Config.txt" file's changed content.
    def changeTXTConfigFile(self):
        # get current environment from comboBox
        boxText = self.choose_tenant_comboBox.currentText()
        envURL = re.search(".*\s-\s(.*)", boxText).group(1)

        # Read config file content
        config_file = os.getcwd()+"\\V3_Denton\\common\\configs\\Config.txt"
        with open(config_file) as file:
            config = file.readlines()

        # Store line index in file
        lineIndex=0
        for line in config:
            # We need to change only lines wich contains environment
            if re.search("Environment.*\:\s*.*", line):
                environment_str = re.search("Environment.?\:\s*(.*)", line).group(1)
                # If environment is not the selected one, comment it
                if (environment_str != envURL):
                    if not re.search("^#", line):
                        line = '#' +line
                else: # otherwise remove '#' if exists
                    if re.search("^#", line):
                        line = line.replace("#", "")

                #print "$$$$$$$$$$$$$$$$  ", line
                config[lineIndex]=line
            # Increment line number
            lineIndex=lineIndex+1

        # return Config file's edited content
        return config

    # This function is added to handle config file changes when a tenant selected from ui.
    # If tenant changed, the function reads from "Configuration.xml" file, and change the content appropriately.
    # The selected environment's "value" attribute's value is set 'default'.
    # The function returns "Configuration.xml" file's changed content.
    def changeConfigFile(self):
        # Read config file content
        root = ET.parse(self.current_tenant_config_path).getroot()

        # Run over all 'VersionConfig' tags and handle tenant change
        for VersionConfig in root.findall('VersionConfig'):
            # Iterate over all tenants and remove 'default' value from it's 'value' attribute
            for TenantConfig in VersionConfig:
                if TenantConfig.attrib['value'] == 'default':
                    TenantConfig.attrib['value'] = ''

            # Get current environment from comboBox
            boxText = self.choose_tenant_comboBox.currentText()
            envURL = re.search(".*\s-\s(.*)", boxText).group(1)

            # Set value of 'value' attribute, for the selected tenant
            for TenantConfig in VersionConfig:
                if TenantConfig.attrib['Url'] == envURL:
                    TenantConfig.attrib['value'] = 'default'
                    break

        # Return file's changed content
        return ET.tostring(root)

    # This function is added to parse combo box current value, and get configuration path
    def get_selected_tenant_config_path(self):
        # Get working directory
        config_file_path = kofile_test_script_dir_path
        if not re.search("(\\\\)$|(\/)$", config_file_path):
            config_file_path = config_file_path + "\\"

        # Iterate over all files and get directories
        dirs = [d for d in os.listdir(config_file_path) if os.path.isdir(config_file_path + d)]

        # Get current environment from comboBox
        boxText = self.choose_tenant_comboBox.currentText()

        # Find selected version
        versionMatch =  re.search("^(V\d).+", boxText)
        if versionMatch:
            self.versionString = versionMatch.group(1)

        # get the config path in selected version dir
        for version_dir in dirs:
            match = re.search("^(V\d).+", version_dir)
            if match and match.group(1) == self.versionString:
                config_dir_path = config_file_path + version_dir + "\\common\\configs\\"

                # If we get valid path, check it and break the loop
                if not os.path.isdir(config_dir_path):
                    errorMessage =  "Error: %s dir is not present under %s"% (config_dir_path, version_dir)
                    ctypes.windll.user32.MessageBoxA(0, errorMessage, "Error", 0)
                    exit(0)

                # Initialize private variable
                self.current_tenant_config_path = config_dir_path + "Configuration.xml"
                self.current_version_dir        = config_file_path + version_dir
                break


    # Slot for clearing TC input path
    def setTCFieldState(self):
        self.tcPathInput.clear()

    # This function is called when Run button clicked
    def TcRunButtonClicked(self):
        tcPath = str(self.tcPathInput.text())
        tcLogPath = str(self.tcLogPathInput.text())

        if tcPath:
            if not (str(self.versionString) in tcPath):
                errorMessage = "Specified TC or GTL doesn't belong to selected version.\n Please Select appropriate TC or GTL."
                ctypes.windll.user32.MessageBoxA(0, errorMessage, "Error", 0)
                self.tcPathInput.clear()
                return

        if self.runTcRadioButton.isChecked() or self.runGtlRadioButton.isChecked():
            kofile_TC_runner(tcPath, tcLogPath, self.current_version_dir, self.configFileNewContent)

    # This function is called when TC path browse button clicked
    def TcPathBrowseButtonClicked(self):
        TC_path_txt = self.tcPathInput.text()
        filePath=''
        if self.runTcRadioButton.isChecked():
            if TC_path_txt and os.path.isdir(TC_path_txt):
                filePath = QtGui.QFileDialog.getExistingDirectory(QtGui.QFileDialog(), "Open TC", TC_path_txt, QtGui.QFileDialog.ShowDirsOnly)
            else:
                filePath = QtGui.QFileDialog.getExistingDirectory(QtGui.QFileDialog(), "Open TC", "E:\\", QtGui.QFileDialog.ShowDirsOnly)
        elif self.runGtlRadioButton.isChecked():
            if TC_path_txt and os.path.isfile(TC_path_txt):
                filePath = QtGui.QFileDialog.getOpenFileName(QtGui.QFileDialog(), "Open gtl", TC_path_txt, '*.gtl *.tl')
            else:
                filePath = QtGui.QFileDialog.getOpenFileName(QtGui.QFileDialog(),"Open gtl", "E:\\", '*.gtl *.tl')

        filePath.replace("/", "\\")

        # Check if the selected TC belongs to the selected tenant version
        if filePath:
            if self.versionString in filePath:
                self.tcPathInput.setText(filePath)
            else:
                errorMessage = "Specified TC or GTL doesn't belong to selected version.\n Please Select appropriate TC or GTL."
                ctypes.windll.user32.MessageBoxA(0, errorMessage, "Error", 0)
                self.tcPathInput.clear()


    # This function is called when log path browse button clicked
    def TcLogPathBrowseButtonClicked(self):
        dirPath = QtGui.QFileDialog.getExistingDirectory(QtGui.QFileDialog(), "Open TC", "E:\\", QtGui.QFileDialog.ShowDirsOnly)
        dirPath.replace("/", "\\")
        self.tcLogPathInput.setText(dirPath)

    # handle all key events
    def keyPressEvent(self, event):
        # IF pressed Enter key, run the TC
        if event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter:
            self.TcRunButtonClicked()
        # If pressed Esc key, quit the program
        if event.key() == QtCore.Qt.Key_Escape:
            sys.exit(0)

    # Set startup TC path in UI
    def setTCPath(self):
        if (os.path.isfile(self.TC_Path_file)):
            lines = tuple(open(self.TC_Path_file, 'r'))
            if len(lines) != 0 and len(lines) < 3:
                # Get and set TC path
                TC_path = lines[0].rstrip()
                if TC_path:
                    self.tcPathInput.setText(TC_path)

                    # If in file written gtl path, set gtl radiobutton checked
                    if re.search("\.g?tl$", TC_path):
                        self.runGtlRadioButton.setChecked(True)
                # Check if log dir is also given
                if (len(lines) == 2):
                    log_path = lines[1].rstrip()
                    if log_path:
                        self.tcLogPathInput.setText(log_path)
        else:
            open(self.TC_Path_file, "w")

    # Handle close event, need to save TC and log paths
    def closeEvent(self, event):
        # Open TC_Path.txt file to write in it
        if (os.path.isfile(self.TC_Path_file)):
            file = open(self.TC_Path_file, 'w')
            TC_path = self.tcPathInput.text()
            log_path = self.tcLogPathInput.text()
            file.write(TC_path)
            file.write('\n')
            file.write(log_path)
            file.close()

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.setTCPath()


############################################## START ################################################
# python test.py arg1 arg2

def help():
    print "\n\t\tUsage:", str(sys.argv[0]), ' <TestCase.tc or GroupTestList.gtl> ',"log_dir\n"
    #print 'Number of arguments:', len(sys.argv), 'arguments.'

def add_backslash_AtTheEndOfPath(path):
    if not re.search("(\\\\)|(\/)$", path):
        path += "\\"
    return path

def remove_backslash_AtTheEndOfPath(path):
    # print "Initiall path:", path
    if re.search("(.+)(\\\\|\/)$", path):
        path = re.search("(.+)(\\\\|\/)$", path).group(1)
        print "New Path", path
    # else:
    #     print "No Backslash"
    return path

# define error message reporting function
def return_error_message(message):
    # if we are working with interface open error message dialog
    if isInterface:
        ctypes.windll.user32.MessageBoxA(0, message, "Error", 0)
    else: #otherwise simply print error message
        print message

def create_new_logDir(message):

    # if we are working with interface open error message dialog
    if isInterface:
        answer=ctypes.windll.user32.MessageBoxA(0, message + "! Create?", "Log file doesn't exist", 4)
    else: #otherwise simply print error message
        print message
        answer = raw_input("\tCreate ? [y/n]:> ")

    #Create a log dir depending on answer
    if answer == "y" or answer == "Y" or answer == "yes" or answer == "YES" or answer == "Yes" or answer == 6:
        return True
    else:
        return False

def log_dir_contain_log(message):
    # if we are working with interface open error message dialog
    if isInterface:
        answer = ctypes.windll.user32.MessageBoxA(0, message + "Create in specified directory?", "Log file doesn't exist", 4)
    else: #otherwise simply print error message
        answer = raw_input("\n\tCreate in specified directory? [y/n]:> ")

    #Create a log dir depending on answer
    if answer == "y" or answer == "Y" or answer == "yes" or answer == "YES" or answer == "Yes" or answer == 6:
        return True
    else:
        return False

########################################################################
# Definition of  kofile_TC_runner function
# This function is the main engine of automated environment
# The function steps are:
# 1) Validate TC/GTL directory
# 2) Validate log dir
# 3) Check 'log' named dir presence in given log dir
# 4) Copy CRS_lob into C:\Python27\Lib
# 5) Execution part
########################################################################
def kofile_TC_runner(testCaseOrGTL, log_root, environment_path = "",  configFileContent = ""):
    # testCaseOrGTL = sys.argv[1] #  E:\Kofile\V3_Denton\test_suites\Regression.ts\admin_key_assign_to_any_clerk.tc
    # log_root  = sys.argv[2] # "D:\log"
    #testCaseOrGTL = "E:\\Kofile\\V3_Denton\\test_suites\\All_Order_item_type_finalization.ts\\Account_Payment.tc"
    #testCaseOrGTL = "D:\\Kofile\\V3_Denton\\test_suites\\All_Order_item_type_finalization.ts\\All_Order_item_type_finalization.gtl"
    #log_root  = "D:\\log"
    #log_root  = "D:\\log"
    isTC  = False
    isGTL = False
    testCaseName = ""
    # kofile_test_script_dir_path = os.path.dirname(os.path.abspath('__file__'))
    pythonLib = "C:\Python27\Lib"
    isUserExists = False
        
    #------------------------- TC/gtl dir validation -------------------------
    # removing '\' at the end of given testCase Or GTL
    testCaseOrGTL = remove_backslash_AtTheEndOfPath(testCaseOrGTL)

    # determining is test case or is gtl # cheking if valid
    if not re.search("(\.tc|\.g?tl)$", testCaseOrGTL):
        errorMessage = "The first argument should be specified as either path/TestCase.tc (with ending .tc),or path/GroupTestList.gtl (with ending .gtl) "
        return_error_message(errorMessage)
        help()
        return
    elif re.search("\.tc$", testCaseOrGTL) and os.path.isdir(testCaseOrGTL):
        print(".tc is given !")
        isTC  = True
        isGTL = False
    elif re.search("\.g?tl$", testCaseOrGTL) and os.path.isfile(testCaseOrGTL):
        print(".gtl is given !")
        isTC  = False
        isGTL = True
    else:
        errorMessage = "Error: The given is not a valid dir(in case of .tc) or file(in case of .gtl, .tl):%s Exiting the script." % (testCaseOrGTL)
        return_error_message(errorMessage)
        return


    #------------------------- Log dir validation -------------------------
    # adding username and '\' at the end of given log dir
    if not re.search("(\\\\|\/)$", log_root):
        log_root = log_root.replace("log", str(username) + "_" + str(today) + "_log")      #TODO add support for  upper cases
        log_root += "\\"
    # checking if given log dir exist
    if not os.path.isdir(log_root):
        logDirMessage = "The log dir you have specified doesn't exist: %s" %(log_root)
        if create_new_logDir(logDirMessage):
            os.makedirs(log_root)
            print "\t- Made directory: ", log_root, "\n"
        else:
            print "\n\tExiting the script."
            return

    # added by Mher test ( lines are in incorrect place - they shouldnt be in Log part, instead, they should be in COPY part )
    # print "kofile_test_script_dir_path = " + kofile_test_script_dir_path
    # if os.path.isdir(kofile_test_script_dir_path + "\\Kofile"):
    #     os.system("xcopy /s /Y %s %s" % (kofile_test_script_dir_path + "\\Kofile", log_root))

    # checking 'log' named dir presence in given log dir argument
    if not re.search("(log\\\\|log\/)$", log_root, re.I):
        warnMessage = "Info: Dir with the name 'log' is necessary to be present to store all log data.\nWarning: Dir with the name 'log' is not found in \"%s\".\n" % (log_root)
        if log_dir_contain_log(warnMessage):
            log_root = log_root + "log"
            os.makedirs(log_root)
            print "\t- Made directory:", log_root
        else:
            print "\n\tExiting the script."
            return

    #------------------------- Lib part (CRS_Lib into Python27 lib copy part) -------------------------
    # Detecting environment_path. E.g.  D:\Kofile\V3_Denton
    """if re.search("Kofile", testCaseOrGTL, re.I):
        environment_path = re.search("(.*Kofile(\\\\|\/)['\w_-]*).*", testCaseOrGTL, re.I).group(1)
        # print("1. " + environment_path)
    elif re.search("Kofile", os.getcwd() + "\\" + testCaseOrGTL , re.I):
        print "current path:", os.getcwd()
        environment_path = re.search("(.*Kofile(\\\\|\/)['\w_-]*).*", os.getcwd() + "\\" + testCaseOrGTL , re.I).group(1)
        # print("2. " + environment_path)
    else:
        message = "Something went wrong: Couldn't detect 'Kofile' word in path Please check given TestCase/gtl name: %s" %(testCaseOrGTL)
        return_error_message(message)
        return"""

    # 1) After we have environment_path , looking if we have correct lib dir in specific environment. E.g.  D:\Kofile\V3_Denton\common\libs\ .
    # 2) Cheking if lib files are present in lib_dir and coping it ito original C:\Python27 lib
    lib_dir = environment_path + "\\" + "common\\libs\\"
    lib_file = lib_dir + "CRS_Lib.py"
    lib_file2 = lib_dir + "Required_fileds.py"
    lib_file3 = lib_dir + "fields_values"
    if os.path.isdir(lib_dir):
        if os.path.isfile(lib_file) and os.path.isfile(lib_file2):
            # TODO - FutureAction: check if C:\pyth27\ exists
            # distutils.dir_util.copy_tree(lib_file, "C:\Python27\libs")
            # shutil.copyfile(lib_file, "C:\Python27\libs")
            os.system("xcopy /s /Y %s %s" % (lib_file, pythonLib))
            os.system("xcopy /s /Y %s %s" % (lib_file2, pythonLib))
            os.system("xcopy /s /Y %s %s" % (lib_file3, pythonLib))
        else:
            message = "Lib files: 'CRS_Lib.py' and/or 'Required_fileds.py' and/or 'fields_values' file is not present at the: %s" % (lib_dir)
            return_error_message(message)
            return
    else:
        errorMessage = "Error: Lib directory doesnt exist: %s" % (lib_dir)
        return_error_message(errorMessage)
        return


    config_dir = environment_path + "\\" + "common\\configs\\"
    config_file = config_dir + "Configuration.xml" # already xml
    if os.path.isdir(config_dir):
        if os.path.isfile(config_file):
            os.system("xcopy /s /Y %s %s" % (config_file, pythonLib)) # already xml
        else:
            errorMessage = "Config file: 'Configuration.xml' is not present at the: %s" % ( config_dir)
            return_error_message(errorMessage)
            return
    else:
        errorMessage = "Error: Config directory doesn't exist: %s" % (config_dir)
        return_error_message(errorMessage)
        return

    # Print Configuration.xml file's content into the file wich was copied to C:\Python27 lib
    if configFileContent != "":
        with open("C:/Python27/Lib/Configuration.xml", 'w') as file:
            file.writelines(configFileContent)

    #
    # config_dir = environment_path + "\\" + "common\\configs\\"
    # config_file = config_dir + "Config.txt"
    # if os.path.isdir(config_dir):
    #     if os.path.isfile(config_file):
    #         os.system("xcopy /s /Y %s %s" % (config_file, pythonLib))
    #     else:
    #         print "\n\tConfig file: 'Config.txt' is not present at the: ", config_dir
    #         exit(0)
    # else:
    #     print "\n\tError: Config directory doesn't exist: ", config_dir
    #     exit(0)


    # This part is removed as folowing user were used with firefox only.
    # users_dir = environment_path + "\\" + "common\\users\\"
    # if os.path.isdir(users_dir):
    #     for user in os.listdir(users_dir):
    #         if user.endswith(".exe"):
    #             os.system("xcopy /s /Y %s %s" % (users_dir + user, pythonLib))
    #             isUserExists = True
    #
    #     if not isUserExists:
    #         print "\n\tWarning: No any 'user.exe' credentials file in:", users_dir
    #
    # else:
    #     errorMessage = "Error: Config directory doesn't exist: %s" % (users_dir)
    #     return_error_message(errorMessage)
    #     return



    # ---------  Directories hierarchy COPY into LOG part -------------------
    # TODO  old- exclude .git folder , it is not necessary to copy as it brings error for our script
    # TODO - uncomment and test in real examples
    # distutils.dir_util.copy_tree(kofile_test_script_dir_path+"\\V3_Denton", log_root)


    # this check added to support Visual Studio run. this case apperears when default script execution directory is: ./Kofile/kofile_test.py  but not ./kofile_test.py
    print "kofile_test_script_dir_path = " + kofile_test_script_dir_path
    if os.path.isdir(kofile_test_script_dir_path + "\\Kofile"):
        os.system("xcopy /s /Y %s %s" % (kofile_test_script_dir_path + "\\Kofile", log_root))

    # else copy all current directory into LOG  ( current dir contains V3_Denton, kofile_test.py, etc.)
    else:
        ignorePatterns = ('.git')
        tmp = "./kofile_tmp"
        if os.path.isdir(tmp):
            distutils.dir_util.remove_tree(tmp)
        shutil.copytree(kofile_test_script_dir_path, tmp, ignore=shutil.ignore_patterns(ignorePatterns))
        distutils.dir_util.copy_tree(tmp, log_root)
        distutils.dir_util.remove_tree(tmp)

        # Internet example
        #shutil.copytree(kofile_test_script_dir_path, "__TMP__", ignore=shutil.ignore_patterns(ignorePatterns))
        #distutils.dir_util.copy_tree("__TMP__", log_root)
        #distutils.dir_util.remove_tree("__TMP__")





    #------------------------- Execution Part -------------------------
    def exec_tc(path, what_is):
        #print "path:", path # don't use
        testCaseName = re.search("(['\w_-]+)\.tc(\\\)?|(\/)?$", path, re.I).group(1)

        # print ">>>>>>>>>>>>>>>> Log_root: ", log_root
        log_dir = log_root # initially
        if re.search("Kofile", testCaseOrGTL, re.I):
            log_dir = re.search(".*Kofile(\\\\|\/)(['\w_-]*.*)", path, re.I).group(2)
            print("1. log_dir: " + log_dir)
        elif re.search("Kofile", os.getcwd() + "\\" + testCaseOrGTL , re.I):
            print "current path:", os.getcwd()
            log_dir = re.search(".*Kofile(\\\\|\/)(['\w_-]*.*)", os.getcwd() + "\\" + path , re.I).group(2)
            print("2. log_dir: " + log_dir)
        else:
            errorMessage = "Something went wrong: Couldn't detect 'Kofile' word in path. Please check given TestCase/gtl name: %s" % (testCaseOrGTL)
            return_error_message(errorMessage)
            return

        log_dir = log_root + log_dir
        log_file = log_dir + "\\run_out.log"
        # print "Final Log Dir: ", log_dir # don't use
        print "TEST_CASE: ", testCaseName # don't use
        # check if python script with correct name exists
        for script_file in os.listdir(path):
            # script_file.endswith(".py") and
            if testCaseName+".py" == script_file:

                # check and remove run_out.log if exists
                if os.path.isfile(log_file):
                    try: os.remove(log_file)
                    except OSError:
                        print "Couldn't remove:", log_file
                        print OSError

                # Execute Test Case
                os.system("C:\\Python27\\python.exe " + path + "\\" + script_file + " " + log_file)
                # execfile(path + "\\" + script_file)

                # Below line is for closing chrome.exe and chromedriver.exe from tasklist
                os.system("C:\\Python27\\python.exe " + log_root + ".\Common_scripts\close_chromedriver.py ")
                # os.system("C:\\Python27\\python.exe " + log_root + ".\Common_scripts\close_chromedriver.py " + "chrome.exe")

                # os.system("C:\\Python27\\python.exe " + log_root + "report.py " + log_root + "V3_Denton\\test_suites\All_Order_item_type_finalization.ts " + log_root) # TODO Update to support more then one test suite report generation
                os.system("C:\\Python27\\python.exe " + log_root + "report.py " + log_root + "V3_Denton\\test_suites " + log_root) # TODO Update to support more then one test suite report generation

                # Below line is for FireFox, it is closing firefox.exe from tasklist
                # os.system("C:\\Python27\\python.exe " + log_root + ".\Common_scripts\close_any_task.py " + "firefox.exe")

    # This function was added to run over TCs list
    def exec_gtl(tcList):
        # this is a list to save TC paths from gtl, without comments
        TC_cleared_list = []

        # 2  checking if all .tcs' paths are valid in gtl
        for tc in tcList:
            tc = tc.strip()

            # We don't need to work with skipped TCs
            if re.search("tc\s+(-skip)|(-s).*", tc):
                continue

            # Ignore comments, if exists
            comment_search = re.search("(.*tc.*)(\s+(-c)|(-comment).*)", tc)
            if comment_search and comment_search.group(2):
                tc_path_without_comment = comment_search.group(1)
            else:
                tc_path_without_comment = tc

            # 1. This part is skipping empty lines from gtl file | 2. GENERAL CHECK - checking if in gtl present invalid .tc path
            if tc_path_without_comment and not os.path.isdir(tc_path_without_comment):
                errorMessage =  "Invalid tc path in GTL file:\n" + tc_path_without_comment + "\n Please correct and run again..."
                return_error_message(errorMessage)
                return False
            TC_cleared_list.append(tc_path_without_comment)

        # 3  Executing
        for tc in TC_cleared_list:
            tc = tc.strip()
            # This part is skipping empty lines from gtl file
            if tc:
                exec_tc(tc, "gtl")
        return True

    # in TestCase.tc take .py , exec()
    # taking tc name
    if isTC:
        exec_tc(testCaseOrGTL,"test_case")
    elif isGTL:

        TC_in_all_gtl=[]
        # Check if we get .tl file
        if re.search("\.tl$", testCaseOrGTL):
            # get all test suites list
            with open(testCaseOrGTL) as file:
                test_suites_list = file.readlines()

                # Iterate over all test suites and read appropriate gtl file
                for test_suite in test_suites_list:
                    test_suite = test_suite.strip()

                    # Open ts if it exists
                    if os.path.isdir(test_suite):

                        # Get gtl file name
                        last_slash = test_suite.rfind("\\")
                        test_suite_name = test_suite[last_slash+1:]
                        gtl_name = test_suite + "\\" + re.search("(.*\.)ts",test_suite_name).group(1) + "gtl"

                        # Iterate over all files in test suite, pick appropriate gtl file, and fill in TC list
                        for filename in os.listdir(test_suite):

                            filename = test_suite + "\\" + filename.strip()

                            if gtl_name == filename:
                                lines = tuple(open(gtl_name, 'r'))
                                for line in lines:
                                    TC_in_all_gtl.append(line)
                    else:
                        errorMessage = "There is no such directory with name: %s. Please correct it and run again." % (test_suite)
                        return_error_message(errorMessage)
                        return

            # Check that everything is written correct in gtl and Run all TCs
            if not exec_gtl(TC_in_all_gtl):
                return

        else:
            with open(testCaseOrGTL) as file:
                gtl = file.readlines()
                # Check that everything is written correct in gtl
                if not exec_gtl(gtl):
                    return


    else:
        errorMessage = "Something went wrong: Couldn't recognise given path whether it is TC or .gtl "
        return_error_message(errorMessage)
        return

    # TODO - remove this hardcode
    driver = webdriver.Chrome("C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe")
    # final_report_url = "file:///All_Order_item_type_finalization.ts_report.html"
    print

    # import CRS_Lib

    from CRS_Lib import Config
    obj_conf = Config()
    env_name = obj_conf.parse_config()
    final_report_url = "file:///" + str(log_root) + str(env_name[0]) + "_Test_Summary_Report.html"
    print "final_report_url in kofile_test.py = " + str(final_report_url)
    driver.get(final_report_url)


# Open ui if arguments is not given,
# otherwise run the script with the given arguments
if len(sys.argv) != 3 and len(sys.argv) != 1:
    help()
    sys.exit(0)
else:
    obj = Read_Config()
    obj.read_all_configs(kofile_test_script_dir_path)  # This requires root path only (kofile_test run dir) | then it will plus e.g.  V4_Lewington + "\\common\\configs\\" in function(method)
    # Create GUI dialog for automated environment
    if len(sys.argv) == 1:
        app = QtGui.QApplication(sys.argv)
        runTcUi = Ui_Form()
        runTcUi.show()
        isInterface = True
        sys.exit(app.exec_())
    else:
        # Get TC/GTL and log dir paths
        TC_path_from_cmd        = sys.argv[1]
        log_dir_path_from_cmd   = sys.argv[2]

        # We need to detect specified version
        versionMatch = re.search("(V\d).+", TC_path_from_cmd)
        if versionMatch:
            versionString = versionMatch.group(1)
            if not re.search("(\\\\)$|(\/)$", kofile_test_script_dir_path):
                kofile_test_script_dir_path = kofile_test_script_dir_path + "\\"

            # Iterate over all files and get directories
            dirs = [d for d in os.listdir(kofile_test_script_dir_path) if os.path.isdir(kofile_test_script_dir_path + d)]

            # get the config path in selected version dir
            for version_dir in dirs:
                match = re.search("(V\d).+", version_dir)
                if match and match.group(1) == versionString:
                    environment_path = kofile_test_script_dir_path + version_dir
                    kofile_TC_runner(TC_path_from_cmd, log_dir_path_from_cmd, environment_path)
        else:
            print "Error: Specified TC or GTL path is incorrect!"





# binary = FirefoxBinary(r"E:\FirefoxPortable\FirefoxPortable.exe")
# driver = webdriver.Firefox(firefox_binary=binary)
# final_report_url = "file:///"+ log_root + "All_Order_item_type_finalization.ts_report.html"



