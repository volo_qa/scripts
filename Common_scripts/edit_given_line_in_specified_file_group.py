import re
import  os


#Variables for  #replace line 
search_partially_line_for_replace = ''
search_full_line_for_replace = ''
replace_line = ''

# Variable for # replace with empty line 
search_partially_line_for_replace_with_empty = ''

#variables for #append substring before or after
search_partially_line_for_append = ''
search_full_line_for_append = ''
append_line = ''

#variable for # remove line
search_partially_line_for_remove = ''


try:
    # path_example
    path = 'C:/Kofile_Nel/Kofile/V3_Denton/test_suites/All_Order_item_type_finalization.ts'

    # list of files inside folder
    dirs = os.listdir(path)

    for file_1 in dirs:
        # we need only .tc files in folder
        if re.search('.tc', file_1, re.I):
            # replace extension
            file_2 = file_1.replace('.tc', '.py')

            # the full path
            file_3 = path+'/'+file_1+ '/'+ file_2
            print file_3

            # Open a .py file
            lines = open(file_3, 'r').readlines()
            output = open(file_3, 'w+')

            for line in lines:
                # replace line
                if re.search(search_partially_line_for_replace, line, re.I):
                    line = line.replace(search_full_line_for_replace,replace_line)

                # replace with empty line
                if re.search(search_partially_line_for_replace_with_empty, line, re.I):
                    line = ''

                #append substring before or after
                if re.search(search_partially_line_for_append, line, re.I):
                     # append new substring before
                    line = append_line+'\n' + search_full_line_for_append + '\n'

                    # append new substring after
                    line = search_full_line_for_append +'\n'+ append_line+ '\n'

                # remove line
                if not re.search(search_partially_line_for_remove, line, re.I):
                    output.write(line)

            output.close()

except Exception as e:
    print "Given path is incorect"

    exit(0)