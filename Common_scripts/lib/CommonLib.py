import os
from datetime import datetime
import subprocess
from selenium import webdriver
import sys
import re
import xml.etree.ElementTree as ET
import ctypes

# define error message reporting function
def return_error_message(message):
    # open error message dialog
    ctypes.windll.user32.MessageBoxA(0, message, "Error", 0)

class Read_Config():
    # config_file= "" # TODO - will be removed , will get a list of config files from V3,V4,V5
    version_plus_name_plus_url = []
    db_connection_data = []

    def read_all_configs(self,path):
        flag = False
        all_version_dirs = []

        if not re.search("(\\\\)$|(\/)$", path):
            path = path + "\\"

        # ----------- PATH & FILE PRESENCE checks -----------#
        # dirs = [d for d in os.listdir(path) if os.path.isdir(path+d)]
        dirs = []

        for d in os.listdir(path):
            # print "path and d", path +"\\"+ d
            if os.path.isdir(path + d):
                dirs.append(d)

        standard_path_to_config = "\\common\\configs\\"
        for version_dir in dirs:
            match = re.search("^V(\d).+",version_dir)
            if match:
                item_version =  match.group(1) # Takes digit of 'V4', e.g.: 3, or 4 or 5
                config_dir_path = path + version_dir + standard_path_to_config

                # PATH check
                if not os.path.isdir(config_dir_path):
                    errorMessage = "Error: %sdir is not present under %s"% (standard_path_to_config, version_dir)
                    return_error_message(errorMessage)
                    flag = True

                # appending
                all_version_dirs.append((item_version,config_dir_path))

        if flag:
            print "\nExiting..."
            exit(0) # Attention. don't remove exit, script will not work without it

        # here flag is still false (as above exiting if it it True)
        flag = False
        for current_version, config_dir_path in all_version_dirs:
            # File presence check
            if not os.path.isfile(str(config_dir_path) + "Configuration.xml"):
                errorMessage =  "V%s Error: Config xml file is missing in: %s" % (current_version, config_dir_path)
                return_error_message(errorMessage)

        if flag:
            print "\nExiting..."
            exit(0)


        for current_version, config_dir_path in all_version_dirs:
            Read_Config.format_checker(current_version, config_dir_path + "Configuration.xml")
            self.get_all_envionments(current_version, config_dir_path + "Configuration.xml")


    def get_all_envionments(self,current_version, config_file):

        root = ET.parse(config_file).getroot()
        for VersionConfig in root.findall('VersionConfig'):
            # making string in case, version is given as an 'int' instead of string. Ex: 3 instead of "3"
            current_version = str(current_version)

            # Tries to find current version in all <VersionConfig> xml Tags.
            if VersionConfig.attrib['version'] == current_version: # '3'
                for TenantConfig in VersionConfig:
                    env_name = TenantConfig.attrib['TenantName']
                    environment_url = TenantConfig.attrib['Url']
                    self.version_plus_name_plus_url.append("V" + current_version +" "+ env_name +" - "+ environment_url)

                    # TODO: get all db configs
                    tenant_name = TenantConfig.attrib['TenantName']
                    tenant_code = TenantConfig.attrib['TenantCode']
                    server_ip= TenantConfig.getchildren()[1].attrib['SERVER']
                    database_name = TenantConfig.getchildren()[1].attrib['DATABASE']
                    uid = TenantConfig.getchildren()[1].attrib['UID']
                    password = TenantConfig.getchildren()[1].attrib['PASSWD']
                    self.db_connection_data.append(["V" + current_version + tenant_name, tenant_code, server_ip, password, uid, database_name])

        if not self.version_plus_name_plus_url and not self.db_connection_data:
            errorMessage = "Error: Something went wrong. Couldn't get environment NAME and/or URL and/or DB releated data for Current \"%s\" version from <TenantConfig> tag from conf xml file.\n\nInfo: Please check the config and run again. Exiting..." % (str(current_version))
            return_error_message(errorMessage)
            exit(0)


        # return self.version_plus_name_plus_url





    @staticmethod
    def format_checker(current_version, config_file):

        with open(config_file) as file:
            file =  file.readlines()

        # ------------------------------^^ TOP level (root level) checks ^^-------------------------------------#
        is_EnvConfig_1_found = False
        is_EnvConfig_2_found = False
        for line in file:
            line = line.strip()

            # # TODO we need first or second line to be EnvConfig
            # if  re.search("^[\s\t]+$", line, re.I): # or  re.search("^$", line, re.I):
            #     print "Found..."

            if re.search("\<EnvConfig", line, re.I):
                is_EnvConfig_1_found = True

            if re.search("\<\/EnvConfig\>", line, re.I):
                is_EnvConfig_2_found = True



        if not is_EnvConfig_1_found:
            errorMessage = "V%s Error: '<EnvConfig>' as a root node is NOT found. Please correct xml config file and run again. Exiting ..." % current_version
            return_error_message(errorMessage)
            exit(0)

        if not is_EnvConfig_2_found:
            errorMessage =  "V%s Error: '</EnvConfig>' as a root node is NOT found. Please correct xml config file and run again. Exiting ..." % current_version
            return_error_message(errorMessage)
            exit(0)



        root = ET.parse(config_file).getroot()
        # ------------------------------^ HIGH level checks ^-------------------------------------#
        # checking <VersionConfig> tag presence under <EnvConfig> tag
        # exiting in case either, <VersionConfig> or <UserCredentials> tag is not present under <EnvConfig> tag
        if not root.findall('VersionConfig') or not root.findall('UserCredentials'):
            if not root.findall('VersionConfig'):
                errorMessage = "V%s Error: Version Config is NOT present in config xml file under <EnvConfig> tag\nExample: <EnvConfig>\n\t\t   <VersionConfig version='%s'>\nCorrect xml config and run again. Exiting..." % (current_version,current_version)
                return_error_message(errorMessage)


            # checking <UserCredentials> tag presence under <EnvConfig> tag
            if not root.findall('UserCredentials'):
                errorMessage = "V%s Error: UserCredentials are NOT present under <EnvConfig> tag\n Example: <EnvConfig>\n\t\t   <UserCredentials>\n\nCorrect xml config and run again. Exiting..." %current_version
                return_error_message(errorMessage)

            exit(0)

        is_version_found = False
        is_TenantConfig_found = False

        flag = True


        # ------------------------------ LOW level checks --------------------------------------#
        for VersionConfig in root.findall('VersionConfig'):
            # Tries to find current version in all <VersionConfig> xml Tags.
            # TODO 1: Done? -> check if current version present. Ex: current version = 3
            # TODO 2: check negative case: When other version is not present at all
            # TODO 3: after TODO 2 (above) is implemented, <<if VersionConfig == current_version: # '3'>> part can be removed from` GET_ALL_ENVIRONMENTS method
            if VersionConfig.attrib['version'] == current_version: # '3'

                for TenantConfig in VersionConfig:
                    if TenantConfig.attrib['value'] == 'default':

                        if flag:
                            #print TenantConfig.attrib['TenantName'], TenantConfig.attrib['Url']

                            env_name = TenantConfig.attrib['TenantName']
                            environment_url = TenantConfig.attrib['Url']

                            # if at least one have 'default' value then flag will be set to False
                            flag = False
                        else:
                            # TODO remove and (check if default value is not present ?) - if it is OK for standlone run , then remove it
                            # Will exit in case there are 2 or more 'default' values set by chanse, in xml configuration file
                            errorMessage = "Error: There is more than one TenantConfig with 'default' value in configuration xml file.\n\nPlease correct and run again."
                            return_error_message(errorMessage)
                            exit(0)
                    else:
                        is_TenantConfig_found = True

                    #DB cofigs parsing part
                    tenant_name = TenantConfig.attrib['TenantName']
                    tenant_code = TenantConfig.attrib['TenantCode']
                    server_ip= TenantConfig.getchildren()[1].attrib['SERVER']
                    database_name = TenantConfig.getchildren()[1].attrib['DATABASE']
                    uid = TenantConfig.getchildren()[1].attrib['UID']
                    password =TenantConfig.getchildren()[1].attrib['PASSWD']


                # Will exit the script in case  <TenantConfig value='default'>  is not found in config xml
                if flag:
                    errorMessage = "Error : TenantConfig with 'default' value doesn't exist. Couldn't get environment URL.\n\n" \
                                   "Info : In order to get environment URL. Configuration.xml should contain at least one '<TenantConfig' tag which have 'value' attribute equal to 'default'.\n\n" \
                                   "Example : <TenantConfig value='default' TenantName='Denton' TenantCode='48121' Url='http://crs.qa.kofile.com/48121'>"

                    return_error_message(errorMessage)
                    exit(0)

                # if current version Tag is found, will not look for further versions
                is_version_found = True
                break
            else:
                is_version_found = False

        # Will exit the script in case <VersionConfig> xml Tag have wrong "current version" (of the product).
        if not is_version_found:
            errorMessage =  "V%s Error: <VersionConfig version=\"%s\">  is not found in configuration xml file" %(current_version, current_version)
            return_error_message(errorMessage)
            exit(0)





# obj = Read_Config()
# obj.read_all_configs("E:\\Kofile_VS\\Kofile\\")
# # obj.get_all_envionments(3, "E:\\Kofile_VS\\Kofile\\V3_Denton\\common\\configs\\Configuration.xml")

# if not obj.version_plus_name_plus_url:
#     print "Error: O.O"
#     # exit(0)
# print obj.version_plus_name_plus_url
#
