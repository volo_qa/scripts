# Owner: Raffi
"""
This script c
It works multi-version allowing user to pick a version for which script will work
Or works for a ONE VERSION if it is specified as an Default value.
"""
import os,re

root = ''
real_path = os.path.dirname(os.path.realpath(__file__))

match = re.search("(.+(\/|\\\\))Common_scripts", real_path, re.I)

if match:
    root = match.group(1)
else:
    print "Process went wrong. This script's real Path doesn't include .+\Common_scripts." \
          "\nIf the script's real path is changed please update it." \
          "\n\nExiting..."
    exit(0)

Default = True
# Default = False

if not Default:
    dirs = []
    isFound = False
    for d in os.listdir(root):
        # print "path and d", path +"\\"+ d
        if os.path.isdir(root + d) and re.search("(^V\d.+)",d):
            isFound = True
            dirs.append(d)

    if isFound:
        print "Versions are:"
        for directory in dirs:
            print "\t",directory
    else:
        print "\tNo any version (starting with V digit , e.g. V3) in:", root

    environment = raw_input("\nEnter Verson for which script will work >: ")

else:
    # environment  var can be e.g. V4_Grayson
    environment = "V3_Denton"

print "\nEnvironment is:", environment
print "\nRoot is:", root + environment + "\\test_suites"



import fnmatch

# Finds all gtl files and collects in a list
gtl_files = []
for root_1, dirnames, filenames in os.walk(root + environment + "\\test_suites"):
    for filename in fnmatch.filter(filenames, '*.gtl'):
        gtl_files.append(os.path.join(root_1, filename))


# ---- File Modify part ----- #
for gtl in gtl_files:

    new_file = []
    with open(gtl, "r") as f:
        GtlFile = f.readlines()
    f.close()

    for line in GtlFile:

        match2 = re.search("(\w\:.+(\\\\|\/))V\d", line)
        if match2:

            # print "Before:",  line
            # print "Group1:",  match2.group(1)
            # print "root  :",  root

            # making '\\' instead of '\'  for regex syntax (when we maualy write regex we use double slash, and teh same in this case)
            # from  K:\Kofile_VS\  to  K:\\Kofile_VS\\
            first = match2.group(1).replace("\\","\\\\")
            second = root.replace("\\","\\\\")
            line = re.sub(first, second, line)

            # print "After:", line

        new_file.append(line)


    with open(gtl, "w") as f:
        file= f.writelines(new_file)
    f.close()

