import os
import re
from os import listdir
from os.path import isfile, join
import sys
# import CRS_Lib
from CRS_Lib import Config
from datetime import datetime

# print("Content-Type: text/html")    # HTML is following               #for CGI
# print                               # blank line, end of headers      #for CGI

script_file_name = os.path.basename(__file__)

print "---------------------------------------------------------"
print "Start executing \"" + str(script_file_name) + "\" script."
print "---------------------------------------------------------"
print "\t\tCurrent path is: \"" + os.getcwd() + "\"."


# title = "Automat V3 Denton Regression"
# env = "CRS - Environment: http://crs.qa.kofile.com/48121"
env = "CRS - Environment: "
run = "Test_Summary"
test_suite_path = "All_Order_item_type_finalization.ts"

# old way
# env_name = CRS_Lib.Config.get_env_name()
obj_conf = Config()
env_name = obj_conf.parse_config()


# print "Environment = " + str(env_name)
print "Environment URL = " + str(env_name[1])         # http://10.0.1.73:8002/48121
print "Environment Name = " + str(env_name[0])        # V3_Denton_local

environment_url = env_name[1]                   # http://10.0.1.73:8002/48121
env_name = env_name[0]                          # V3_Denton_local

title = "Automat " + env_name + " Regression"

if len(sys.argv) > 1:   # TODO 1 should be changed to 2 all scripts have minimum 1 argument sys.argv[0] which is script name
    mypath = sys.argv[1]
    log_root_path = sys.argv[2]
    print "\t\tWas run with two arguments: 1 - \"" + str(sys.argv[1]) + "\" and 2 - \"" + str(sys.argv[2]) + "\""
else:
    mypath = os.getcwd() + "\V3_Denton\\test_suites"
    log_root_path = os.getcwd() + "\\"
    # mypath = "E:\\mher.simonyan_log\V3_Denton\\test_suites"
    # log_root_path = "E:\\mher.simonyan_log\\"

num = 1
num_pass = 0
num_fail = 0
str1 = ""
str0 = ""
num_pass_per = ""
# num_pass_per = ""
num_fail_per = ""

print "\t\tTest suites path:\t\t", mypath
print "\t\tCurrent path:\t\t\t", os.getcwd()
print "\t\tList of items in\t\t", mypath, ": " , listdir(mypath)
new_list = []

list = listdir(mypath)
for item in list:
    if re.search(".ts", item, re.I):
        new_list.append(item)

print "\t\tList of test suites in\t", mypath, ": " , new_list
if not new_list:
    print "\t\tNo any Test Suite found in\t", mypath
    exit(0)

str1 += "<table border=\"1\" cellpadding=\"2\" cellspacing=\"1\" align=\"left\">"
str1 += "<tbody>"
# str1 += "<tr cellspacing=\"4\">"

all_sec = 0
for test_suite in new_list:
    suite_path = mypath + "\\" + test_suite
    print "\t\tFound test suite:\t\t\"" + suite_path + "\""

    run_test_count = 0
    for f in listdir(suite_path):  # find all *.tc or files in full path of run folder name
        if os.path.isfile(suite_path + "\\" + f + "\\run_out.log"): #Check if exist min one run_out.log then print headers in HTML
            run_test_count +=1

    if run_test_count > 0:
        #str1 = "<table border=\"1\" cellpadding=\"2\" cellspacing=\"1\" align=\"left\">"
        # str1 += "<table border=\"1\" cellpadding=\"2\" cellspacing=\"1\" align=\"left\">"
        # str1 += "<tbody>"
        str1 += "<tr cellspacing=\"4\">"
        # str1 += "<th align=\"center\" bgcolor=\"#ffea9b\" width=\"100%\" colspan=\"4\">Test Suite - Order Finalization All OIT</th>"                     # Writes test suite name
        # str1 += "<th align=\"center\" bgcolor=\"#ffea9b\" width=\"100%\" colspan=\"4\">" + test_suite.replace(".ts", " Test Suite") + "</th>"             # Writes test suite name
        str1 += "<th align=\"center\" bgcolor=\"#ffea9b\" width=auto colspan=\"5\">" + test_suite.replace(".ts", " Test Suite") + "</th>"             # Writes test suite name  100%
        str1 += "</tr>"
        str1 += "<tr>"
        str1 += "<th align=\"left\" bgcolor=\"#ffea9b\" width=\"auto\">N</th>"           #N         5%
        str1 += "<th align=\"left\" bgcolor=\"#ffea9b\" width=\"75%\">Test Case</th>"    #Test Case 75%

        str1 += "<th align=\"center\" bgcolor=\"#ffea9b\" width=\"150\" nowrap>"
        str1 += "<font color=\"#000000\">Owner</font></a>"
        str1 += "</th>"

        str1 += "<th align=\"center\" bgcolor=\"#ffea9b\" width=\"150\" nowrap>"         #Status    10%
        str1 += "<font color=\"#000000\">Status</font></a>"
        str1 += "</th>"
        str1 += "<th align=\"center\" bgcolor=\"#ffea9b\" width=\"auto\">"               #Duration  10%
        str1 += "<font color=\"#000000\">Duration</font></a>"
        str1 += "</th>"
        str1 += "</tr>"

    for f in listdir(suite_path):                               # find all *.tc or files in full path of run folder name
        # print "f = " + f
        if not re.search("\.tc", f):
            continue

        validate_log = suite_path + "\\" + f + "\\run_out.log"
        test_case_script_dir_path = suite_path + "\\" + f
        test_case_script_file_path = suite_path + "\\" + f + "\\" + f.replace(".tc", ".py")
        # print "suite_path = " + suite_path
        # print "validate_log = " + validate_log
        if not os.path.exists(validate_log):
            continue

        f_val = open(validate_log, "r+")
        # y = str(f_val)
        # print y.find("Passed")

        str1 += "<tr>"
        # str1 += "<td>"
        str1 += "<td width=\"auto\">"
        str1 += "<font size=\"2\"><font color=\"#0000FF\">"
        str1 += "<b>" + str(num) + "</b>"                   # Writes in html page each *.tc number
        num += 1
        str1 += "</td>"

        # str1 += "<tr>"
        # str1 += "<td>"
        str1 += "<td width=\"75%\">"       #Test Case 75%
        str1 += "<font size=\"2\"><font color=\"#0000FF\">"
        str1 += "<b><a href='" + test_case_script_dir_path + "'>" + f + "</a></b>"                          # Writes in html page each *.tc name
        # print test_case_script_dir_path
        str1 += "</td>"

        # link = "D:\log\V3_Denton\\test_suites\All_Order_item_type_finalization.ts\Account_Payment.tc\\run_out.log"
        link = validate_log

        with open(validate_log) as f1:                      # opens easch validate_out.log file and searches for a Test Case Pass/Fail string

            # if 'Test case PASS' in f1.read():
            #     num_pass += 1
            #     print f, " PASS"
            #     str1 += "<td align=\"center\" bgcolor=\"#43f400\" width=\"150\" nowrap>"
            #     str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>PASS</a></font>"
            # elif 'Validate part FAIL' in f1.read():
            #     num_fail += 1
            #     print f, " FAIL"
            #     str1 += "<td align=\"center\" bgcolor=\"#ff0014\" width=\"150\" nowrap>"
            #     str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>Validate part FAIL</a></font>"
            # elif 'Run part FAIL' in f1.read():
            #     num_fail += 1
            #     print f, " FAIL"
            #     str1 += "<td align=\"center\" bgcolor=\"#ff0014\" width=\"150\" nowrap>"
            #     str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>Run part FAIL</a></font>"
            # else:
            #     num_fail += 1
            #     print f, " CRASH"
            #     str1 += "<td align=\"center\" bgcolor=\"#ff0014\" width=\"150\" nowrap>"
            #     str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>CRASH</a></font>"

            # Collcet Owners from test cases
            str1 += "<td align=\"center\" width=\"auto\">"
            str1 += "<font size=\"2\"><font color=\"#000000\">"
            Owner = ""
            with open(test_case_script_file_path) as script:
                for line in script:
                    if "#Owner:" in line:
                        str1 += "<b>"

                        line = line.replace(" ", "")  # Removes spaces: from string
                        line = line.replace("#Owner:", "")  # Removes Owner: from string
                        line = line.replace("\t", "")  # Removes tabs: from string
                        Owner = line

                        if Owner == "":
                            Owner = "No Owner assigned"
                        break
                    else:
                        Owner = "No Owner line"
            str1 += Owner
            str1 += "</td>"

            # Collect test case statuses from run_out.log files
            found = False
            for line in f1:
                if  "Test case PASS" in line:
                    num_pass += 1
                    # print f, " PASS"
                    str1 += "<td align=\"center\" bgcolor=\"#AAEEAA\" width=\"150\" nowrap>"            #43f400
                    str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>PASS</a></font>"
                    found = True
                    break
                elif "Validate part FAIL" in line:
                    num_fail += 1
                    # print f, " FAIL"
                    str1 += "<td align=\"center\" bgcolor=\"#FFBBAA\" width=\"150\" nowrap>"        #ff0014
                    str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>Validate part FAIL</a></font>"
                    found = True
                    break
                elif "Run part FAIL" in line:
                    num_fail += 1
                    # print f, " FAIL"
                    str1 += "<td align=\"center\" bgcolor=\"#FFBBAA\" width=\"150\" nowrap>"
                    str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>Run part FAIL</a></font>"
                    found = True
                    break

            if not found:
                num_fail += 1
                # print f, " CRASH"
                str1 += "<td align=\"center\" bgcolor=\"#FFBBAA\" width=\"150\" nowrap>"
                str1 += "<font color=\"black\" size=\"2\"><a href='" + link + "'>CRASH</a></font>"

        str1 += "</td>"

        str1 += "<td align=\"right\" width=\"auto\">"
        str1 += "<font size=\"2\"><font color=\"#000000\">"

        run_log = suite_path + "\\" + f + "\\run_out.log"     # print run_log       80 20 20 % is stretched
        f_run = open(run_log, "r+")                       # Openes each fun_out.log file and reads Duration of Test case Run
        with open(run_log) as f1:
            for line in f1:
                if "Duration:" in line:
                    str1 += "<b>"

                    line = line.replace("Duration: ", "")   # Removes Duration: from string
                    line = line[:7]                         # removes miliseconds from the end of string

                    i=0
                    if line[2:3] == ":":        # Example: 01:12:15 and else 0:15:51
                        i=1

                    hour_to_sec = int(line[0+i:1+i]) * 60 * 60
                    minute_to_sec = int(line[2+i:4+i]) * 60
                    hour = int(line[0+i:1+i])
                    minute = int(line[2+i:4+i])
                    second = int(line[5+i:7+i])
                    line2 = hour * 60 * 60 + minute * 60 + second

                    all_sec += line2
                    # str1 += "<b>" + str(line) + " sec</b>"                            # Adds Duration in Html    Example: 0:01:45
                    # str1 += "<b>" + str(line2) + " sec</b>"                           # Adds Duration in Html    Example: 105 sec
                    if hour == 0:
                        if minute == 0:
                            str1 += str(second) + "s"                                              # Adds Duration in Html    Example: 45s
                        else:
                            str1 += str(minute) + "m " + str(second) + "s"                         # Adds Duration in Html    Example: 1m 45s
                    elif hour > 0:
                            str1 += str(hour) + "h " + str(minute) + "m " + str(second) + "s"      # Adds Duration in Html    Example: 1h  1m  4s
                    else:
                        str1 += "-"                                                                # Adds Duration in Html    Example: -


                else:
                    str1 += "</b>"

        str1 += "</td>"
        str1 += "</tr>"

        f_val.close()
            # str1 += "<tr>"
            # str1 += "<td>"
            # str1 += "<font size=\"2\"><font color=\"#0000ff\">"
            # str1 += "<b>Birth_Verification.tc</b>"
            # str1 += "</font></font>"
            # str1 += "</td>"
            # str1 += "<td align=\"center\" bgcolor=\"#ff0014\" width=\"20%\">"
            # str1 += "<font color=\"black\" size=\"2\"> FAIL</font>"
            # str1 += "</td>"
            # str1 += "</tr>"

    # str1 += "</tbody>"
    # str1 += "</table>"
    # str1 += "<br><br>"

    if num != 1:
        num_pass_per = float(num_pass)/float(num-1)*100
        num_fail_per = float(num_fail)/float(num-1)*100
        num_pass_per = int(round(num_pass_per,0))
        num_fail_per = int(round(num_fail_per,0))
    else:
        print "\t\tCould not find any log file."

str1 += "</tbody>"
str1 += "</table>"
str1 += "<br><br>"

# Here is the start part og HTML as PASS/FAIL nums are calculated at the top
# str0 =  "<head>"
str0 +=  "<head>"
str0 += "<title>" + title + "</title>"
str0 += "<link type=\"text/css\" rel=\"stylesheet\" href=\"app-combined.css\" media=\"all\">"
str0 += "</head>"
str0 += "<body>"
str0 += "<br>"
str0 += "<h2>" + env_name + "</h2>"
str0 += "<br>"
str0 += "<h3>" + env + environment_url + "</h3>"
str0 += "<br><br>"
# str0 += "<h3>Run Folder: " + run + "</h3>"                                                  # Here is the html line which writes run folder name in html
# print run
# -----------------------------------------------------Graphiical percent part---------------------------------------------------------start-
str0 += "<div class=\"column chart-pie-column-legend-compact\">"
str0 += "<div class=\"chart-legend\" style=\"padding-top: 20px\">"
str0 += "<div class=\"table chart-legend-item\" style=\"height: auto\">"
str0 += "<div class=\"column column-valign chart-legend-column-icon\">"
str0 += "<div class=\"chart-legend-icon\" style=\"background: #43f400\"></div>"
str0 += "</div>"
str0 += "<div class=\"column\">"
str0 += "<div class=\"chart-legend-name text-ppp\">" + str(num_pass) + " Passed</div>"                         # 20
str0 += "<div class=\"chart-legend-description text-ppp\">" + str(num_pass_per) + "% Passed</div>"                 # 95%   Passed
str0 += "</div>"
str0 += "</div>"
str0 += "<div class=\"table chart-legend-item\" style=\"height: auto\">"
str0 += "<div class=\"column column-valign chart-legend-column-icon\">"
str0 += "<div class=\"chart-legend-icon\" style=\"background: #ff0014\"></div>"
str0 += "</div>"
str0 += "<div class=\"column\">"
str0 += "<div class=\"chart-legend-name text-ppp\">" + str(num_fail) + " Failed</div>"                          # 1
str0 += "<div class=\"chart-legend-description text-ppp\">" + str(num_fail_per) + "% Failed</div>"                  # 5% Failed
str0 += "</div>"
str0 += "</div>"
str0 += "</div>"
str0 += "</div>"
str0 += "<div class=\"column chart-pie-column-percent\">"
str0 += "<div class=\"chart-pie-percent\" style=\"padding-top: 5em\">"
str0 += "<div class=\"chart-pie-percent-title\">" + str(num_pass_per) + "%</div>"
str0 += "<div class=\"chart-pie-percent-description\">passed</div>"
# sr1 += "<div class=\"chart-pie-percent-note text-secondary\" style=\"line-height: 130%\">	0 / 21 untested (0%). </div>"
str0 += "</div>"
str0 += "</div>"
str0 += "<br>"

sec1 = all_sec % 60
min1 = all_sec / 60 - (all_sec / 3600)*60
hour1 = all_sec / 3600
str0 += "<div>Total Run Time: " + str(hour1) + "h " + str(min1) + "m " + str(sec1) + "s" + "</div>"
date_now = str(datetime.now())
str0 += "<div>Run Date: " + str(date_now[0:10]) + "</div>"

# str0 += "<tr>"
# str0 += "<th align=\"left\" bgcolor=\"#ffea9b\" width=\"75%\">Test Sute - Order Finalization All OIT</th>"
# str0 += "</tr>"


str0 += "<br>"
# -----------------------------------------------------Graphiical percent part---------------------------------------------------------end--


# report = open("F:\log\\"+ run + "_report.html", "w+")
# report = open("D:\log\\"+ run + "_report.html", "w+")
# report = open(log_root_path + run + "_Report.html", "w+")
print "report = ", log_root_path + env_name + "_" + run + "_Report.html"
report = open(log_root_path + env_name + "_" + run + "_Report.html", "w+")   # Example V3_Denton_local_Test_Summary_Report.html
report.write(str0+str1)
report.close()

print "---------------------------------------------------------"
print "Finish execution of \"" + str(script_file_name) + "\" script."
print "---------------------------------------------------------"

# str1 += "</body><html>"     # for CGI
# print str0+str1             # for CGI